/**
 * All purpose FitAnalysis script for 2mm resolution scans.
 * Compatible with scans using both gantries.
 * Generates different types of gain and relative detection efficiency plots
 * based on user input (plots with respect to gantry position as well as with
 * absolute coordinates of the point of laser injection on the PMT cover).
 *
 * Edited by: Rika Sugimoto
 * Date: February 10, 2017
 *
 * NOTE: some values for the analysis of ADC spectra are still hardcoded
 *       and must be edited manually.
 **/


#include "TTree.h"
#include "TBranch.h"
#include "time.h"
#include "stdio.h"
#include "unistd.h"
#include <iostream>
#include <sstream>
#include "TGraph.h"
#include "TH1F.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TH2D.h"
#include "TColor.h"
#include "TStyle.h"
#include <cmath>


Double_t roundToThousandths(Double_t x){

    x*=10000;
    return floor(x+0.5)/10000;
    

}

void FitAnalysis2mmShiftedPed(){

    const Double_t PI = 3.141592653589793238463;
    const int GANTRY_POS = 0; //Plot of Gantry position
    const int PMT_ABS_COORD = 1; //Plot of PMT position in absolute coordinates
    const int SIZE = 100; //Size of phidgtilt array

    //Run number of scans run
    TString run0; //Scan using Gantry 0
    TString run1; //Scan using Gantry 1
    std::cout<<"\nEnter run number for scan with Gantry 0:"<<endl;
    std::cin>>run0;
    std::cout<<"\nEnter run number for scan using Gantry 1 \n(if only one file used, enter same run number):"<<endl;
    std::cin>>run1;

    int whichPlot = PMT_ABS_COORD; //Choose plot type (GANTRY_POS or PMT_ABS_COORD)
    std::cout<<"\nChoose plot type \n(0 == position of source, 1 == position of target):"<<endl;
    std::cin>>whichPlot;

    if(whichPlot==GANTRY_POS){
	TString gainPlotTitle = "Gain from Pedestal as a Function of Position of Source";
	TString detPlotTitle = "Relative Detection Efficiency as a Function of Position of Source";
    }
    else{
	TString gainPlotTitle = "Gain from Pedestal as a Function of Position on Target";
	TString detPlotTitle = "Relative Detection Efficiency as a Function of Position on Target";
    }

    //Set up Display Canvas etc.
    Double_t w = 1600;
    Double_t h = 1000;
    TCanvas *c1 = new TCanvas("c1","Results of Gaussian Fit to Pedestal", w, h);
    TCanvas *c2 = new TCanvas("c2","Plot of Positional Data from PMT Run",800,800);
    TCanvas *c3 = new TCanvas("c3","Plot of Positional Data from PMT Run",800,800);
    
    c3->Divide(1,1);
    c2->Divide(1,1);
    c1->Divide(2,3);
    
    //Segev BenZvi's Colour palette (http://ultrahigh.org/2007/08/20/making-pretty-root-color-palettes/)                                                                               
    const int NRGBs = 5;
    const int NCont = 255;
    
    double stops[NRGBs] = { 0.00, 0.34, 0.61, 0.84, 1.00 };
    double red[NRGBs]   = { 0.00, 0.00, 0.87, 1.00, 0.51 };
    double green[NRGBs] = { 0.00, 0.81, 1.00, 0.20, 0.00 };
    double blue[NRGBs]  = { 0.51, 1.00, 0.12, 0.00, 0.00 };
    TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
    gStyle->SetNumberContours(NCont);
    
    
    // Pad & Canvas Options
    gStyle->SetPadColor(10);
    gStyle->SetPadTickX(1);
    gStyle->SetPadTickY(1);
    gStyle->SetCanvasColor(10);
    gStyle->SetOptStat(0);
    gStyle->SetPadGridX(kTRUE);
    gStyle->SetPadGridY(kTRUE);
    
    // Frame Options
    gStyle->SetFrameLineWidth(1);
    gStyle->SetFrameFillColor(10);
    
    // Marker Options
    gStyle->SetMarkerStyle(20);
    gStyle->SetMarkerSize(0.8);
    
    gStyle->SetTitleSize(0.05,"x");//X-axis title size
    gStyle->SetTitleSize(0.05,"y");
    gStyle->SetTitleSize(0.05,"z");
    gStyle->SetLabelSize(0.05,"x");//X-axis title size
    gStyle->SetLabelSize(0.05,"y");
    gStyle->SetLabelSize(0.05,"z");
    
    //local variable creation
    Int_t arr[200000];
    Int_t val_points;
    Int_t numEntries;
    Int_t p_lo_bound;
    Int_t p_hi_bound;
    Int_t e_lo_bound;
    Int_t e_hi_bound;
    Int_t binmax;
    Int_t x_pos;

    Double_t xVal = 0;
    Double_t yVal = 0;
    Double_t xposition, yposition, zposition; //gantry position
    Double_t gantrytilt;
    Double_t gantryrot;
    Double_t phidg0tilt[SIZE]={0};
    Double_t phidg1tilt[SIZE]={0};
    Double_t averagePhidgTilt;
    Double_t x_inc, y_inc; //point of incidence of laser beam
    Double_t x, y; //point plotted on gain and detection efficiency plots
    Double_t final_offset_x, final_offset_y; //Corrections for optical box
    Double_t beamLength = 0.1;

    Float_t sigmaEvent;
    Float_t sigmaPed;
    Float_t meanEvent;
    Float_t meanPed;
    Float_t constantEvent;
    Float_t constantPed;

    //offset in laser aperture with respect to gantry position
    Double_t g0_offset1 = 0.115;
    Double_t g0_offset2 = 0.051;// - 0.004*beamLength*10; //corrections for offset in x added (+0.004m drift per 0.1m beam length)
    Double_t g0_offset3 = 0.037;
    Double_t g1_offset1 = 0.115;
    Double_t g1_offset2 = 0.052;
    Double_t g1_offset3 = 0.039;

    //10 is empirical and observed average spread, will be refined later
    Int_t spread = 2.5;
    
    //empirical, tbd later
    e_lo_bound = 192;
    e_hi_bound = 220;

    //Creates histograms that will be used to store data
    TH1F *smallHist = new TH1F("LittleHist","Little",120,130,250); //used for local data analysis
    TH1F *sigmaEventHist = new TH1F("SigmaEventHist","Width (SPE Peak Gaussian Fit)(Sigma)",200,0,150);
    TH1F *sigmaPedHist = new TH1F("SigmaPedHist","Width (Pedestal Gaussian Fit)(Sigma)",200,0.6,1.40);
    TH1F *chiSquareHistSPE = new TH1F("ChiSquare1","Chi-Square / NDoF Distro for SPE Peak Gaussian Fit", 75, -1, 5);
    TH1F *chiSquareHistPed = new TH1F("ChiSquare2","Chi-Square / NDoF Distro for ADC Pedestal Gaussian Fit",75,-1,20);
    TH1F *peakValleyRatioHist = new TH1F("pvRatio","Peak to Valley Ratio of ADC Distro", 120, 0, 12);
    TH1F *gain1DHist = new TH1F("gain1DHist","Gain of PMT in ADC Counts", 80, 0, 80);
    /**If start coordinates are even values
    TH2D *gainPositionHist = new TH2D("gainHist", gainPlotTitle, 312, 0.001, 0.625, 310, 0.001, 0.621);
    TH2D *detPositionHist = new TH2D("detHist", detPlotTitle, 312, 0.001, 0.625, 310, 0.001, 0.621);
    TH2D *binCheckHist = new TH2D("binningCheck","PositionBinningHisto",312,0.001,0.625, 310, 0.001, 0.621);*/
    
    //If start coordinates are odd values
    TH2D *gainPositionHist = new TH2D("gainHist", gainPlotTitle,312,0.0,0.624,310,0.00,0.62);
    TH2D *detPositionHist = new TH2D("detHist", detPlotTitle,312, 0.0, 0.624,310, 0.00, 0.62);
    TH2D *binCheckHist = new TH2D("binningCheck","PositionBinningHisto",312,0.0,0.624,310,0.00,0.62);
     
    //Make Graph to check if signal dependant on position
    gainPositionHist->SetTitle(gainPlotTitle);
    
    gainPositionHist->GetXaxis()->SetTitle("X Position (m)");
    gainPositionHist->GetXaxis()->CenterTitle();
    gainPositionHist->GetXaxis()->SetTitleSize(0.04);
    gainPositionHist->GetXaxis()->SetLabelSize(0.03);
    
    gainPositionHist->GetYaxis()->SetTitle("Y Position (m)");
    gainPositionHist->GetYaxis()->SetTitleSize(0.04);
    gainPositionHist->GetYaxis()->SetTitleOffset(1.2);
    gainPositionHist->GetYaxis()->SetLabelSize(0.03);
    gainPositionHist->GetYaxis()->CenterTitle();
    
    //Set axis limits
    gainPositionHist->GetXaxis()->SetRangeUser(0,0.6);
    gainPositionHist->GetYaxis()->SetRangeUser(0,0.64);
    gainPositionHist->GetZaxis()->SetRangeUser(0,35);
    gainPositionHist->GetZaxis()->SetLabelSize(0.02);
    
    
    //Second graph for detection efficiency
    detPositionHist->SetTitle(detPlotTitle);
    
    detPositionHist->GetXaxis()->SetTitle("X Position (m)");
    detPositionHist->GetXaxis()->CenterTitle();
    detPositionHist->GetXaxis()->SetTitleSize(0.04);
    detPositionHist->GetXaxis()->SetLabelSize(0.03);
    
    detPositionHist->GetYaxis()->SetTitle("Y Position (m)");
    detPositionHist->GetYaxis()->SetTitleSize(0.04);
    detPositionHist->GetYaxis()->SetTitleOffset(1.2);
    detPositionHist->GetYaxis()->SetLabelSize(0.03);
    detPositionHist->GetYaxis()->CenterTitle();
    
    //Set axis limits
    detPositionHist->GetXaxis()->SetRangeUser(0,0.60);
    detPositionHist->GetYaxis()->SetRangeUser(0,0.64);
    detPositionHist->GetZaxis()->SetRangeUser(0,0.14);
    detPositionHist->GetZaxis()->SetLabelSize(0.02);
    
    //Code to draw the histograms to the canvas (Displays data to user)
    c1->cd(1);
    sigmaEventHist->Draw();
    c1->cd(2);
    sigmaPedHist->Draw();
    c1->cd(3);
    chiSquareHistPed->Draw();
    c1->cd(4);
    chiSquareHistSPE->Draw();
    c1->cd(5);
    peakValleyRatioHist->Draw();
    c1->cd(6);
    gain1DHist->Draw();
    
    c2->cd(1);
    gain1DHist->Draw();
    
    gain1DHist->GetXaxis()->SetTitle("Gain");
    gain1DHist->GetXaxis()->CenterTitle();
    gain1DHist->GetYaxis()->SetTitle("Events");
    gain1DHist->GetYaxis()->CenterTitle();

    //Set up File I/O
    for(Int_t whichGantry = 0; whichGantry < 2; whichGantry++){ //loops twice, once for data from each gantry
	if(whichGantry==0){
	    TFile* f = new TFile("../../rootfiles/out_run0" + run0 + ".root");
	} else{
	    TFile* f = new TFile("../../rootfiles/out_run0" + run1 + ".root");
	} 

        //Creates pointer to TTree contained in the input file and pointers to its relevant branches
        TTree* T = (TTree*)f->Get("scan_tree");
        TBranch *branch = T->GetBranch("ADC0_voltage");
        TBranch *points = T->GetBranch("num_points");
	if(whichGantry==0){
            TBranch *gantry_x = T->GetBranch("gantry0_x");
            TBranch *gantry_y = T->GetBranch("gantry0_y");
	    TBranch *gantry_z = T->GetBranch("gantry0_z");
            TBranch *gantry_tilt = T->GetBranch("gantry0_tilt");
            TBranch *gantry_rot = T->GetBranch("gantry0_rot");
	    TBranch *phidg_tilt = T->GetBranch("phidg0_tilt");
	} else{
            TBranch *gantry_x = T->GetBranch("gantry1_x");
            TBranch *gantry_y = T->GetBranch("gantry1_y");
            TBranch *gantry_tilt = T->GetBranch("gantry1_tilt");
            TBranch *gantry_rot = T->GetBranch("gantry1_rot");
	    TBranch *phidg_tilt = T->GetBranch("phidg1_tilt");
	}
    
        //defines total number of entries for indexing bound
        numEntries = branch->GetEntries();
    
       //Sets where the value read in by GetEntry() is stored
        gantry_x->SetAddress(&xposition);
        gantry_y->SetAddress(&yposition);
	gantry_z->SetAddress(&zposition);
        gantry_tilt->SetAddress(&gantrytilt);
        gantry_rot->SetAddress(&gantryrot);
        points->SetAddress(&val_points);
        branch->SetAddress(arr);
        branch->SetAutoDelete(kFALSE); 

	if(whichGantry==0){
	    phidg_tilt->SetAddress(&phidg0tilt);
	} else{
	    phidg_tilt->SetAddress(&phidg1tilt);
	}   
    
        //looping code to iterate through the TTree Structure
        for(Long64_t i = 0; i < numEntries; i++){
	
	    T->GetEntry(i);

	    if( !(whichGantry==0 && xposition<=0.001 && (yposition<=0.001 || yposition>0.665))
                && !(whichGantry==1 && xposition>0.646 && (yposition<=0.001 || yposition>0.665)) ) {
	
	        for(Int_t k = 0; k < val_points; k++){
	            smallHist->Fill(arr[k]);
	        }
	
	        //Insert Code to analyze smallHistogram here
                binmax = smallHist->GetMaximumBin();
                x_pos = smallHist->GetXaxis()->GetBinCenter(binmax);
	
	        p_lo_bound = x_pos - spread;
	        p_hi_bound = x_pos + spread;
	
	        //Creates fit objects for gaussians on the histogram smallHist
	        TF1 *fPed = new TF1("pedFit", "gaus", p_lo_bound, p_hi_bound);
	        TF1 *fEvent = new TF1("eventFit","gaus",e_lo_bound, e_hi_bound);
	
	        smallHist->Fit(fPed,"RQ0");
	        smallHist->Fit(fEvent,"RQ0");
	
	        TF1 *fInter = new TF1("interFit","abs(eventFit - pedFit)", 180, 230);
	
	        meanEvent = fEvent->GetParameter(1);
	        meanPed = fPed->GetParameter(1);
	
	        sigmaEvent = fEvent->GetParameter(2);
	        sigmaPed = fPed->GetParameter(2);
	
	        constantEvent = fEvent->GetParameter(0);
	        constantPed = fPed->GetParameter(0);
	
	        //Code to fill masterHist with data from smallHist	
	        sigmaPedHist->Fill(sigmaPed);
	
	        if(meanEvent>=meanPed){
	            sigmaEventHist->Fill(sigmaEvent);
                }

		//std::cout<<xposition<<" "<<yposition<<" "<<zposition<<" "<<gantryrot<<" "<<gantrytilt<<endl; //For debugging purposes

		if(whichPlot==PMT_ABS_COORD){
	            //Code to calculate point of incidence of laser beam on PMT:
                    gantryrot = (gantryrot - 3)*PI/180; //convert to radians
		    gantrytilt *= PI/180;

	            if(whichGantry==0){
		         Double_t sum = 0.0;
			 Long64_t j = 0;
			 for(j = 0; phidg0tilt[j] != 0 || j >= SIZE; j++){
			     sum += phidg0tilt[j];
			 }
			 if(j==0)
			     j=1;
			 averagePhidgTilt = sum/j;
                         //std::cout<<"Gantry0_Tilt = "<<gantrytilt*180/PI<<"; Phidg0_Tilt = "<<averagePhidgTilt<<"; Gantry0_Rot = "<<gantryrot*180/PI<<endl;//For debugging purposes
			 // gantrytilt = (averagePhidgTilt + 1.8)*PI/180; //1.8 degrees correction factor from phidget reading to actual gantry tilt

                        //Corrections for gantry 0 optical box
                        final_offset_x = g0_offset1*cos(gantryrot)*cos(gantrytilt) - g0_offset2*sin(gantryrot) + g0_offset3*cos(gantryrot)*sin(gantrytilt);
                        final_offset_y = g0_offset1*sin(gantryrot)*cos(gantrytilt) + g0_offset2*cos(gantryrot) + g0_offset3*sin(gantryrot)*sin(gantrytilt);
	            }
                    else{
		         Double_t sum = 0.0;
			 Long64_t j = 0;
			 for(j = 0; phidg1tilt[j] != 0 || j >= SIZE; j++){
			     sum += phidg1tilt[j];
			     // std::cout<<"Phidg1_Tilt = "<<phidg1tilt[j]<<endl;
			 }
			 if(j==0)
			     j=1;
			 averagePhidgTilt = sum/j;

		         // std::cout<<"Gantry1_Tilt = "<<gantrytilt*180/PI<<"; Phidg1_Tilt = "<<averagePhidgTilt<<endl;//For debugging purposes

			 //gantrytilt = (averagePhidgTilt - 1.8)*PI/180; //2.6 degrees correction factor from phidget reading to actual gantry tilt
			 //To be precise, phidget_tilt angle correction seems to vary with rotation angle. . . 

                        //Corrections for gantry 1 optical box
			gantryrot += PI;
		        final_offset_x = g1_offset1*cos(gantryrot)*cos(gantrytilt) - g1_offset2*sin(gantryrot) + g1_offset3*cos(gantryrot)*sin(gantrytilt);
                        final_offset_y = g1_offset1*sin(gantryrot)*cos(gantrytilt) + g1_offset2*cos(gantryrot) + g1_offset3*sin(gantryrot)*sin(gantrytilt);
	            }
	
                    //Calculating the point of incidence of laser beam
	            if(beamLength*cos(gantrytilt)<0){
		        beamLength *= -1; //making sure that the value of beamLength*cos(gantrytilt) is always positive
                                          //to ensure that the sign of the x and y components of the beam trajectory
                                          //are only dictated by the rotation angle of gantry0
	            }
	            x_inc = xposition + final_offset_x + beamLength*cos(gantryrot)*cos(gantrytilt);
	            y_inc = yposition + final_offset_y + beamLength*sin(gantryrot)*cos(gantrytilt);

		    x = x_inc;
		    y = y_inc;
		    //std::cout<<x<<" "<<y<<" "<<endl; //For debugging purposes
		}

		else{ //Plots of gantry position
		    x = xposition;
		    y = yposition;
		}

	        //Code to Verify Position vs. PMT Measurement
	        if((fEvent->GetNDF()!=0) && (fPed->GetNDF()!=0)){
	    
	            xVal = fInter->GetMinimumX();
	            yVal = fEvent->Eval(xVal, 0, 0, 0);
	            yPeak = fEvent->GetMaximum(xVal, xVal + 100, 1.E-10, 100, false);
	    
	            if(meanEvent - meanPed < 40 && meanEvent - meanPed >= 0 && smallHist->Integral(70,230)/val_points > 0.05){

		        gainPositionHist->Fill(roundToThousandths(x), roundToThousandths(y), meanEvent - meanPed);
		        detPositionHist->Fill(roundToThousandths(x), roundToThousandths(y), (float)smallHist->Integral(70, 230)/val_points);
		        peakValleyRatioHist->Fill(yPeak/yVal);
		        gain1DHist->Fill((meanEvent - meanPed));
		        binCheckHist->Fill(roundToThousandths(x),roundToThousandths(y));
		        chiSquareHistSPE->Fill((fEvent->GetChisquare())/(fEvent->GetNDF()));
		        chiSquareHistPed->Fill((fPed->GetChisquare())/(fPed->GetNDF()));

	            }
	    
	            else{

		        gainPositionHist->Fill(roundToThousandths(x), roundToThousandths(y),1);
		        detPositionHist->Fill(roundToThousandths(x), roundToThousandths(y), 0.0001);
		        peakValleyRatioHist->Fill(0.000);
		        binCheckHist->Fill(roundToThousandths(x),roundToThousandths(y));

	            }
	    
	            //Prepares small histogram (local) to be written to again for the next entry of data	    
	            smallHist->Reset();
	    
	        }
	    }
	
        }
    }
    
    gainPositionHist->Divide(binCheckHist);
    c2->cd(1);
    gainPositionHist->Draw("colz");
    
    detPositionHist->Divide(binCheckHist);
    c3->cd(1);
    detPositionHist->Draw("colz");
    
    c3->SetRightMargin(0.5);
    c2->SetRightMargin(0.5);
    
    c2->Update();
    c3->Update();
       
}
