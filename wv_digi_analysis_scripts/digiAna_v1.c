/**
 * Initial Test of Waveform Digitizer Analysis
 *
 *
 **/

#include "TAttFill.h"
#include "THStack.h"
#include "TTree.h"
#include "TBranch.h"
#include "time.h"
#include "stdio.h"
#include "unistd.h"
#include <iostream>
#include <sstream>
#include "TGraph.h"
#include "TH1F.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TH2D.h"
#include "TColor.h"
#include "TStyle.h"
#include "TString.h"
#include "TMath.h"

Double_t roundToThousandths(Double_t x){

	x*=1000;
	return floor(x+0.5)/1000;

}

Double_t pulseFitFunc(Double_t * x, Double_t * par){

    Double_t g1 = 0.0;

    Double_t arg1 = (par[2] != 0.0) ? (x[0] - par[1])/(par[2]) : 0.0;

    g1 = 8135.7 + par[0]*exp(-0.5*arg1*arg1);

    return g1;

}

void digiAna_v1(){

    const double PI = 3.141592653589793238463;
    const int GANTRY_POS = 0;
    const int PMT_ABS_COORD = 1;
    const double voltageConversion = 2000.0/(50*16384);

    TString runNumber;

    std::cout<<"\nEnter run number for scan using Gantry 1:"<<endl;
    
    std::cin>>runNumber;

    //Set up Display Canvas etc.

    /*============================================================================================
    / Long Section of Histogram Creation, Titles and Axes setup
    / ==========================================================================================*/

    
    //Canvases for test data fitting
    Double_t w = 800;
    Double_t h = 600;
    TCanvas *c1 = new TCanvas("c1","Results of Gaussian Fit to Pedestal", w, h);
    TCanvas *c2 = new TCanvas("c2","2d Correlation Between Integral and Minimum Bin", w, h);
    TCanvas *c3 = new TCanvas("c3","Simulated ADC Spec", w, h);
    TCanvas *c4 = new TCanvas("c4","Waveform Pre-Processing", w, h);
    TCanvas *c5 = new TCanvas("c5","Waveform Post-Processing", w, h);
    c1->Divide(1,1);
    c2->Divide(1,1);
    c3->Divide(1,1);
    c4->Divide(1,1);
    c5->Divide(1,1);

    //Canvases for PMT positional data
    TCanvas *c6 = new TCanvas("c6","Results of Gaussian Fit to Pedestal", w, h);
    TCanvas *c7 = new TCanvas("c7","Plot of Positional Data from PMT Run",800,800);
    TCanvas *c8 = new TCanvas("c8","Plot of Positional Data from PMT Run",800,800);
    TCanvas *c9 = new TCanvas("c9","Plot of Positional Data from PMT Run",800,800);    
    c6->Divide(2,3);
    c7->Divide(1,1);
    c8->Divide(1,1);
    c9->Divide(1,1);

    //Create Histograms for 1d variables
    TH1F* integratedWaveFormHist = new TH1F("Plot of Minimum Bin Location", "integral", 1000, 0, 1000);
    TH1F* waveFitHist = new TH1F("Difference between Raw Integral and Integral of Fitting Function", "wfHist", 100, -2, 2);
    TH1D* adcSpecHist = new TH1D("Plot of simulated ADC values", "ADC", 80 , -4, 1);
    TH2F* doubleHist = new TH2F("Plot of integral vs rise time", "minbins", 500, 0, 12, 400, -4, 1);
    TH1F* processedWave = new TH1F("ProcessedWaveForm", "procWave", 1000, 0, 1000);
    TH1F *smoothHist = new TH1F("Smoothed Plot of Charge Distribution","Smoothed Spectrum", 300, 0, 12);
    TH1F *pulseFitHist = new TH1F("Plot of Pulse Integral", "PulseSpec", 80, -4, 1);

    //Create PMT Positional Histograms
    
    //NEED TO EDIT THESE PARAMETERS TO CHANGE RESOLUTION!                                                    

    //1cm Resolution                                                                                         
    //TH2D *gainPositionHist = new TH2D("gainHist", gainPlotTitle, 75, -0.005, 0.745, 75, -0.005, 0.745);    
    //TH2D *detPositionHist = new TH2D("detHist", detPlotTitle, 75, -0.005, 0.745, 75, -0.005, 0.745);       
    //TH2D *binCheckHist = new TH2D("binningCheck","PositionBinningHisto" ,75, -0.005, 0.745, 75, -0.005, 0.745);                               
    //TH2D *pvRatioHist = new TH2D("pvRatioHist","pvRatioHist",75,-0.005,0.745,75,-0.005,0.745);                                              
    
    TH2D *gainPositionHist = new TH2D("gainHist", gainPlotTitle,149, -0.0025, 0.7475,149, -0.0025, 0.7475);
    TH2D *detPositionHist = new TH2D("detHist", detPlotTitle, 149, -0.0025, 0.7475, 149, -0.0025, 0.7475);
    TH2D *binCheckHist = new TH2D("binningCheck","PositionBinningHisto", 149, -0.0025, 0.7475, 149, -0.0025, 0.7475);
    TH2D *pvRatioHist = new TH2D("pvRatioHist","pvRatioHist",149,-0.0025,0.7475,149,-0.0025,0.7475);

    //Segev BenZvi's Colour palette (http://ultrahigh.org/2007/08/20/making-pretty-root-color-palettes/)                                          
    const int NRGBs = 5;
    const int NCont = 255;
    
    double stops[NRGBs] = { 0.00, 0.34, 0.61, 0.84, 1.00 };
    double red[NRGBs]   = { 0.00, 0.00, 0.87, 1.00, 0.51 };
    double green[NRGBs] = { 0.00, 0.81, 1.00, 0.20, 0.00 };
    double blue[NRGBs]  = { 0.51, 1.00, 0.12, 0.00, 0.00 };
    TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
    gStyle->SetNumberContours(NCont);
    
    // Pad & Canvas Options
    gStyle->SetPadColor(10);
    gStyle->SetPadTickX(1);
    gStyle->SetPadTickY(1);
    gStyle->SetCanvasColor(10);
    gStyle->SetOptStat(0);
    gStyle->SetPadGridX(kTRUE);
    gStyle->SetPadGridY(kTRUE);
    
    // Frame Options
    gStyle->SetFrameLineWidth(1);
    gStyle->SetFrameFillColor(10);
    
    // Marker Options
    gStyle->SetMarkerStyle(20);
    gStyle->SetMarkerSize(0.8);
    
    gStyle->SetTitleSize(0.05,"x");//X-axis title size
    gStyle->SetTitleSize(0.05,"y");
    gStyle->SetTitleSize(0.05,"z");
    gStyle->SetLabelSize(0.05,"x");//X-axis title size
    gStyle->SetLabelSize(0.05,"y");
    gStyle->SetLabelSize(0.05,"z");

    waveFitHist->SetTitle("Charge Difference (Fitted vs. Unfitted)");

    waveFitHist->GetXaxis()->SetTitle("Charge Difference (pC)");
    waveFitHist->GetXaxis()->CenterTitle();
    waveFitHist->GetXaxis()->SetTitleSize(0.04);
    waveFitHist->GetXaxis()->SetLabelSize(0.03);
    
    waveFitHist->GetYaxis()->SetTitle("Count");
    waveFitHist->GetYaxis()->SetTitleSize(0.04);
    waveFitHist->GetYaxis()->SetTitleOffset(1.2);
    waveFitHist->GetYaxis()->SetLabelSize(0.03);
    waveFitHist->GetYaxis()->CenterTitle();
    
    integratedWaveFormHist->SetTitle("Position of Minimum Bin");
    
    integratedWaveFormHist->GetXaxis()->SetTitle("Bin Number (Time ns)");
    integratedWaveFormHist->GetXaxis()->CenterTitle();
    integratedWaveFormHist->GetXaxis()->SetTitleSize(0.04);
    integratedWaveFormHist->GetXaxis()->SetLabelSize(0.03);
    
    integratedWaveFormHist->GetYaxis()->SetTitle("Count");
    integratedWaveFormHist->GetYaxis()->SetTitleSize(0.04);
    integratedWaveFormHist->GetYaxis()->SetTitleOffset(1.2);
    integratedWaveFormHist->GetYaxis()->SetLabelSize(0.03);
    integratedWaveFormHist->GetYaxis()->CenterTitle();

    doubleHist->SetTitle("Charge vs. Pulse Width");
    
    doubleHist->GetXaxis()->SetTitle("Pulse FWHM (ns)");
    doubleHist->GetXaxis()->CenterTitle();
    doubleHist->GetXaxis()->SetTitleSize(0.04);
    doubleHist->GetXaxis()->SetLabelSize(0.03);
    
    doubleHist->GetYaxis()->SetTitle("Integrated Charge");
    doubleHist->GetYaxis()->SetTitleSize(0.04);
    doubleHist->GetYaxis()->SetTitleOffset(1.2);
    doubleHist->GetYaxis()->SetLabelSize(0.03);
    doubleHist->GetYaxis()->CenterTitle();

    adcSpecHist->SetTitle("Charge Spectrum");
    
    adcSpecHist->GetXaxis()->SetTitle("Total Integrated Charge (pC)");
    adcSpecHist->GetXaxis()->CenterTitle();
    adcSpecHist->GetXaxis()->SetTitleSize(0.04);
    adcSpecHist->GetXaxis()->SetLabelSize(0.03);
    
    adcSpecHist->GetYaxis()->SetTitle("Count");
    adcSpecHist->GetYaxis()->SetTitleSize(0.04);
    adcSpecHist->GetYaxis()->SetTitleOffset(1.2);
    adcSpecHist->GetYaxis()->SetLabelSize(0.03);
    adcSpecHist->GetYaxis()->CenterTitle();

    pulseFitHist->SetTitle("Charge Spectrum");
    
    pulseFitHist->GetXaxis()->SetTitle("Total Integrated Charge (pC)");
    pulseFitHist->GetXaxis()->CenterTitle();
    pulseFitHist->GetXaxis()->SetTitleSize(0.04);
    pulseFitHist->GetXaxis()->SetLabelSize(0.03);
    
    pulseFitHist->GetYaxis()->SetTitle("Count");
    pulseFitHist->GetYaxis()->SetTitleSize(0.04);
    pulseFitHist->GetYaxis()->SetTitleOffset(1.2);
    pulseFitHist->GetYaxis()->SetLabelSize(0.03);
    pulseFitHist->GetYaxis()->CenterTitle();

    smoothHist->SetTitle("Pulse Width (FWHM) Distribution");
    
    smoothHist->GetXaxis()->SetTitle("FWHM (ns)");
    smoothHist->GetXaxis()->CenterTitle();
    smoothHist->GetXaxis()->SetTitleSize(0.04);
    smoothHist->GetXaxis()->SetLabelSize(0.03);
    
    smoothHist->GetYaxis()->SetTitle("Count");
    smoothHist->GetYaxis()->SetTitleSize(0.04);
    smoothHist->GetYaxis()->SetTitleOffset(1.2);
    smoothHist->GetYaxis()->SetLabelSize(0.03);
    smoothHist->GetYaxis()->CenterTitle();

    //Set Axis Limits to Limits for 4cm Square ADC Incidence Angle Scans
    
    //Make Graph to check if signal dependant on position
    pvRatioHist->SetTitle(pvPlotTitle);
    
    pvRatioHist->GetXaxis()->SetTitle("X Position (m)");
    pvRatioHist->GetXaxis()->CenterTitle();
    pvRatioHist->GetXaxis()->SetTitleSize(0.04);
    pvRatioHist->GetXaxis()->SetLabelSize(0.03);
    
    pvRatioHist->GetYaxis()->SetTitle("Y Position (m)");
    pvRatioHist->GetYaxis()->SetTitleSize(0.04);
    pvRatioHist->GetYaxis()->SetTitleOffset(1.2);
    pvRatioHist->GetYaxis()->SetLabelSize(0.03);
    pvRatioHist->GetYaxis()->CenterTitle();
    
    //Set axis limits
    pvRatioHist->GetXaxis()->SetRangeUser(0, 0.75);
    pvRatioHist->GetYaxis()->SetRangeUser(0, 0.75);
    pvRatioHist->GetZaxis()->SetRangeUser(0,10);
    pvRatioHist->GetZaxis()->SetLabelSize(0.02);
    
    //Make Graph to check if signal dependant on position
    gainPositionHist->SetTitle(gainPlotTitle);
    
    gainPositionHist->GetXaxis()->SetTitle("X Position (m)");
    gainPositionHist->GetXaxis()->CenterTitle();
    gainPositionHist->GetXaxis()->SetTitleSize(0.04);
    gainPositionHist->GetXaxis()->SetLabelSize(0.03);
    
    gainPositionHist->GetYaxis()->SetTitle("Y Position (m)");
    gainPositionHist->GetYaxis()->SetTitleSize(0.04);
    gainPositionHist->GetYaxis()->SetTitleOffset(1.2);
    gainPositionHist->GetYaxis()->SetLabelSize(0.03);
    gainPositionHist->GetYaxis()->CenterTitle();
    
    //Set axis limits
    gainPositionHist->GetXaxis()->SetRangeUser(0, 0.75);
    gainPositionHist->GetYaxis()->SetRangeUser(0,0.75);
    gainPositionHist->GetZaxis()->SetRangeUser(0,35);
    gainPositionHist->GetZaxis()->SetLabelSize(0.02);
        
    //Second graph for detection efficiency
    detPositionHist->SetTitle(detPlotTitle);
    
    detPositionHist->GetXaxis()->SetTitle("X Position (m)");
    detPositionHist->GetXaxis()->CenterTitle();
    detPositionHist->GetXaxis()->SetTitleSize(0.04);
    detPositionHist->GetXaxis()->SetLabelSize(0.03);
    
    detPositionHist->GetYaxis()->SetTitle("Y Position (m)");
    detPositionHist->GetYaxis()->SetTitleSize(0.04);
    detPositionHist->GetYaxis()->SetTitleOffset(1.2);
    detPositionHist->GetYaxis()->SetLabelSize(0.03);
    detPositionHist->GetYaxis()->CenterTitle();
    
    //Set axis limits
    detPositionHist->GetXaxis()->SetRangeUser(0,0.75);
    detPositionHist->GetYaxis()->SetRangeUser(0,0.75);
    detPositionHist->GetZaxis()->SetRangeUser(0,0.14);
    detPositionHist->GetZaxis()->SetLabelSize(0.02);

    /*===============================================================================================================
    / End of Formatting Section
    / Start of Actual Analysis Section
    / =============================================================================================================*/

    //local variable creation for addressing of branch values from TTree
    Int_t arr[200000][80];
    Int_t val_points;
    Int_t numEntries;
    Int_t p_lo_bound;
    Int_t p_hi_bound;
    Int_t e_lo_bound;
    Int_t e_hi_bound;
    Int_t binmax;
    Int_t x_pos;

    Double_t xVal = 0;
    Double_t yVal = 0;
    Double_t xposition, yposition; //gantry0 position
    Double_t gantrytilt;
    Double_t gantryrot;
    Double_t x_inc, y_inc; //point of incidence of laser beam
    Double_t x, y; //point plotted on gain and detection efficiency plots
    Double_t final_offset_x, final_offset_y; //Corrections for optical box
    Double_t beamLength = 0.05; // (26Apr2017): changed from 0.1m to 0.05m

    Double_t gantryXlimit = 0.749;
    Double_t gantryYlimit = 0.696;

    //offset in laser aperture with respect to gantry position
    Double_t g0_offset1 = 0.115;
    Double_t g0_offset2 = 0.042;
    Double_t g0_offset3 = 0.0365;

    Double_t g1_offset1 = 0.115;
    Double_t g1_offset2 = 0.052;
    Double_t g1_offset3 = 0.0395;


    //c4->cd(1);
    
    //Set up File I/O

    char waveFormID[20];
    
    TFile* f = new TFile("~/online/rootfiles/out_run0" + runNumber + ".root");
    
    TH1F* waveFormHist;

    //Creates pointer to TTree contained in the input file and pointers to its relevant branches
    TTree* T = (TTree*)f->Get("scan_tree");
    TBranch *branch = T->GetBranch("ADC0_voltage");
    TBranch *points = T->GetBranch("num_points");
    
    TBranch *gantry_x = T->GetBranch("gantry0_x");
    TBranch *gantry_y = T->GetBranch("gantry0_y");
    TBranch *gantry_tilt = T->GetBranch("gantry0_tilt");
    TBranch *gantry_rot = T->GetBranch("gantry0_rot");

    TBranch *gantry_x = T->GetBranch("gantry1_x");
    TBranch *gantry_y = T->GetBranch("gantry1_y");
    TBranch *gantry_tilt = T->GetBranch("gantry1_tilt");
    TBranch *gantry_rot = T->GetBranch("gantry1_rot");
    
    //defines total number of entries for indexing bound
    numEntries = branch->GetEntries();
    
    //Sets where the value read in by GetEntry() is stored
    gantry_x->SetAddress(&xposition);
    gantry_y->SetAddress(&yposition);
    gantry_tilt->SetAddress(&gantrytilt);
    gantry_rot->SetAddress(&gantryrot);
    points->SetAddress(&val_points);
    branch->SetAddress(arr);
    branch->SetAutoDelete(kFALSE);
    



    Double_t integralValue;
    Int_t integralRange = 40;

    Int_t min_range = 350;
    Int_t max_range = 450;
    Int_t npar = 3;

    TF1 * pulseFitFunc = new TF1("pulseFitFunc", pulseFitFunc, min_range, max_range, npar);

    pulseFitFunc->SetParName(0, "height");
    pulseFitFunc->SetParName(1, "mu");
    pulseFitFunc->SetParName(2, "sigma");
    
    pulseFitFunc->SetParameters(-50.0, 400.0, 4.0);
    
    pulseFitFunc->SetParLimits(0, -200.0, -1.0);
    pulseFitFunc->SetParLimits(1, 350.0, 550.0);
    pulseFitFunc->SetParLimits(2, 0.6, 6.00);

    Float_t integralFitValue;
    
    for(int i = 0; i < 20000; i++){
	
	sprintf(waveFormID, "Chan_0_Wave_%d;1", i);
	waveFormHist = (TH1F*) f->Get(waveFormID);

	integralValue = voltageConversion*(waveFormHist->Integral(80, 80+integralRange) - (integralRange+1)*8135.7);

	integratedWaveFormHist->Fill(waveFormHist->GetMinimumBin());
	//printf("Integrated Value %f\n", integralValue*voltageConversion);
	adcSpecHist->Fill(integralValue);

	if(integralValue < -0.1){
	    
	    printf("%d\n", i);
	    	    
	    waveFormHist->Fit("pulseFitFunc", "FMNO");

	    //if(pulseFitFunc->GetParameter(2) > 5){
	    
		//waveFormHist->Draw();
	    
		//c4->Update();
	    
		//usleep(50000);
	    
	    //}

	    smoothHist->Fill(pulseFitFunc->GetParameter(2)*2.355);

	    integralFitValue = (pulseFitFunc->Integral(300, 500) - 200*8135.7)*voltageConversion;
	    
	    pulseFitHist->Fill(integralFitValue);
	    
	    waveFitHist->Fill(integralFitValue - integralValue);
	    
	    //printf("%f\n",(pulseFitFunc->Integral(300, 500) - 200*8135.7)*voltageConversion);

	    doubleHist->Fill(pulseFitFunc->GetParameter(2)*2.355, integralFitValue);
	    
	}
    }
    
    c5->cd(1);
    smoothHist->Draw();
    
    c4->cd(1);
    waveFitHist->Draw();

    c3->cd(1);

    THStack *hs = new THStack("hs","");

    adcSpecHist->SetFillStyle(3004);
    adcSpecHist->SetFillColor(kBlue);
    hs->Add(adcSpecHist);
    pulseFitHist->SetFillStyle(3005);
    pulseFitHist->SetFillColor(kRed);
    hs->Add(pulseFitHist);

    hs->Draw("nostack");
    
    hs->SetTitle("Charge Spectrum");
    
    hs->GetXaxis()->SetTitle("Total Integrated Charge (pC)");
    hs->GetXaxis()->CenterTitle();
    hs->GetXaxis()->SetTitleSize(0.04);
    hs->GetXaxis()->SetLabelSize(0.03);
    
    hs->GetYaxis()->SetTitle("Count");
    hs->GetYaxis()->SetTitleSize(0.04);
    hs->GetYaxis()->SetTitleOffset(1.2);
    hs->GetYaxis()->SetLabelSize(0.03);
    hs->GetYaxis()->CenterTitle();

   

    c2->cd(1);
    integratedWaveFormHist->Draw();

    c1->cd(1);
    doubleHist->Draw();

    //c1->Update();
        
}
