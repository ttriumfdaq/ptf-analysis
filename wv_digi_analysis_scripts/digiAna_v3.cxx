#include "TAttFill.h"
#include "THStack.h"
#include "TTree.h"
#include "TBranch.h"
#include "time.h"
#include "stdio.h"
#include "unistd.h"
#include <iostream>
#include <sstream>
#include "TGraph.h"
#include "TH1F.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TH2D.h"
#include "TColor.h"
#include "TStyle.h"
#include "TString.h"
#include "TMath.h"
#include "TH1D.h"
#include "TGraph2D.h"

Double_t roundToThousandths(Double_t x){

    x*=1000;
	
    return floor(x+0.5)/1000;

}

// user-made Gaussian fitting functions
// par[0,1,2,3] = multiplicative coefficient(height), mean, spread, DC offset
Double_t pulseFitFunc(Double_t * x, Double_t * par){

    Double_t g1 = 0.0;

    Double_t arg1 = (par[2] != 0.0) ? (x[0] - par[1])/(par[2]) : 0.0;

    g1 = par[0]*exp(-0.5*arg1*arg1);

    return g1;

}

Double_t refPulseFitFunc(Double_t * x, Double_t * par){

    Double_t g1 = 0.0;

    Double_t arg1 = (par[2] != 0.0) ? (x[0] - par[1])/(par[2]) : 0.0;

    g1 = par[3] + par[0]*exp(-0.5*arg1*arg1);

    return g1;

}
  

// global hist for circle fit
TH2D *h2;
TH2D *h2_circ;

// Plot a circle on the histogram h2 which is defined in the main body as a clone of another parameter (in this case det eff)
// This takes the outer bins and places them into a new histogram, then fits a circle on them
// the filtering procedure looks for bins between two threshold values and outside a certain radius from an estimated center position; this aids in filtering
void myFitCircle(Int_t &, Double_t *, Double_t &f, Double_t *par, Int_t){
  //minimisation function computing the sum of squares of residuals
  // play with pe_thr and upper_limit to confine circle fit to intensity values between pe_thr and upper_limit 
  int nbins_x = h2->GetNbinsX();
  int nbins_y = h2->GetNbinsY();
  f = 0;
  double pe_thr = 0.01; //for some reason, for run838 0.4 didn't work..
  double upper_limit = 0.045;
  double radiusGuess = 0.21;
  double xcGuess = 0.39;
  double ycGuess = 0.29;
  for (int i = 0;i<nbins_y;i++) {
    for (int j = 0;j<nbins_x;j++) {
      if (sqrt((h2->GetXaxis()->GetBinCenter(i) - xcGuess)*(h2->GetXaxis()->GetBinCenter(i) - xcGuess) + (h2->GetYaxis()->GetBinCenter(j) - ycGuess)*(h2->GetYaxis()->GetBinCenter(j) - ycGuess)) < radiusGuess) {
	continue;
      }
      // TODO: try to define boundary better?
      // eg. More clever way: Only pick the first bin above threshold?
      if(h2->GetBinContent(i,j) < upper_limit && h2->GetBinContent(i,j) > pe_thr){
	std::cout << i << " " << j << " " << h2->GetBinContent(i,j) << std::endl;

	h2_circ->Fill(h2->GetXaxis()->GetBinCenter(i),
		      h2->GetYaxis()->GetBinCenter(j),1);

	double u = h2->GetXaxis()->GetBinCenter(i) - par[0];
	double v = h2->GetYaxis()->GetBinCenter(j) - par[1];
	double dr = par[2] - TMath::Sqrt(u*u+v*v);
	//std::cout << i << " " << j << " " << dr*dr << std::endl;
	f += dr*dr;
      }
    }
  }
}

void digiAna_v3() {
    
    //Initialize and define global constants *some of these are temporary
    const double PI = 3.141592653589793238463;
    const int GANTRY_POS = 0;
    const int PMT_ABS_COORD = 1;
    const double voltageConversion = 2000.0/(50*16384); //2000 mV, 50 ohm impedance, 14-bit = 16384 possibilities -- converts digital voltage to analog
    
    double monEfficiency;
    double runningAverageTTS = 0;
    int trackNumber = 0;
    int whichGantry;
    int whichPlot;

    TString ttsPlotTitle = "Transit Time Spread (Sigma)";
    TString peakPlotTitle = "Transit Time Peak Position (Mu)";
    TString ttsFitPlotTitle = "Fitted Transit Time Spread (Sigma)";
    TString peakFitPlotTitle = "Fitted Transit Time Peak Position (Mu)";
    TString gainPlotTitle;
    TString detPlotTitle;
    TString pvPlotTitle;
    
    //Gets the run to be analyzed as input from the user
    TString runNumber;
    Int_t analysisType;
    Int_t writeFile;

    std::cout<<"\nEnter run number for scan:"<<std::endl;
    std::cin>>runNumber; //2965 is good data for testing

    std::cout << "\nEnter gantry used: " << std::endl;
    std::cin>>whichGantry;
  
    std::cout<<"\nChoose plot type \n(0 == plot of gantry positions, 1 == PMT position in absolute coordinates):"<<std::endl;
    std::cin>>whichPlot;
    if(whichPlot==GANTRY_POS){
    	gainPlotTitle = "Gain from Pedestal as a Function of Gantry Position";
    	detPlotTitle = "Relative Detection Efficiency as a Function of Gantry Position";
    	pvPlotTitle = "Peak to Valley Ratio as a Function of Gantry Position";
    }
    else{
    	gainPlotTitle = "Gain from Pedestal as a Function of Position on PMT in Absolute Coordinates";
    	detPlotTitle = "Relative Detection Efficiency as a Function of Position on PMT in Absolute Coordinates";
    	pvPlotTitle = "Peak to Valley Ratio as a Function of Position on PMT in Absolute Coordinates";
    }

    std::cout << "\nChoose analysis for charge (0), time (1), or both (2):" <<std::endl;
    std::cin>>analysisType;

    std::cout << "\nWrite a file? (0 = no, 1 = yes)" << std::endl;
    std::cin>>writeFile;

    /*============================================================================================
    / Long Section of Histogram Creation, Titles and Axes setup
    / ==========================================================================================*/

    //Canvases for test data fitting
    
    //Set standard width and height of canvases for data display
    Double_t w = 1600; 
    Double_t h = 1000; 
    
    // currently unused
    // TCanvas *c1 = new TCanvas("c1","ADC Spectrum from fitted Charge", w, h);
    // c1->Divide(1,1);
    TCanvas *c2 = new TCanvas("c2","ADC Spectrum", w, h);
    c2->Divide(1,1);
    TCanvas *c3 = new TCanvas("c3","Time Spectrum", w, h);
    c3->Divide(1,1);
   
    // TCanvas *c4 = new TCanvas("c4","PMT Waveform", w, h);  
    // c4->Divide(1,1);
    // TCanvas *c5 = new TCanvas("c5","Laser Signal Waveform", w, h);
    // c5->Divide(1,1);

    // Raw waveform observation canvas
    TCanvas *c14 = new TCanvas("c14", "Raw waveforms", 800, 800);
    c14->Divide(2,2);

    // Use for observing goodness of fits
    // TCanvas *c6 = new TCanvas("c6","Results of Gaussian Fit to Pedestal", w, h);
    // c6->Divide(2,3);  
 
    //Canvases for PMT positional data

    // Charge canvases
    TCanvas *c7 = new TCanvas("c7","Plot of Positional Data from PMT Run",800,800);
    c7->Divide(1,1);
    TCanvas *c8 = new TCanvas("c8","Plot of Positional Data from PMT Run",800,800);    
    c8->Divide(1,1);
    // TCanvas *c9 = new TCanvas("c9","Bin Check Histogram",800,800); 
    // c9->Divide(1,1);
    
    // Time canvases
    TCanvas *c10 = new TCanvas("c10", "Plot of Positional Data from PMT Run", 800, 800);
    c10->Divide(1,1);
    TCanvas *c11 = new TCanvas("c11", "Plot of Positional Data from PMT Run", 800, 800);
    c11->Divide(1,1);
    
    // Monitor PMT and other calibration canvases
    TCanvas *c15 = new TCanvas("c15", "Monitor PMT Integrated Spectrum", 800, 800);
    c15->Divide(1,1);
    TCanvas *c16 = new TCanvas("c16", "Count Histograms", 800,800);
    c16->Divide(2,2);
    // Use for seeing errors if desired
    // TCanvas *c17 = new TCanvas("c17", "Error Plots", 800, 800);
    // c17->Divide(2,2);
    TCanvas *c18 = new TCanvas("c18", "Trigger Graph", 800, 800);
    c18->Divide(2,2);

    TCanvas *c19 = new TCanvas("c19", "Monitor PMT Transit Time Distribution", 800, 800);
    c19->Divide(1,1);
   
    // ========================================================================
    //Create Histograms for 1d variables
    // ========================================================================
    
    //Histograms for channel 0 and channel 1 waveforms (digitizer data histograms)
    TH1D* waveFormHist0 = new TH1D("WaveformHistogram0", "Histogram of Raw WaveForm 0", 70, 0, 70); // TH1D(name, title, # of bins, left edge, right edge) 
    TH1D* waveFormHist1 = new TH1D("WaveformHistogram1","Histogram of Raw Waveform 1", 67, 3, 70); 

    // Histogram for channel 3 and 5 waveforms (G0 and G1 Monitor PMT)
    TH1D* waveFormMonG0 = new TH1D("WaveformMonG0", "Histogram of Raw Waveform 3", 70, 0, 70);
    TH1D* waveFormMonG1 = new TH1D("WaveformMonG1", "Histogram of Raw Waveform 5",70, 0, 70);
    
    //Plot showing charge distribution
    int numChargeBins = 117;
    double adcSpecLowerBound = -9.5;
    double adcSpecUpperBound = 3.5;
    double monSpecLowerBound = -5;
    double monSpecUpperBound = 2;
    TH1D* adcSpecHist = new TH1D("Plot of Actual ADC values", "ADC", numChargeBins, adcSpecLowerBound, adcSpecUpperBound);
    TH1D* waveFitHist = new TH1D("Plot of Fitted ADC values", "wfHist", numChargeBins, adcSpecLowerBound, adcSpecUpperBound );
    TH1F* smoothHist = new TH1F("smoothHist", "Smoothed Spectrum", numChargeBins,  adcSpecLowerBound, adcSpecUpperBound); // smoothed ADC spectrum
    TH1D* monSpecHist = new TH1D("Plot of Monitor PMT ADC values", "monADC", numChargeBins, monSpecLowerBound, monSpecUpperBound);

    //Histogram for timing correction between trigger pulse and actual PMT Pulse time *to show correction for the latching uncertainty*
    TH2F* doubleHist = new TH2F("Plot of integral vs rise time", "minbins", 55, -6, 3, 55, 240, 350);

    //Histogram for timing data for a single point
    TH1D* timeHist = new TH1D("Plot of Transit Time", "TDCHist", 30, 260, 320); 
    TH1D* timeFitHist = new TH1D("Plot of Fitted Transit Time","TDCFit", 30, 240,300);
    TH1F *fwhmHist = new TH1F("fwhmHist","TTS (fwhm ns)",1000,-100,300);
    TH1F *posHist = new TH1F("posHist","Position of TTS center",1000,-100,400);
    TH1D* monTimeHist = new TH1D("Plot of Monitor PMT Transit Time", "monTDC", 30, 140, 200);

    // Bunch of histograms that track fitting parameters if desired
    TH1F *chiSquareHistTTS = new TH1F("ChiSquareTTS","Chi-Square / NDoF Distro", 75, -1, 20);
    TH1F *chiSquareHistWave = new TH1F("ChiSquareWave", "Chi-Square / NDoF Distro", 75, -1, 10); 
    TH1F *sigmaEventHist = new TH1F("SigmaEventHist","Width (SPE Peak Gaussian Fit)(Sigma)",100,0,5);
    TH1F *sigmaPedHist = new TH1F("SigmaPedHist","Width (Pedestal Gaussian Fit)(Sigma)",100,0,2);
    TH1F *chiSquareHistSPE = new TH1F("ChiSquareSPE","Chi-Square / NDoF Distro for SPE Peak Gaussian Fit", 75, -1, 5);
    TH1F *chiSquareHistPed = new TH1F("ChiSquarePED","Chi-Square / NDoF Distro for ADC Pedestal Gaussian Fit",75,-1,15);
    TH1F *peakValleyRatioHist = new TH1F("pvRatio","Peak to Valley Ratio of ADC Distro", 120, 0, 12);
    TH1F *gain1DHist = new TH1F("gain1DHist","Gain of PMT in ADC Counts", 50, 0, 20); 

    /* -----------------Create PMT Positional Histograms-----------------------------
    / ttsPositionHist: Transit Time Spread as a function of position
    / peakPositionHist: Transit Time (peak of Gaussian fit of timing distro) as a function of position)
    / ttsFitPositionHist: TTS when fitting Gaussians to every sample
    / peakFitPositionhist: TT when fitting Gaussians to every sample
    /
    / gainPositionHist: Gain as a Function of position
    / detPositionHist: Detection Efficiency as a function of position
    / binCheckHist: Makes sure no errors from double binning (precautionary)
    / pvRatioHist: Peak to Valley Ratio as a Function of position
    /
    / monPositionHist: Detection Efficiency of Monitor PMT as a function of position
    / monCountHist: Fraction of triggers that produce a monitor PMT pulse as a function of position
    / pmtCountHist: Fraction of triggers that produce a main PMT pulse as a function of position
    / simulCountHist:
    / -----------------------------------------------------------------------------*/

    //NEED TO EDIT THESE PARAMETERS TO CHANGE RESOLUTION!                                                    

    //2cm Resolution   
    // TH2D *ttsPositionHist = new TH2D("ttsPositionHist", ttsPlotTitle, 37, 0.0, 0.74, 37, 0.0, 0.74);
    // TH2D *peakPositionHist = new TH2D("peakPositionHist", peakPlotTitle, 37, 0.0, 0.74, 37, 0.0, 0.74);
    // TH2D *ttsFitPositionHist = new TH2D("ttsFitPositionHist", ttsPlotTitle, 37, 0.0, 0.74, 37, 0.0, 0.74);
    // TH2D *peakFitPositionHist = new TH2D("peakFitPositionHist", peakPlotTitle, 37, 0.0, 0.74, 37, 0.0, 0.74);

    // TH2D *gainPositionHist = new TH2D("gainHist", gainPlotTitle, 37, 0.0, 0.74, 37, 0.0, 0.74);    
    // TH2D *detPositionHist = new TH2D("detHist", detPlotTitle, 37, 0.0, 0.74, 37, 0.0, 0.74);      
    // TH2D *binCheckHist = new TH2D("binningCheck","PositionBinningHisto" ,37, 0.0, 0.74, 37, 0.0, 0.74);                           
    // TH2D *pvRatioHist = new TH2D("pvRatioHist","pvRatioHist", 37, 0.0, 0.74, 37, 0.0, 0.74);   


    // // New histograms for monitor PMT data and comparison data
    // TH2D *monPositionHist = new TH2D("monPositionHist", "monPositionHist",  37, 0.0, 0.74, 37, 0.0, 0.74);   
    // TH2D *monCountHist = new TH2D("monCountHist", "monCountHist", 37, 0.0, 0.74, 37, 0.0, 0.74); 
    // TH2D *pmtCountHist = new TH2D("pmtCountHist", "pmtCountHist", 37, 0.0, 0.74, 37, 0.0, 0.74); 
    // TH2D *simulCountHist = new TH2D("simulCountHist", "simulCountHist", 37, 0.0, 0.74, 37, 0.0, 0.74);


    //1cm Resolution                                                                                         
    TH2D *gainPositionHist = new TH2D("gainHist", gainPlotTitle, 75, -0.005, 0.745, 75, -0.005, 0.745);    
    TH2D *detPositionHist = new TH2D("detHist", detPlotTitle, 75, -0.005, 0.745, 75, -0.005, 0.745);     
    TH2D *binCheckHist = new TH2D("binningCheck","PositionBinningHisto" ,75, -0.005, 0.745, 75, -0.005, 0.745);                               
    TH2D *pvRatioHist = new TH2D("pvRatioHist","pvRatioHist",75,-0.005,0.745,75,-0.005,0.745);          

    TH2D *ttsPositionHist = new TH2D("ttsPositionHist", ttsPlotTitle, 75, -0.005, 0.745, 75, -0.005, 0.745);
    TH2D *peakPositionHist = new TH2D("peakPositionHist", peakPlotTitle, 75, -0.005, 0.745, 75, -0.005, 0.745);
    TH2D *ttsFitPositionHist = new TH2D("ttsFitPositionHist", ttsPlotTitle, 75, -0.005, 0.745, 75, -0.005, 0.745);
    TH2D *peakFitPositionHist = new TH2D("peakFitPositionHist", peakPlotTitle, 75, -0.005, 0.745, 75, -0.005, 0.745);

    TH2D *monPositionHist = new TH2D("monPositionHist", "monPositionHist", 75, -0.005, 0.745, 75, -0.005, 0.745);    
    TH2D *monCountHist = new TH2D("monCountHist", "monCountHist", 75, -0.005, 0.745, 75, -0.005, 0.745); 
    TH2D *pmtCountHist = new TH2D("pmtCountHist", "pmtCountHist", 75, -0.005, 0.745, 75, -0.005, 0.745); 
    TH2D *simulCountHist = new TH2D("simulCountHist", "simulCountHist", 75, -0.005, 0.745, 75, -0.005, 0.745); 

    // TH2D *gainErrorHist = new TH2D("gainErrorHist", "gainErrorHist", 75,-0.005,0.745,75,-0.005,0.745);          
    // TH2D *detErrorHist = new TH2D("detErrorHist", "detErrorHist", 75,-0.005,0.745,75,-0.005,0.745);          
    // TH2D *peakErrorHist = new TH2D("peakErrorHist", "peakErrorHist", 75, -0.005, 0.745, 75, -0.005, 0.745);
    // TH2D *ttsErrorHist = new TH2D("ttsErrorHist", "ttsErrorHist", 75, -0.005, 0.745, 75, -0.005, 0.745);
    
    //5mm Resolution
    // TH2D *gainPositionHist = new TH2D("gainHist", "GainHisto",149, -0.0025, 0.7475,149, -0.0025, 0.7475);
    // TH2D *detPositionHist = new TH2D("detHist", "detEffHisto", 149, -0.0025, 0.7475, 149, -0.0025, 0.7475);
    // TH2D *binCheckHist = new TH2D("binningCheck","PositionBinningHisto", 149, -0.0025, 0.7475, 149, -0.0025, 0.7475);
    // TH2D *pvRatioHist = new TH2D("pvRatioHist","pvRatioHist",149,-0.0025,0.7475,149,-0.0025,0.7475);

    // TH2D *ttsPositionHist = new TH2D("ttsPositionHist", ttsPlotTitle, 149, -0.0025, 0.7475,149, -0.0025, 0.7475);
    // TH2D *peakPositionHist = new TH2D("peakPositionHist", peakPlotTitle,149, -0.0025, 0.7475,149, -0.0025, 0.7475);
    // TH2D *ttsFitPositionHist = new TH2D("ttsFitPositionHist", ttsPlotTitle, 149, -0.0025, 0.7475,149, -0.0025, 0.7475);
    // TH2D *peakFitPositionHist = new TH2D("peakFitPositionHist", peakPlotTitle, 149, -0.0025, 0.7475,149, -0.0025, 0.7475);

    // TH2D *monPositionHist = new TH2D("monPositionHist", "monPositionHist", 149, -0.0025, 0.7475,149, -0.0025, 0.7475);
    // TH2D *monCountHist = new TH2D("monCountHist", "monCountHist",149, -0.0025, 0.7475,149, -0.0025, 0.7475);
    // TH2D *pmtCountHist = new TH2D("pmtCountHist", "pmtCountHist", 149, -0.0025, 0.7475,149, -0.0025, 0.7475);
    // TH2D *simulCountHist = new TH2D("simulCountHist", "simulCountHist", 149, -0.0025, 0.7475,149, -0.0025, 0.7475);

    //2mm Resolution
    // TH2D *gainPositionHist = new TH2D("gainHist", gainPlotTitle,390, -0.0005, 0.7525, 390, -0.0005, 0.7525);    
    // TH2D *detPositionHist = new TH2D("detHist", detPlotTitle, 390, -0.0005, 0.7525, 390, -0.0005, 0.7525);     //   325, -0.005, 0.745, 325, -0.005, 0.745);    
    // TH2D *binCheckHist = new TH2D("binningCheck","PositionBinningHisto" ,375, -0.005, 0.745, 375, -0.005, 0.745);                               
    // TH2D *pvRatioHist = new TH2D("pvRatioHist","pvRatioHist",375,-0.005,0.745,375,-0.005,0.745);          

    // TH2D *ttsPositionHist = new TH2D("ttsPositionHist", ttsPlotTitle, 425, -0.005, 0.745, 425, -0.005, 0.745);
    // TH2D *peakPositionHist = new TH2D("peakPositionHist", peakPlotTitle, 375, -0.005, 0.745, 375, -0.005, 0.745);
    // TH2D *ttsFitPositionHist = new TH2D("ttsFitPositionHist", ttsPlotTitle, 375, -0.005, 0.745, 375, -0.005, 0.745);
    // TH2D *peakFitPositionHist = new TH2D("peakFitPositionHist", peakPlotTitle, 375, -0.005, 0.745, 375, -0.005, 0.745);

    // TH2D *monPositionHist = new TH2D("monPositionHist", "monPositionHist",  375, -0.005, 0.745, 375, -0.005, 0.745);    
    // TH2D *monCountHist = new TH2D("monCountHist", "monCountHist",  375, -0.005, 0.745, 375, -0.005, 0.745); 
    // TH2D *pmtCountHist = new TH2D("pmtCountHist", "pmtCountHist",  375, -0.005, 0.745, 375, -0.005, 0.745); 
    // TH2D *simulCountHist = new TH2D("simulCountHist", "simulCountHist",375, -0.005, 0.745, 375, -0.005, 0.745); 

    /*============================================================================================
    / Long Section of Colormap definitions (Beautification of Plots)
    / ==========================================================================================*/

    //Segev BenZvi's Colour palette (http://ultrahigh.org/2007/08/20/making-pretty-root-color-palettes/)                                          
    const int NRGBs = 5;
    const int NCont = 255;
    
    double stops[NRGBs] = { 0.00, 0.34, 0.61, 0.84, 1.00 };
    double red[NRGBs]   = { 0.00, 0.00, 0.87, 1.00, 0.51 };
    double green[NRGBs] = { 0.00, 0.81, 1.00, 0.20, 0.00 };
    double blue[NRGBs]  = { 0.51, 1.00, 0.12, 0.00, 0.00 };
    TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
    gStyle->SetNumberContours(NCont);
    
    // Pad & Canvas Options
    gStyle->SetPadColor(10);
    gStyle->SetPadTickX(1);
    gStyle->SetPadTickY(1);
    gStyle->SetCanvasColor(10);
    gStyle->SetOptStat(0);
    gStyle->SetPadGridX(kTRUE);
    gStyle->SetPadGridY(kTRUE);
    
    // Frame Options
    gStyle->SetFrameLineWidth(1);
    gStyle->SetFrameFillColor(10);
    
    // Marker Options
    gStyle->SetMarkerStyle(20);
    gStyle->SetMarkerSize(0.8);
    
    gStyle->SetTitleSize(0.05,"x");//X-axis title size
    gStyle->SetTitleSize(0.05,"y");
    gStyle->SetTitleSize(0.05,"z");
    gStyle->SetLabelSize(0.05,"x");//X-axis title size
    gStyle->SetLabelSize(0.05,"y");
    gStyle->SetLabelSize(0.05,"z");

    /*============================================================================================
    / Long Section of Axes Naming and Plot Tinkering (most formatting is in here)
    / ==========================================================================================*/

    waveFitHist->SetTitle("Charge Difference (Fitted vs. Unfitted)");
    waveFitHist->GetXaxis()->SetTitle("Charge Difference (pC)");
    waveFitHist->GetXaxis()->CenterTitle();
    waveFitHist->GetXaxis()->SetTitleSize(0.04);
    waveFitHist->GetXaxis()->SetLabelSize(0.03);
    waveFitHist->GetYaxis()->SetTitle("Count");
    waveFitHist->GetYaxis()->SetTitleSize(0.04);
    waveFitHist->GetYaxis()->SetTitleOffset(1.2);
    waveFitHist->GetYaxis()->SetLabelSize(0.03);
    waveFitHist->GetYaxis()->CenterTitle();
    waveFitHist->GetZaxis()->SetRangeUser(0,300);

    timeFitHist->SetTitle("Position of Minimum Bin");
    timeFitHist->GetXaxis()->SetTitle("Bin Number (Time ns)");
    timeFitHist->GetXaxis()->CenterTitle();
    timeFitHist->GetXaxis()->SetTitleSize(0.04);
    timeFitHist->GetXaxis()->SetLabelSize(0.03);
    timeFitHist->GetYaxis()->SetTitle("Count");
    timeFitHist->GetYaxis()->SetTitleSize(0.04);
    timeFitHist->GetYaxis()->SetTitleOffset(1.2);
    timeFitHist->GetYaxis()->SetLabelSize(0.03);
    timeFitHist->GetYaxis()->CenterTitle();

    doubleHist->SetTitle("Integrated Charge vs. Transit Time");
    doubleHist->GetXaxis()->SetTitle("Integrated Charge (pC)");
    doubleHist->GetXaxis()->CenterTitle();
    doubleHist->GetXaxis()->SetTitleSize(0.04);
    doubleHist->GetXaxis()->SetLabelSize(0.03);
    doubleHist->GetYaxis()->SetTitle("PMT Pulse Transit Time (ns)");
    doubleHist->GetYaxis()->SetTitleSize(0.04);
    doubleHist->GetYaxis()->SetTitleOffset(1.2);
    doubleHist->GetYaxis()->SetLabelSize(0.03);
    doubleHist->GetYaxis()->CenterTitle();

    adcSpecHist->SetTitle("Charge Distribution");
    adcSpecHist->GetXaxis()->SetTitle("Total Integrated Charge (pC)");
    adcSpecHist->GetXaxis()->CenterTitle();
    adcSpecHist->GetXaxis()->SetTitleSize(0.04);
    adcSpecHist->GetXaxis()->SetLabelSize(0.03);
    adcSpecHist->GetYaxis()->SetTitle("Count");
    adcSpecHist->GetYaxis()->SetTitleSize(0.04);
    adcSpecHist->GetYaxis()->SetTitleOffset(1.2);
    adcSpecHist->GetYaxis()->SetLabelSize(0.03);
    adcSpecHist->GetYaxis()->CenterTitle();
    adcSpecHist->GetZaxis()->SetRangeUser(0,300);

    monSpecHist->SetTitle("Charge Distribution");
    monSpecHist->GetXaxis()->SetTitle("Total Integrated Charge (pC)");
    monSpecHist->GetXaxis()->CenterTitle();
    monSpecHist->GetXaxis()->SetTitleSize(0.04);
    monSpecHist->GetXaxis()->SetLabelSize(0.03);
    monSpecHist->GetYaxis()->SetTitle("Count");
    monSpecHist->GetYaxis()->SetTitleSize(0.04);
    monSpecHist->GetYaxis()->SetTitleOffset(1.2);
    monSpecHist->GetYaxis()->SetLabelSize(0.03);
    monSpecHist->GetYaxis()->CenterTitle();
    monSpecHist->GetZaxis()->SetRangeUser(0,300);

    smoothHist->SetTitle("Smoothed Charge Distribution");
    smoothHist->GetXaxis()->SetTitle("Smoothed Total Integrated Charge (pC)");
    smoothHist->GetXaxis()->CenterTitle();
    smoothHist->GetXaxis()->SetTitleSize(0.04);
    smoothHist->GetXaxis()->SetLabelSize(0.03);
    smoothHist->GetYaxis()->SetTitle("Smoothed Count");
    smoothHist->GetYaxis()->SetTitleSize(0.04);
    smoothHist->GetYaxis()->SetTitleOffset(1.2);
    smoothHist->GetYaxis()->SetLabelSize(0.03);
    smoothHist->GetYaxis()->CenterTitle();
    smoothHist->GetZaxis()->SetRangeUser(0,300);
    
    timeHist->SetTitle("Transit Time Distribution");
    timeHist->GetXaxis()->SetTitle("Transit Time (ns)");
    timeHist->GetXaxis()->CenterTitle();
    timeHist->GetXaxis()->SetTitleSize(0.04);
    timeHist->GetXaxis()->SetLabelSize(0.03);
    timeHist->GetYaxis()->SetTitle("Count");
    timeHist->GetYaxis()->SetTitleSize(0.04);
    timeHist->GetYaxis()->SetTitleOffset(1.2);
    timeHist->GetYaxis()->SetLabelSize(0.03);
    timeHist->GetYaxis()->CenterTitle();

    monTimeHist->SetTitle("Monitor PMT Transit Time Distribution");
    monTimeHist->GetXaxis()->SetTitle("Transit Time (ns)");
    monTimeHist->GetXaxis()->CenterTitle();
    monTimeHist->GetXaxis()->SetTitleSize(0.04);
    monTimeHist->GetXaxis()->SetLabelSize(0.03);
    monTimeHist->GetYaxis()->SetTitle("Count");
    monTimeHist->GetYaxis()->SetTitleSize(0.04);
    monTimeHist->GetYaxis()->SetTitleOffset(1.2);
    monTimeHist->GetYaxis()->SetLabelSize(0.03);
    monTimeHist->GetYaxis()->CenterTitle();

    pvRatioHist->SetTitle(pvPlotTitle);
    pvRatioHist->GetXaxis()->SetTitle("X Position (m)");
    pvRatioHist->GetXaxis()->CenterTitle();
    pvRatioHist->GetXaxis()->SetTitleSize(0.04);
    pvRatioHist->GetXaxis()->SetLabelSize(0.03);
    pvRatioHist->GetYaxis()->SetTitle("Y Position (m)");
    pvRatioHist->GetYaxis()->SetTitleSize(0.04);
    pvRatioHist->GetYaxis()->SetTitleOffset(1.2);
    pvRatioHist->GetYaxis()->SetLabelSize(0.03);
    pvRatioHist->GetYaxis()->CenterTitle();
    pvRatioHist->GetXaxis()->SetRangeUser(0, 0.75);
    pvRatioHist->GetYaxis()->SetRangeUser(0, 0.75);
    pvRatioHist->GetZaxis()->SetRangeUser(0,10);
    pvRatioHist->GetZaxis()->SetLabelSize(0.02);

    gainPositionHist->SetTitle(gainPlotTitle);
    gainPositionHist->GetXaxis()->SetTitle("X Position (m)");
    gainPositionHist->GetXaxis()->CenterTitle();
    gainPositionHist->GetXaxis()->SetTitleSize(0.04);
    gainPositionHist->GetXaxis()->SetLabelSize(0.03);
    gainPositionHist->GetYaxis()->SetTitle("Y Position (m)");
    gainPositionHist->GetYaxis()->SetTitleSize(0.04);
    gainPositionHist->GetYaxis()->SetTitleOffset(1.2);
    gainPositionHist->GetYaxis()->SetLabelSize(0.03);
    gainPositionHist->GetYaxis()->CenterTitle();
    gainPositionHist->GetXaxis()->SetRangeUser(0, 0.75);
    gainPositionHist->GetYaxis()->SetRangeUser(0,0.75);
    gainPositionHist->GetZaxis()->SetRangeUser(0,5);
    gainPositionHist->GetZaxis()->SetLabelSize(0.02);

    // gain error 2d histogram 
    // gainErrorHist->GetXaxis()->SetTitle("X Position (m)");
    // gainErrorHist->GetXaxis()->CenterTitle();
    // gainErrorHist->GetXaxis()->SetTitleSize(0.04);
    // gainErrorHist->GetXaxis()->SetLabelSize(0.03);
    // gainErrorHist->GetYaxis()->SetTitle("Y Position (m)");
    // gainErrorHist->GetYaxis()->SetTitleSize(0.04);
    // gainErrorHist->GetYaxis()->SetTitleOffset(1.2);
    // gainErrorHist->GetYaxis()->SetLabelSize(0.03);
    // gainErrorHist->GetYaxis()->CenterTitle();
    // gainErrorHist->GetXaxis()->SetRangeUser(0, 0.75);
    // gainErrorHist->GetYaxis()->SetRangeUser(0,0.75);
    // gainErrorHist->GetZaxis()->SetRangeUser(0,1);
    // gainErrorHist->GetZaxis()->SetLabelSize(0.02);

    detPositionHist->SetTitle(detPlotTitle);
    detPositionHist->GetXaxis()->SetTitle("X Position (m)");
    detPositionHist->GetXaxis()->CenterTitle();
    detPositionHist->GetXaxis()->SetTitleSize(0.04);
    detPositionHist->GetXaxis()->SetLabelSize(0.03);
    detPositionHist->GetYaxis()->SetTitle("Y Position (m)");
    detPositionHist->GetYaxis()->SetTitleSize(0.04);
    detPositionHist->GetYaxis()->SetTitleOffset(1.2);
    detPositionHist->GetYaxis()->SetLabelSize(0.03);
    detPositionHist->GetYaxis()->CenterTitle();
    detPositionHist->GetXaxis()->SetRangeUser(0,0.75);
    detPositionHist->GetYaxis()->SetRangeUser(0,0.75);
    detPositionHist->GetZaxis()->SetRangeUser(0,0.14);
    detPositionHist->GetZaxis()->SetLabelSize(0.02);

    // det eff error hist
    // detErrorHist->GetXaxis()->SetTitle("X Position (m)");
    // detErrorHist->GetXaxis()->CenterTitle();
    // detErrorHist->GetXaxis()->SetTitleSize(0.04);
    // detErrorHist->GetXaxis()->SetLabelSize(0.03);
    // detErrorHist->GetYaxis()->SetTitle("Y Position (m)");
    // detErrorHist->GetYaxis()->SetTitleSize(0.04);
    // detErrorHist->GetYaxis()->SetTitleOffset(1.2);
    // detErrorHist->GetYaxis()->SetLabelSize(0.03);
    // detErrorHist->GetYaxis()->CenterTitle();
    // detErrorHist->GetXaxis()->SetRangeUser(0,0.75);
    // detErrorHist->GetYaxis()->SetRangeUser(0,0.75);
    // detErrorHist->GetZaxis()->SetRangeUser(0,0.005);
    // detErrorHist->GetZaxis()->SetLabelSize(0.02);

    monPositionHist->SetTitle("Monitor PMT Det Eff");
    monPositionHist->GetXaxis()->SetTitle("X Position (m)");
    monPositionHist->GetXaxis()->CenterTitle();
    monPositionHist->GetXaxis()->SetTitleSize(0.04);
    monPositionHist->GetXaxis()->SetLabelSize(0.03);
    monPositionHist->GetYaxis()->SetTitle("Y Position (m)");
    monPositionHist->GetYaxis()->SetTitleSize(0.04);
    monPositionHist->GetYaxis()->SetTitleOffset(1.2);
    monPositionHist->GetYaxis()->SetLabelSize(0.03);
    monPositionHist->GetYaxis()->CenterTitle();
    monPositionHist->GetXaxis()->SetRangeUser(0,0.75);
    monPositionHist->GetYaxis()->SetRangeUser(0,0.75);
    monPositionHist->GetZaxis()->SetRangeUser(0,0.3); 
    monPositionHist->GetZaxis()->SetLabelSize(0.02);

    ttsPositionHist->SetTitle(ttsPlotTitle);
    ttsPositionHist->GetXaxis()->SetTitle("X Position (m)");
    ttsPositionHist->GetXaxis()->CenterTitle();
    ttsPositionHist->GetXaxis()->SetTitleSize(0.04);
    ttsPositionHist->GetXaxis()->SetLabelSize(0.03);
    ttsPositionHist->GetYaxis()->SetTitle("Y Position (m)");
    ttsPositionHist->GetYaxis()->SetTitleSize(0.04);
    ttsPositionHist->GetYaxis()->SetTitleOffset(1.2);
    ttsPositionHist->GetYaxis()->SetLabelSize(0.03);
    ttsPositionHist->GetYaxis()->CenterTitle();
    ttsPositionHist->GetXaxis()->SetRangeUser(0,0.75);
    ttsPositionHist->GetYaxis()->SetRangeUser(0,0.75);
    ttsPositionHist->GetZaxis()->SetRangeUser(0,5);
    ttsPositionHist->GetZaxis()->SetLabelSize(0.02);

    peakPositionHist->SetTitle(peakPlotTitle);
    peakPositionHist->GetXaxis()->SetTitle("X Position (m)");
    peakPositionHist->GetXaxis()->CenterTitle();
    peakPositionHist->GetXaxis()->SetTitleSize(0.04);
    peakPositionHist->GetXaxis()->SetLabelSize(0.03);
    peakPositionHist->GetYaxis()->SetTitle("Y Position (m)");
    peakPositionHist->GetYaxis()->SetTitleSize(0.04);
    peakPositionHist->GetYaxis()->SetTitleOffset(1.2);
    peakPositionHist->GetYaxis()->SetLabelSize(0.03);
    peakPositionHist->GetYaxis()->CenterTitle();
    peakPositionHist->GetXaxis()->SetRangeUser(0,0.75);
    peakPositionHist->GetYaxis()->SetRangeUser(0,0.75);
    peakPositionHist->GetZaxis()->SetRangeUser(240, 300);
    peakPositionHist->GetZaxis()->SetLabelSize(0.02);

    // peak error hist
    // peakErrorHist->GetXaxis()->SetTitle("X Position (m)");
    // peakErrorHist->GetXaxis()->CenterTitle();
    // peakErrorHist->GetXaxis()->SetTitleSize(0.04);
    // peakErrorHist->GetXaxis()->SetLabelSize(0.03);
    // peakErrorHist->GetYaxis()->SetTitle("Y Position (m)");
    // peakErrorHist->GetYaxis()->SetTitleSize(0.04);
    // peakErrorHist->GetYaxis()->SetTitleOffset(1.2);
    // peakErrorHist->GetYaxis()->SetLabelSize(0.03);
    // peakErrorHist->GetYaxis()->CenterTitle();
    // peakErrorHist->GetXaxis()->SetRangeUser(0,0.75);
    // peakErrorHist->GetYaxis()->SetRangeUser(0,0.75);
    // peakErrorHist->GetZaxis()->SetRangeUser(0,0.1);
    // peakErrorHist->GetZaxis()->SetLabelSize(0.02);

    // // tts error hist
    // ttsErrorHist->GetXaxis()->SetTitle("X Position (m)");
    // ttsErrorHist->GetXaxis()->CenterTitle();
    // ttsErrorHist->GetXaxis()->SetTitleSize(0.04);
    // ttsErrorHist->GetXaxis()->SetLabelSize(0.03);
    // ttsErrorHist->GetYaxis()->SetTitle("Y Position (m)");
    // ttsErrorHist->GetYaxis()->SetTitleSize(0.04);
    // ttsErrorHist->GetYaxis()->SetTitleOffset(1.2);
    // ttsErrorHist->GetYaxis()->SetLabelSize(0.03);
    // ttsErrorHist->GetYaxis()->CenterTitle();
    // ttsErrorHist->GetXaxis()->SetRangeUser(0,0.75);
    // ttsErrorHist->GetYaxis()->SetRangeUser(0,0.75);
    // ttsErrorHist->GetZaxis()->SetRangeUser(0,0.1);
    // ttsErrorHist->GetZaxis()->SetLabelSize(0.02);

    ttsFitPositionHist->SetTitle(ttsFitPlotTitle);
    ttsFitPositionHist->GetXaxis()->SetTitle("X Position (m)");
    ttsFitPositionHist->GetXaxis()->CenterTitle();
    ttsFitPositionHist->GetXaxis()->SetTitleSize(0.04);
    ttsFitPositionHist->GetXaxis()->SetLabelSize(0.03);
    ttsFitPositionHist->GetYaxis()->SetTitle("Y Position (m)");
    ttsFitPositionHist->GetYaxis()->SetTitleSize(0.04);
    ttsFitPositionHist->GetYaxis()->SetTitleOffset(1.2);
    ttsFitPositionHist->GetYaxis()->SetLabelSize(0.03);
    ttsFitPositionHist->GetYaxis()->CenterTitle();
    ttsFitPositionHist->GetXaxis()->SetRangeUser(0,0.75);
    ttsFitPositionHist->GetYaxis()->SetRangeUser(0,0.75);
    ttsFitPositionHist->GetZaxis()->SetRangeUser(0,5);
    ttsFitPositionHist->GetZaxis()->SetLabelSize(0.02);

    peakFitPositionHist->SetTitle(peakPlotTitle);
    peakFitPositionHist->GetXaxis()->SetTitle("X Position (m)");
    peakFitPositionHist->GetXaxis()->CenterTitle();
    peakFitPositionHist->GetXaxis()->SetTitleSize(0.04);
    peakFitPositionHist->GetXaxis()->SetLabelSize(0.03);
    peakFitPositionHist->GetYaxis()->SetTitle("Y Position (m)");
    peakFitPositionHist->GetYaxis()->SetTitleSize(0.04);
    peakFitPositionHist->GetYaxis()->SetTitleOffset(1.2);
    peakFitPositionHist->GetYaxis()->SetLabelSize(0.03);
    peakFitPositionHist->GetYaxis()->CenterTitle();
    peakFitPositionHist->GetXaxis()->SetRangeUser(0,0.75);
    peakFitPositionHist->GetYaxis()->SetRangeUser(0,0.75);
    peakFitPositionHist->GetZaxis()->SetRangeUser(270, 290);
    peakFitPositionHist->GetZaxis()->SetLabelSize(0.02);

    // set plot parameters for count histograms
    monCountHist->SetTitle("Count of Monitor PMT Events");
    monCountHist->GetXaxis()->SetTitle("X Position (m)");
    monCountHist->GetXaxis()->CenterTitle();
    monCountHist->GetXaxis()->SetTitleSize(0.04);
    monCountHist->GetXaxis()->SetLabelSize(0.03);
    monCountHist->GetYaxis()->SetTitle("Y Position (m)");
    monCountHist->GetYaxis()->SetTitleSize(0.04);
    monCountHist->GetYaxis()->SetTitleOffset(1.2);
    monCountHist->GetYaxis()->SetLabelSize(0.03);
    monCountHist->GetYaxis()->CenterTitle();
    monCountHist->GetXaxis()->SetRangeUser(0,0.75);
    monCountHist->GetYaxis()->SetRangeUser(0,0.75);
    monCountHist->GetZaxis()->SetRangeUser(0,0.3); // 0.7
    monCountHist->GetZaxis()->SetLabelSize(0.02);

    pmtCountHist->SetTitle("Count of Test PMT Events");
    pmtCountHist->GetXaxis()->SetTitle("X Position (m)");
    pmtCountHist->GetXaxis()->CenterTitle();
    pmtCountHist->GetXaxis()->SetTitleSize(0.04);
    pmtCountHist->GetXaxis()->SetLabelSize(0.03);
    pmtCountHist->GetYaxis()->SetTitle("Y Position (m)");
    pmtCountHist->GetYaxis()->SetTitleSize(0.04);
    pmtCountHist->GetYaxis()->SetTitleOffset(1.2);
    pmtCountHist->GetYaxis()->SetLabelSize(0.03);
    pmtCountHist->GetYaxis()->CenterTitle();
    pmtCountHist->GetXaxis()->SetRangeUser(0,0.75);
    pmtCountHist->GetYaxis()->SetRangeUser(0,0.75);
    pmtCountHist->GetZaxis()->SetRangeUser(0,0.3);
    pmtCountHist->GetZaxis()->SetLabelSize(0.02);

    simulCountHist->SetTitle("Count of Simultaneous PMT Events");
    simulCountHist->GetXaxis()->SetTitle("X Position (m)");
    simulCountHist->GetXaxis()->CenterTitle();
    simulCountHist->GetXaxis()->SetTitleSize(0.04);
    simulCountHist->GetXaxis()->SetLabelSize(0.03);
    simulCountHist->GetYaxis()->SetTitle("Y Position (m)");
    simulCountHist->GetYaxis()->SetTitleSize(0.04);
    simulCountHist->GetYaxis()->SetTitleOffset(1.2);
    simulCountHist->GetYaxis()->SetLabelSize(0.03);
    simulCountHist->GetYaxis()->CenterTitle();
    simulCountHist->GetXaxis()->SetRangeUser(0,0.75);
    simulCountHist->GetYaxis()->SetRangeUser(0,0.75);
    simulCountHist->GetZaxis()->SetRangeUser(0,0.3);
    simulCountHist->GetZaxis()->SetLabelSize(0.02);

    // Initial draw of fitting results
    // c6->cd(1);
    // sigmaEventHist->Draw();
    // c6->cd(2);
    // sigmaPedHist->Draw();
    // c6->cd(3);
    // chiSquareHistPed->Draw();
    // c6->cd(4);
    // chiSquareHistSPE->Draw();
    // c6->cd(5);
    // peakValleyRatioHist->Draw();
    // c6->cd(6);
    // gain1DHist->Draw();

    /*===============================================================================================================
    / End of Formatting Section
    / Start of Actual Analysis Section
    / =============================================================================================================*/

    //local variable creation for addressing of branch values from TTree

    /*===============================================================================================================
    ** Arrays for waveforms: 
    ** Each row corresponds to one sample
    ** Each column is the time point in 2ns intervals
    **=============================================================================================================*/
    
    Double_t arr0[200000][70]; //Preallocated for speed -- PMT // changed from 70 to 100 to test new timing windows
    Double_t arr1[200000][70]; //Preallocated for speed -- ref
    Double_t arr3[200000][70]; // G0 monitor PMT
    Double_t arr5[200000][70]; // G1 monitor PMT


    //Variable holding the number of samples at a given position (one sample is one row in arrays above
    Int_t val_points;
    
    //Variable holding number of positions
    Int_t numEntries0;
    Int_t numEntries1;
    
    //Positional Variables 
    Double_t xVal = 0;
    Double_t yVal = 0;
    Double_t yPeak = 0;
    Double_t xposition, yposition; // gantry position
    Double_t gantrytilt;
    Double_t gantryrot;
    Double_t x_inc, y_inc; //point of incidence of laser beam
    Double_t x, y; //point plotted on gain and detection efficiency plots
    Double_t final_offset_x, final_offset_y; //Corrections for optical box
    Double_t beamLength = 0.05; // (26Apr2017): changed from 0.1m to 0.05m

    //Hard limits of gantry space in x and y
    Double_t gantryXlimit = 0.749;
    Double_t gantryYlimit = 0.696;
    
    //offset in laser aperture with respect to gantry0 position
    Double_t g0_offset1 = 0.115;
    Double_t g0_offset2 = 0.042;
    Double_t g0_offset3 = 0.0365;

    //offset in laser aperture with respect to gantry1 position
    Double_t g1_offset1 = 0.115;
    Double_t g1_offset2 = 0.052;
    Double_t g1_offset3 = 0.0395;

    // Local variables for ADC pedestal and event fitting
    Double_t p_lo_bound;
    Double_t p_hi_bound;
    Double_t e_lo_bound;
    Double_t e_hi_bound;
    Int_t binmax;
    Double_t x_pos;

    // Charge spectra gaussian fit results
    Float_t sigmaEvent;
    Float_t sigmaPed;
    Double_t meanEvent;
    Double_t meanPed;
    Float_t constantEvent;
    Float_t constantPed;
    Double_t monMeanEvent;

    //spread of pedestal fit; can be refined
    //generally the best value captures the top of the pedestal; this allows the fit to avoid background influence
    Double_t spread = 0.3;
   
    // Set up file I/O
    TFile *f = new TFile("/data14b/kxie/rootfiles/out_run0" + runNumber + ".root"); 

    //Creates pointer to TTree contained in the input file and pointers to its relevant branches
    TTree* T = (TTree*)f->Get("scan_tree");
    TBranch *branch0 = T->GetBranch("V1730_wave0"); //branch containing 2d array of samples for pmt signal
    TBranch *branch1 = T->GetBranch("V1730_wave1"); //branch containing 2d array of samples for reference pulse
    TBranch *branch3 = T->GetBranch("V1730_wave3"); // 2d array of samples for G0 monitor PMT pulse
    TBranch *branch5 = T->GetBranch("V1730_wave5"); // branch containing 2d array of samples for G1 monitor PMT pulse
    TBranch *points = T->GetBranch("num_points"); //gets the number of points in the scan

    // read in the chosen gantry data
    TBranch *gantry_x;
    TBranch *gantry_y;
    TBranch *gantry_tilt;
    TBranch *gantry_rot;
    if (whichGantry==0) {
        gantry_x = T->GetBranch("gantry0_x");
	gantry_y = T->GetBranch("gantry0_y");
        gantry_tilt = T->GetBranch("gantry0_tilt");
        gantry_rot = T->GetBranch("gantry0_rot");
    } else{
        gantry_x = T->GetBranch("gantry1_x");
        gantry_y = T->GetBranch("gantry1_y");
        gantry_tilt = T->GetBranch("gantry1_tilt");
        gantry_rot = T->GetBranch("gantry1_rot");
    }

    //defines total number of entries for indexing bound
    numEntries0 = branch0->GetEntries();
    numEntries1 = branch1->GetEntries();
     
    //Sets where the value read in by GetEntry() is stored
    gantry_x->SetAddress(&xposition);
    gantry_y->SetAddress(&yposition);
    gantry_tilt->SetAddress(&gantrytilt);
    gantry_rot->SetAddress(&gantryrot);
    points->SetAddress(&val_points);
    branch0->SetAddress(arr0);
    branch1->SetAddress(arr1);
    branch3->SetAddress(arr3);
    branch5->SetAddress(arr5);
    branch0->SetAutoDelete(kFALSE);
    branch1->SetAutoDelete(kFALSE);
    branch3->SetAutoDelete(kFALSE);
    branch5->SetAutoDelete(kFALSE);
    
    std::cout<<"\nPomf~" <<std::endl;

    // FITTING PARAPHERNALIA THAT DOES MATTER NOW
    // These fit functions are used if you want to fit each indiviual waveform (warning: this takes a long time)
    Double_t adcIntegralValue;
    Double_t monIntegralValue;

    Int_t min_range = 6;// offset of 6 result of experimental startup(?)
    Int_t max_range =56;
    Int_t nParPulse = 3;
    Int_t nParRef = 4;
    
    TF1 * pulseFitFunc = new TF1("pulseFitFunc", pulseFitFunc, min_range, max_range, nParPulse);
    TF1 * refPulseFitFunc = new TF1("refPulseFitFunc", refPulseFitFunc, min_range, max_range, nParRef);
    
    refPulseFitFunc->SetParName(0, "height");
    refPulseFitFunc->SetParName(1, "mu");
    refPulseFitFunc->SetParName(2, "sigma");
    refPulseFitFunc->SetParName(3, "offset");

    pulseFitFunc->SetParName(0, "height");
    pulseFitFunc->SetParName(1, "mu");
    pulseFitFunc->SetParName(2, "sigma");
    
    refPulseFitFunc->SetParameters(-6000, 10, 6, 7500);//guess the starting parameters
    pulseFitFunc->SetParameters(-53.0, 41, 3.1); 

    refPulseFitFunc->SetParLimits(0, -8000, -31.0);
    refPulseFitFunc->SetParLimits(1, 6, 45);
    refPulseFitFunc->SetParLimits(2, 0.01, 20); 
    refPulseFitFunc->SetParLimits(3, 7100, 7700);

    pulseFitFunc->SetParLimits(0, -200.0, -31.0);
    pulseFitFunc->SetParLimits(1, 0, 70); 
    pulseFitFunc->SetParLimits(2, 2.0, 6.00);
    //END OF PARAPHERNALIA RELATED TO FITTING
 

    // ADC charge spectrum + integrated pulse fit overlap into a single canvas
    // also produce stack for monitor charge spectrum
    if (analysisType == 0 || analysisType == 2 ) {
      c2->cd(1);
      THStack *hs = new THStack("hs","");
    
      adcSpecHist->SetFillStyle(3004);
      adcSpecHist->SetFillColor(kBlue);
      hs->Add(adcSpecHist);
      waveFitHist->SetFillStyle(3005);
      waveFitHist->SetFillColor(kRed);
      // hs->Add(waveFitHist);    
      smoothHist ->SetFillStyle(3006);
      smoothHist-> SetFillColor(kGreen);
      //  hs->Add(smoothHist);
      hs->Draw("nostack");
      hs->SetTitle("Charge Spectrum");
      hs->GetXaxis()->SetTitle("Total Integrated Charge (pC)");
      hs->GetXaxis()->CenterTitle();
      hs->GetXaxis()->SetTitleSize(0.04);
      hs->GetXaxis()->SetLabelSize(0.03);
      hs->GetYaxis()->SetTitle("Count");
      hs->GetYaxis()->SetTitleSize(0.04);
      hs->GetYaxis()->SetTitleOffset(1.2);
      hs->GetYaxis()->SetLabelSize(0.03);
      hs->GetYaxis()->CenterTitle();

      gPad->SetLogy();

      c15->cd(1);
      THStack *ms = new THStack("ms", "");
      ms->Add(monSpecHist);
      ms->Draw("nostack");
      ms->SetTitle("Monitor Charge Spectrum");
      ms->GetXaxis()->SetTitle("Total Integrated Charge (pC)");
      ms->GetXaxis()->CenterTitle();
      ms->GetXaxis()->SetTitleSize(0.04);
      ms->GetXaxis()->SetLabelSize(0.03);
      ms->GetYaxis()->SetTitle("Fraction of Proper Pulses");
      ms->GetYaxis()->SetTitleSize(0.04);
      ms->GetYaxis()->SetTitleOffset(1.2);
      ms->GetYaxis()->SetLabelSize(0.03);
      ms->GetYaxis()->CenterTitle();
      
      gPad->SetLogy();
    }
    
    // Put the timing distribution into a stack to allow observation
    if (analysisType == 1 || analysisType == 2 ) {

      c3->cd(1);

      THStack *ts = new THStack("ts", "");
      timeHist->SetFillStyle(3004);
      timeHist->SetFillColor(kBlue);
      ts->Add(timeHist);
      timeFitHist->SetFillStyle(3005);
      timeFitHist->SetFillColor(kRed);
      // ts->Add(timeFitHist);    
      ts->Draw("nostack");
      ts->SetTitle("Timing Distribution");
      ts->GetXaxis()->SetTitle("Time (ns)");
      ts->GetXaxis()->CenterTitle();
      ts->GetXaxis()->SetTitleSize(0.04);
      ts->GetXaxis()->SetLabelSize(0.03);
      ts->GetYaxis()->SetTitle("Count");
      ts->GetYaxis()->SetTitleSize(0.04);
      ts->GetYaxis()->SetTitleOffset(1.2);
      ts->GetYaxis()->SetLabelSize(0.03);
      ts->GetYaxis()->CenterTitle();
    
      c19->cd(1);
      THStack *mts = new THStack("mts", "");
      mts->Add(monTimeHist);
      mts->Draw("nostack");
      mts->SetTitle("Monitor Time Distribution");
      mts->GetXaxis()->SetTitle("Transit Time (ns)");
      mts->GetXaxis()->CenterTitle();
      mts->GetXaxis()->SetTitleSize(0.04);
      mts->GetXaxis()->SetLabelSize(0.03);
      mts->GetYaxis()->SetTitle("Count");
      mts->GetYaxis()->SetTitleSize(0.04);
      mts->GetYaxis()->SetTitleOffset(1.2);
      mts->GetYaxis()->SetLabelSize(0.03);
      mts->GetYaxis()->CenterTitle();
    }

    

    // Initialize variables to hold data for analysis
    Int_t minBin0;
    Int_t minBin1;
    Int_t minBin3;
    Int_t minBin5;
    Int_t sumSmooth;
    Double_t minimumVal;
    Double_t monMinimumVal;

    double threshold = -60;
    double pmtOffset = -8135.4;
    double monitorOffset;
    if (whichGantry == 0) {
      monitorOffset = -8169;
    } else {
      monitorOffset = -8211;
    }
    
    // Count number of proper pulses/samples in each point
    Int_t pmtPulseCount = 0;
    Int_t monPulseCount = 0;
    Int_t simulPulseCount = 0;
    
    // Uncomment to observe raw waveforms
    c14->cd(1);
     waveFormHist1->Draw();
     c14->cd(2);
     waveFormHist0->Draw();
    c14->cd(3);
    waveFormMonG1->Draw();
 
    // Section to initialize plots of the trigger rate
    // Observing the trigger rate is important so that null events can be compensated
    const int arrayLength = numEntries0;
    Double_t arr_x[arrayLength];
    Double_t arr_x_err[arrayLength];
    for (int loop = 0; loop < arrayLength; loop++) {
      arr_x[loop] = loop*3; // points on x separated into 3 second slices // the actual number should be based on the time wait between points, 3 is simply the standard
      arr_x_err[loop] = 0;
    }

   
    Double_t triggerSlice[arrayLength];
    Double_t monGainSlice[arrayLength];
    Double_t monDetEffSlice[arrayLength];
   


    // Initialize variables for spectrum fitting and analysis
    Double_t monitorIntegralEvent;
    Double_t totalIntegralEvent;
    Double_t altTotalIntegralEvent;
    TF1* fPed;
    TF1* fInter;
    TF1* fEvent;
    TF1* monEvent;
    TF1* fTTS;
    TF1* monPed;
    TF1* monTimeFit;

    // Loops over all the points in the gantry space
    // Specifically this is all points in a scan
    for(int i = 0; i < numEntries0; i++){ 


      int filteredPoints = 0;
      int nullEvents = 0;
      adcSpecHist->Reset();
      smoothHist->Reset();
      waveFitHist->Reset();
      timeHist->Reset();
      timeFitHist->Reset();
      monSpecHist->Reset();
      monTimeHist->Reset();
      monPulseCount = 0;
      pmtPulseCount = 0;
      simulPulseCount = 0;


      //writes the data fields contained at the entry 'i' to the variables defined above
      T->GetEntry(i); // read all branches of entry i

      //limits xy space of the gantry
      if( !(whichGantry==0 && xposition<=0.001 && (yposition<=0.001 || yposition>gantryYlimit))
	  && !(whichGantry==1 && xposition>gantryXlimit && (yposition<=0.001 || yposition>gantryYlimit)) ) {
      
	//debugging: output point number
	std::cout << i << std::endl;

	// Loop over all the waveform samples (val_points) in a gantry space point
	for(int j = 0; j < val_points; j++) { 
	 
	  //Cleans the waveform histograms for the next waveform sample
	  waveFormHist0->Reset();
	  waveFormHist1->Reset();
	  waveFormMonG0->Reset();
	  waveFormMonG1->Reset();
	
	  //Fills waveform histograms
	  for(int k = 3; k < 70; k++){
	    
	    waveFormHist0->Fill(k, arr0[j][k] + pmtOffset); // apply offsets
	  
	    waveFormHist1->Fill(k, arr1[j][k]); 
	    
	    
	    if (whichGantry == 0) {    
	      waveFormMonG0->Fill(k, arr3[j][k] + monitorOffset);
	    }
	    if (whichGantry == 1) {
	      waveFormMonG1->Fill(k, arr5[j][k] + monitorOffset);
	    }
	  }

	
	  if (waveFormHist0->GetMinimum() == pmtOffset && waveFormHist0->GetMaximum() == 0) {
	    nullEvents++;
	    filteredPoints++;

	  }
	  
	  //Calculates important values from waveforms for speed once
	  minBin0 = waveFormHist0->GetMinimumBin();
	  minBin1 = waveFormHist1->GetMinimumBin();
	  // c14->cd(1);
	  //   gPad->Modified();
	  //   gPad->Update();
	  //   c14->cd(2);
	  //   gPad->Modified();
	  //   gPad->Update();
	  //   c14->cd(3);
	  //   gPad->Modified();
	  //   gPad->Update();
	
	  double weightedSum = 0;
	  double monTime = 0;

	  // Identify and integrate the waveforms around the minimum value
	  // Monitor data integration
	  if (whichGantry == 0) {
	    
	    minBin3 = waveFormMonG0->GetMinimumBin();
	    monIntegralValue = 2*waveFormMonG0->Integral(minBin3-5, minBin3 + 5) * voltageConversion;
	    monMinimumVal = waveFormMonG0->GetMinimum();
	  
	    // Determing timing distribution of monitor PMT data
	    if (monMinimumVal < threshold && monMinimumVal != monitorOffset && minBin3 > 3) {
	    for (int l = (minBin3 - 3); l < (minBin3 + 3); l++) {
	      double chargeDifference = waveFormMonG0->GetBinContent(l) - threshold;
	      if (chargeDifference < 0 && chargeDifference != monitorOffset - threshold) {
	    	monTime += 2 * l * -chargeDifference; // 2ns per bin
	    	weightedSum += fabs(chargeDifference);
	      }
	    }
	    monTimeHist->Fill(monTime / weightedSum + 120 - 6); // currently just time from histogram window, needs offset from raw data //arbitrary offset of 120
	    
	    }
	  }		
	
	  if (whichGantry == 1) {

	    minBin5 = waveFormMonG1->GetMinimumBin();
	    monIntegralValue = 2*waveFormMonG1->Integral(minBin5 - 5, minBin5 + 5) * voltageConversion;
	    monMinimumVal = waveFormMonG1->GetMinimum();

	    // Determine timing distribution of monitor PMT data
	    if (monMinimumVal < threshold && monMinimumVal != monitorOffset && minBin5 > 3) {
	      for (int l = (minBin5 - 3); l < (minBin5 + 3); l++) {
	      	double chargeDifference = waveFormMonG1->GetBinContent(l) - threshold;
	      	if (chargeDifference < 0 && chargeDifference != monitorOffset - threshold) {
	      	  monTime += 2 * l * -chargeDifference; // 2ns per bin
	      	  weightedSum += fabs(chargeDifference);
	      	}
	      }
	      monTimeHist->Fill(monTime / weightedSum + 120 - 12); // currently just time from histogram window, needs offset from raw data //arbitrary offset of 120
	    }
	    
	  }

	  // Fill monitor charge integral value
	  monSpecHist->Fill(monIntegralValue);
	  
	
	
	  // ADC data integration
	  adcIntegralValue = 2*waveFormHist0->Integral(minBin0 - 10, minBin0 + 10)*voltageConversion; // multiply by 2 because each bin is 2ns
	  adcSpecHist->Fill(adcIntegralValue);
	  minimumVal = waveFormHist0->GetMinimum();
	 

	  // Count the number of proper monitor PMT pulses
	  if (monMinimumVal < threshold && monMinimumVal != monitorOffset) {
	    monPulseCount++;
	  }
	  
	  // //Discriminates pulses less than ~3mV in magnitude and minimum bin of pmt pulse is not first bin of histogram
	  if( minimumVal < threshold && minBin0 != 0 && minBin1!=0 && minimumVal != pmtOffset) {
	   

	    // c14->cd(1);
	    // gPad->Modified();
	    // gPad->Update();
	    // c14->cd(2);
	    // gPad->Modified();
	    // gPad->Update();
	    // c14->cd(3);
	    // gPad->Modified();
	    // gPad->Update();
	    // usleep(100000);
	    // count number of proper 20 in. samples and simultaneous pulses
	    pmtPulseCount++;
	    if (monMinimumVal < threshold && monMinimumVal != monitorOffset) {
	      simulPulseCount++;
	    
	    }

	    /*====================================================================================================================
	     * Fill transit time histogram + fitted histogram (optional)
	     * 12 offset is from 6 bin (2ns / bin) offset between fitted histogram indices and fit function indices.  272 is 136 bin offset between both pulses in digitizer time
	     *=====================================================================================================================
	     */
	    timeHist->Fill(2*minBin0 + 244 - 2*minBin1 - 6); 
	  }
	
	}
	
      

	/*============================================================================================
	 * CHARGE SPECTRA FITTING AND ANALYSIS (WITH AND WITHOUT SMOOTHING)
	 * Seems like it's best to fit smoothed event and unsmoothed pedestal (as of 22/Sep/2017)
	 *=============================================================================================
	 */
        
	if (analysisType == 0 || analysisType == 2) {
	      
	      // For 20 in. PMT ADC spectrum
	      // Smoothing  charge spectrum 
	      Int_t uBound = adcSpecHist->GetNbinsX()-2;
	      
	      // Smooth adcSpecHist using moving averages 3 bins at a time
	      for (Int_t indexSmoothing = 2; indexSmoothing < uBound; indexSmoothing++) {
	      	sumSmooth = 0;
	      	for (Int_t smoothRange = 0; smoothRange < 3; smoothRange++) {
	      	  sumSmooth += adcSpecHist->GetBinContent(indexSmoothing-2 + smoothRange);
	      	}
	      	smoothHist->Fill(adcSpecHist->GetBinCenter(indexSmoothing), sumSmooth/3.0);
	      }

	      // Find pedestal position and create fitting bounds
	      binmax = adcSpecHist->GetMaximumBin();
	      x_pos = adcSpecHist->GetXaxis()->GetBinCenter(binmax);
	      p_lo_bound = x_pos - spread;
	      p_hi_bound = x_pos + spread;


	      // Create fit objects for gaussians on adcSpecHist
	       fPed = new TF1("pedFit", "gaus",p_lo_bound, p_hi_bound);
	       fEvent = new TF1("eventFit", "gaus", x_pos-5, x_pos-0.75); // ranges are somewhat arbitrary and are best adjusted manually
	 
	       // Apply pedestal fit to the adc spectrum, event fit to the smoothed portion
	       adcSpecHist->Fit(fPed, "RQMN0");
	       smoothHist->Fit(fEvent, "RQMN0");
   
	      // Make fit to find intersection point between event and ped fits
	       fInter = new TF1("interFit", "fabs(eventFit-pedFit)", x_pos-2, x_pos);
	      
	       c2->cd(1);
	       fPed->Draw("same");
	       fEvent->Draw("same");
	       
	       //Determine and extract fit parameters
	       meanEvent = fEvent->GetParameter(1);
	       meanPed = fPed->GetParameter(1);
	       
	       sigmaEvent = fEvent->GetParameter(2);
	       sigmaPed = fPed->GetParameter(2);
	       
	       constantEvent = fEvent->GetParameter(0);
	       constantPed = fPed->GetParameter(0);
	       
	       sigmaPedHist->Fill(sigmaPed);
	       
	       if (meanEvent <= meanPed) {
		 sigmaEventHist->Fill(sigmaEvent);
	       }
	       
	       sigmaEventHist->Fill(sigmaEvent);
	       
	      
	      // determine dynamic integration bounds and integrate the event
	      double upperIntegralBound = meanPed;
	      double lowerIntegralBound = meanEvent;
	      while (fEvent->Eval(lowerIntegralBound, 0, 0, 0) >0.1) {
	      	lowerIntegralBound -= 0.1;
	      }
	      while (fPed->Eval(upperIntegralBound, 0, 0, 0) > 0.1) {
	      	upperIntegralBound -= 0.1;
	      }
	      totalIntegralEvent = fEvent->Integral(lowerIntegralBound, upperIntegralBound)*numChargeBins/fabs(adcSpecUpperBound - adcSpecLowerBound); // * number of bins / histogram width // this ratio corrects the fit integral 

	       // calculate error of integral
	      //  Double_t totalIntegralError = 0;
	      //  for (int bin = 1; bin < smoothHist->GetNbinsX(); bin++ ) {
	      // 	 totalIntegralError+= smoothHist->GetBinError(bin);
	      //  }
	      // totalIntegralError /= smoothHist->GetNbinsX();

	      // Do the same for Monitor PMT spectrum
	       monPed = new TF1("monPedFit", "gaus", -spread, spread);
	       monEvent = new TF1("monEventFit", "gaus", -3.5, -0.25);
	       monSpecHist->Fit(monPed, "RMNQ0");
	       monSpecHist->Fit(monEvent, "RMNQ0");
	       c15->cd(1);
	        monPed->Draw("same");
	        monEvent->Draw("same");

	       monMeanEvent = monEvent->GetParameter(1);

	       double monUpper = monPed->GetParameter(1);
	       double monLower = monEvent->GetParameter(1);
	       while (monEvent->Eval(monLower, 0, 0, 0) > 0.1) {monLower -= 0.1;}
	       while (monPed->Eval(monUpper, 0, 0, 0) > 0.1) { monUpper -= 0.1;}
	       
	    
	      monitorIntegralEvent = monEvent->Integral(monLower, monUpper)*numChargeBins/fabs(monSpecUpperBound - monSpecLowerBound); 
	    }

	
	    if (analysisType == 1 || analysisType == 2) {

	      /*==============================================
	       *TIME SPECTRA FITTING AND ANALYSIS
	       *==============================================
	       */
	    
	      fTTS = new TF1("TDCFit", "gaus"); 
	    
	      fTTS->SetRange(260,320);
	      timeHist->Fit(fTTS, "RQ0");
	      c3->cd(1);
	      fTTS->Draw("same");
	    
	      int bin1 = timeHist->FindFirstBinAbove(timeHist->GetMaximum()/2.0);
	      int bin2 = timeHist->FindLastBinAbove(timeHist->GetMaximum()/2.0);
	      double fwhm = timeHist->GetBinCenter(bin2) - timeHist->GetBinCenter(bin1);

	      fwhmHist->Fill(fwhm);
	      posHist->Fill(timeHist->GetBinCenter(timeHist->GetMaximumBin()));

	      if(fTTS->GetNDF()>0){
	        chiSquareHistTTS->Fill(fTTS->GetChisquare()/fTTS->GetNDF());
	      }
	     
	      monTimeFit = new TF1("Monitor PMT Time FIT", "gaus");
	      monTimeFit->SetRange(140, 200);
	      monTimeHist->Fit(monTimeFit, "RQ0");
	      c19->cd(1);
	      monTimeFit->Draw("same");


	    }


	    // Update current pad(s)
	    if (analysisType == 0 || analysisType == 2) {
	      c2->cd(1);
	      gPad->Modified();
	      gPad->Update();
	      c15->cd(1);
	      gPad->Modified();
	      gPad->Update();
	    }
	    if (analysisType == 1 || analysisType == 2) {
	      c3->cd(1);
	      gPad->Modified();
	      gPad->Update();
	      c19->cd(1);
	      gPad->Modified();
	      gPad->Update();
	    }

	    // Correct x and y to absolute coordinates
    	    if(whichPlot==PMT_ABS_COORD){
		
	      //Code to calculate point of incidence of laser beam on PMT:
	      gantryrot *= PI/180; //convert to radians
	      gantrytilt *= PI/180;
	      
	      if(whichGantry==0){
		//Corrections for gantry 0 optical box
		final_offset_x = g0_offset1*cos(gantryrot)*cos(gantrytilt) - g0_offset2*sin(gantryrot) + g0_offset3*cos(gantryrot)*sin(gantrytilt);
		final_offset_y = g0_offset1*sin(gantryrot)*cos(gantrytilt) + g0_offset2*cos(gantryrot) + g0_offset3*sin(gantryrot)*sin(gantrytilt);
	      }
	      else{
		//Corrections for gantry 1 optical box
		gantryrot+=PI;
		final_offset_x = g1_offset1*cos(gantryrot)*cos(gantrytilt) - g1_offset2*sin(gantryrot) + g1_offset3*cos(gantryrot)*sin(gantrytilt);
		final_offset_y = g1_offset1*sin(gantryrot)*cos(gantrytilt) + g1_offset2*cos(gantryrot) + g1_offset3*sin(gantryrot)*sin(gantrytilt);
	      }
	      
	      //Calculating the point of incidence of laser beam
	      if(beamLength*cos(gantrytilt)<0){
		beamLength *= -1; //making sure that the value of beamLength*cos(gantrytilt) is always positive
		//to ensure that the sign of the x and y components of the beam trajectory
		//are only dictated by the rotation angle of gantry0
	      }
	      
	      x_inc = xposition + final_offset_x + beamLength*cos(gantryrot)*cos(gantrytilt);
	      y_inc = yposition + final_offset_y + beamLength*sin(gantryrot)*cos(gantrytilt);
	      
	      x = x_inc;
	      y = y_inc;
    	    } else { 
	      //for plots of gantry position
	      x = xposition;
	      y = yposition;
    	    }
    
	    // The real number of points is the total number of triggers - the number of misfires
	    double eventPoints = (val_points - filteredPoints);
	    
	    std::cout << "val_points: " << val_points << std::endl;
	    std::cout << "eventPoints: " << eventPoints << std::endl;
	    std::cout << "filteredPoints: " << filteredPoints << std::endl;

	    // Track the proper trigger rate
	    triggerSlice[i] = eventPoints/(double)val_points;
	    if (monMeanEvent < 0) {
	      monGainSlice[i] = -monMeanEvent;
	    }
	    monDetEffSlice[i] = monitorIntegralEvent/(double)eventPoints;
	 
	    // Fill plots and fit result histograms
	    if (val_points != 0) {
	      if (analysisType == 0 || analysisType == 2 ) {
		// Find peak and valley of event
		xVal = fInter->GetMinimumX();
		yVal = fEvent->Eval(xVal, 0, 0, 0);
		yPeak = fEvent->GetMaximum(xVal-10, xVal+10, 1.E-10, 100, false);
	      
		// Fill count histograms
		monEfficiency = monPulseCount/eventPoints;
		monCountHist->Fill(roundToThousandths(x), roundToThousandths(y), monPulseCount/eventPoints);
		pmtCountHist->Fill(roundToThousandths(x), roundToThousandths(y), pmtPulseCount/eventPoints);
		simulCountHist->Fill(roundToThousandths(x), roundToThousandths(y), simulPulseCount/eventPoints);
		
		//	std::cout << "pmtPulseCount = " << pmtPulseCount << std::endl;
		// Fill in monitor PMT det eff 2D plot
		if (monitorIntegralEvent/eventPoints > 0.02 && monEvent->GetChisquare()/monEvent->GetNDF() < 3.0) {
		  monPositionHist->Fill(roundToThousandths(x), roundToThousandths(y), monitorIntegralEvent/eventPoints);
		} else {
		  monPositionHist->Fill(roundToThousandths(x), roundToThousandths(y), 0.001);
		}
		
		// Fill in gain and det eff plots
		// Filter out false events	
		if (totalIntegralEvent/eventPoints>0.035) {  // integral filter usually at 0.03
		 
		  gainPositionHist->Fill(roundToThousandths(x), roundToThousandths(y),  meanPed - meanEvent); 
		  detPositionHist->Fill(roundToThousandths(x), roundToThousandths(y), totalIntegralEvent/eventPoints);
		  pvRatioHist->Fill(roundToThousandths(x), roundToThousandths(y), yPeak/yVal);

		  peakValleyRatioHist->Fill(yPeak/yVal);
		  gain1DHist->Fill(meanPed - meanEvent);
		  //  binCheckHist->Fill(roundToThousandths(x)+0.002,roundToThousandths(y)+0.002);
		  // chiSquareHistSPE->Fill((fEvent->GetChisquare())/(fEvent->GetNDF()));
		  // chiSquareHistPed->Fill((fPed->GetChisquare())/(fPed->GetNDF()));
	    
		}  else {
		  gainPositionHist->Fill(roundToThousandths(x), roundToThousandths(y), 0.1);
		  detPositionHist->Fill(roundToThousandths(x), roundToThousandths(y), 0.001);
		  pvRatioHist->Fill(roundToThousandths(x), roundToThousandths(y), 0.001);
		  peakValleyRatioHist->Fill(0.001);
		  //  binCheckHist->Fill(roundToThousandths(x)+0.002,roundToThousandths(y)+0.002);
		}
	      }
	      
	      // Fill in time plots
	      if (analysisType == 1 || analysisType == 2) {
		
		if (timeHist->Integral(timeHist->GetXaxis()->FindBin(260), timeHist->GetXaxis()->FindBin(320))/eventPoints > 0.025 && fTTS->GetParameter(2) < 15) {
	
		  ttsPositionHist->Fill(roundToThousandths(x), roundToThousandths(y), fTTS->GetParameter(2));
		  peakPositionHist->Fill(roundToThousandths(x), roundToThousandths(y),fTTS->GetParameter(1));
		} else {
		  ttsPositionHist->Fill(roundToThousandths(x), roundToThousandths(y), 0.01);
		  peakPositionHist->Fill(roundToThousandths(x), roundToThousandths(y),0.01);
		}
	      }
	    }
      
	    // delete fits as a way to clear draws
	    // reset and prepare histograms for next data point
	    if (analysisType == 0 || analysisType == 2) {
	      delete fPed;
	      delete fEvent;
	      delete fInter;
	      delete monPed;
	      delete monEvent;
	    }
	    if (analysisType == 1|| analysisType == 2) {
	      delete fTTS;
	      delete monTimeFit;
	    }
      }  
      
      adcSpecHist->Reset();
      smoothHist->Reset();
      waveFitHist->Reset();
      timeHist->Reset();
      timeFitHist->Reset();
      monSpecHist->Reset();
      monTimeHist->Reset();
      monPulseCount = 0;
      pmtPulseCount = 0;
      simulPulseCount = 0;
      filteredPoints = 0;
    }
    

    /*==============================================================
     * DISPLAY PLOTS
     *=========================================================
     */
    

     // Create a graph to track the proper trigger rate and monitor data
    TGraph *triggerGraph = new TGraph(arrayLength, arr_x, triggerSlice);
    TGraph *monGainGraph = new TGraph(arrayLength, arr_x, monGainSlice);
    TGraph *monDetEffGraph = new TGraph (arrayLength, arr_x, monDetEffSlice);
    
    
    triggerGraph->SetTitle("Percentage of Proper Triggers Per Point");
    triggerGraph->SetMaximum(1.0);
    triggerGraph->SetMinimum(0);
    c18->cd(1);
    triggerGraph->Draw("AP");
    
    monGainGraph->SetTitle("Monitor PMT Gain");
    c18->cd(2);
    monGainGraph->Draw("AP");
    
    monDetEffGraph->SetTitle("Monitor PMT Detection Efficiency over Time");
    monDetEffGraph->SetMinimum(0.);
    monDetEffGraph->SetMaximum(1.);
    c18->cd(3);
    monDetEffGraph->Draw("AP");
    
    c7->cd(1);
    gainPositionHist->Draw("colz");
    
    c8->cd(1);
    detPositionHist->Draw("colz");
    
    // c9->cd(1);
    // pvRatioHist->Draw("colz");
    
    // c9->cd(1);
    // binCheckHist->Draw("colz"); //check for double binning?
    
    c16->cd(1);
    monCountHist->Draw("colz");
    c16->cd(2);
    pmtCountHist->Draw("colz");
    c16->cd(3);
    simulCountHist->Draw("colz");
    c16->cd(4);
    monPositionHist->Draw("colz");
         
    c10-> cd(1); 
    ttsPositionHist->Draw("colz");
    
    c11->cd(1);
    peakPositionHist->Draw("colz");

    //Intended for getting plots of fitting parameters for ADC spectrum
    //  c6->Update();

    /***********************************************************************************************************************
     *Uncomment to plot a circle to find the PMT center
     * currently using det eff, but can do other histograms. Change parameters in myFitCircle() accordingly.
     **********************************************************************************************************************/

    //   h2 = (TH2D*)detPositionHist->Clone("h2");
    //   h2_circ = new TH2D("circleFit", "circleFitHist", 75, -0.005, 0.745, 75, -0.005, 0.745); 
    
    //   //Fit a circle to the graph points
    //   TVirtualFitter::SetDefaultFitter("Minuit");  //default is Minuit
    //   TVirtualFitter *fitter = TVirtualFitter::Fitter(0, 3);
    //   fitter->SetFCN(myFitCircle);
    //   fitter->SetPrecision(1e-8);
    
    //   fitter->SetParameter(0, "x0",   0.39, 0.01, 0, 0.74);
    //   fitter->SetParameter(1, "y0",   0.29, 0.01, 0, 0.69);
    //   fitter->SetParameter(2, "R",    0.3, 0.05, 0,1);
    
    //   Double_t arglist[1] = {0};
    //   fitter->ExecuteCommand("MIGRAD", arglist, 0);
    
    // // Note : tried HEsse and removing the one center point (run 838, but no
    // // improvement in x_c, y_c error calc)
    // //Draw the circle on top of the points
    //   TArc *arc = new TArc(fitter->GetParameter(0),
    // 			 fitter->GetParameter(1),fitter->GetParameter(2));
    
    //   TMarker *center = new TMarker(fitter->GetParameter(0),
    // 				  fitter->GetParameter(1),29);
    //   center->SetMarkerSize(1.2);
    //   c8->cd(1);
    //   h2_circ->SetMarkerStyle(7);
    //   h2_circ->Draw("same");
    //   arc->SetFillStyle(0);
    //   arc->SetFillColor(10);
    //   arc->SetLineColor(kRed);
    //   arc->SetLineWidth(2);
    //   arc->Draw("same");
 //   center->Draw("same");
}

int main(){
    digiAna_v3();
    return 0;
}
