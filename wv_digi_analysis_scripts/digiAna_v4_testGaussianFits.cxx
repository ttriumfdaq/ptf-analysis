// A slightly deprecated and prototype version of digiAna_v4.cxx in which the pulses are flipped to be positive and fitted by default ROOT gaussians
// A work in progress, and intended to see which fitting method is better
// Fitting the waveforms greatly reduces the timing uncertainty

#include "TAttFill.h"
#include "THStack.h"
#include "TTree.h"
#include "TBranch.h"
#include "time.h"
#include "stdio.h"
#include "unistd.h"
#include <iostream>
#include <sstream>
#include "TGraph.h"
#include "TH1F.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TH2D.h"
#include "TColor.h"
#include "TStyle.h"
#include "TString.h"
#include "TMath.h"
#include "TH1D.h"
#include "TGraph2D.h"
#include <string>
#include <cstring>
#include "TROOT.h"

Double_t roundToThousandths(Double_t x){
    x*=1000;	
    return floor(x+0.5)/1000;
}

void digiAna_v4_testGaussianFits() {
 
  /*============================================================================================
    // Long Section of Colormap definitions (Beautification of Plots)
    // ==========================================================================================*/
  
  //Segev BenZvi's Colour palette (http://ultrahigh.org/2007/08/20/making-pretty-root-color-palettes/)                                          
  
  // Plot style settings
  const Int_t NRGBs = 5;
  const Int_t NCont = 255;
  
  Int_t Palette_style[NCont];
  Double_t stops[NRGBs] = {0.00, 0.34, 0.61, 0.84, 1.00};
  Double_t red[NRGBs] = {0.00, 0.00, 0.87, 1.00, 0.51};
  Double_t green[NRGBs] = {0.00, 0.81, 1.00, 0.20, 0.00};
  Double_t blue[NRGBs] = {0.51, 1.00, 0.12, 0.00, 0.00};
  Int_t FI = TColor::CreateGradientColorTable(NRGBs,stops,red,green,blue,NCont);
  for (int i=0; i<NCont; i++) Palette_style[i] = FI+i;
  gStyle->SetNumberContours(NCont);
  
  
  gROOT->ForceStyle();
  gStyle->SetOptStat(0);
  gStyle->SetCanvasColor(0);
  gStyle->SetTitleColor(1);
  gStyle->SetStatColor(0);
  gStyle->SetFrameFillColor(0);
  gStyle->SetPadColor(0);
  gStyle->SetPadTickX(1);
  gStyle->SetPadTickY(1);
  gStyle->SetTitleSize(0.04);
  gStyle->SetCanvasBorderMode(0);
  gStyle->SetFrameBorderMode(0);
  gStyle->SetFrameLineWidth(2);
  gStyle->SetPadBorderMode(0);
  gStyle->SetPalette(NCont, Palette_style);
  gStyle->SetTitleAlign(23);
  gStyle->SetTitleX(.5);
  gStyle->SetTitleY(0.99);
  gStyle->SetTitleBorderSize(0);
  gStyle->SetTitleFillColor(0);
  gStyle->SetHatchesLineWidth(2);
  gStyle->SetLineWidth(1.5);
  gStyle->SetTitleFontSize(0.07);
  gStyle->SetLabelSize(0.05,"X");
  gStyle->SetLabelSize(0.05,"Y");
  gStyle->SetTitleSize(0.04,"X");
  gStyle->SetTitleSize(0.04,"Y");
  gStyle->SetTitleBorderSize(0);
  gStyle->SetCanvasBorderMode(0);
  // Pad & Canvas Options
  gStyle->SetPadColor(10);
  gStyle->SetPadTickX(1);
  gStyle->SetPadTickY(1);
  gStyle->SetCanvasColor(10);
  gStyle->SetOptStat(0);
  gStyle->SetPadGridX(kTRUE);
  gStyle->SetPadGridY(kTRUE);
  
  // Frame Options
  gStyle->SetFrameLineWidth(1);
  gStyle->SetFrameFillColor(10);
  
  // Marker Options
  gStyle->SetMarkerStyle(20);
  gStyle->SetMarkerSize(0.8);
  
  gStyle->SetTitleSize(0.05,"x");//X-axis title size
  gStyle->SetTitleSize(0.05,"y");
  gStyle->SetTitleSize(0.05,"z");
  gStyle->SetLabelSize(0.05,"x");//X-axis title size
  gStyle->SetLabelSize(0.05,"y");
  gStyle->SetLabelSize(0.05,"z");
  
  //Initialize and define global constants *some of these are temporary
  const double PI = 3.141592653589793238463;
  const int GANTRY_POS = 0;
  const int PMT_ABS_COORD = 1;
  const double voltageConversion = 2000.0/(50*16384); //2000 mV setting on V1730 digitizer, 50 ohm impedance, 14-bit = 16384 possibilities -- converts digital voltage to analog
  
  int whichGantry;
  int whichPlot;

  //Gets the run to be analyzed as input from the user
  TString runNumber;
  Int_t analysisType;
  Int_t writeFile;
  int intensity;
  int corrections;

  // Requires various inputs from user corresponding to run
  std::cout << "Enter run number: " << std::endl;
  std::cin >> runNumber;
  
  std::cout << "\nEnter gantry used: " << std::endl;
  std::cin>>whichGantry;
  
  std::cout << "\nChoose analysis for charge (0), time (1), or both (2):" <<std::endl;
  std::cin>>analysisType;
  
  std::cout<<"\nFor low intensity enter 0, otherwise for high Intensity"<<std::endl;
  std::cin>>intensity;
  
  std::cout <<"\nInput 1 to apply temperature corrections (work in progress), input any other integer otherwise." << std::endl;
  std::cin >> corrections;

  // Set up file Input file
  TFile *f;
  f = new TFile("/data14b/kxie/rootfiles/out_run0" + runNumber + ".root", "read"); 
 
  //Variables holding number of positions
  Int_t numEntries0;
  Int_t numEntries1;
  Int_t val_points;
  double currPoints;

  //Positional Variables 
  Double_t xVal = 0;
  Double_t yVal = 0;
  Double_t yPeak = 0;
  Double_t xposition, yposition; // gantry position
  Double_t gantrytilt;
  Double_t gantryrot;
  Double_t x_inc, y_inc; //point of incidence of laser beam
  Double_t x, y; //point plotted on gain and detection efficiency plots
  Double_t final_offset_x, final_offset_y; //Corrections for optical box
  Double_t beamLength = 0.05; // (26Apr2017): changed from 0.1m to 0.05m
  
  //Hard limits of gantry space in x and y
  Double_t gantryXlimit = 0.749;
  Double_t gantryYlimit = 0.696;
  
  //offset in laser aperture with respect to gantry0 position
  Double_t g0_offset1 = 0.115;
  Double_t g0_offset2 = 0.042;
  Double_t g0_offset3 = 0.0365;
  
  //offset in laser aperture with respect to gantry1 position
  Double_t g1_offset1 = 0.115;
  Double_t g1_offset2 = 0.052;
  Double_t g1_offset3 = 0.0395;
  
  // Local variables for ADC pedestal and event fitting
  Double_t p_lo_bound;
  Double_t p_hi_bound;
  Int_t binmax;
  Double_t x_pos;
  
  // Charge spectra gaussian fit results
  Double_t meanEvent;
  Double_t meanPed;
  Double_t monMeanEvent;
  Double_t monMeanPed;
  
  //spread of pedestal fit; can be refined
  //generally the best value captures the top of the pedestal; this allows the fit to avoid background influence
  Double_t spread = 0.3;
  
  // Integrals of pmt and mon waves
  Double_t adcIntegralValue;
  Double_t monIntegralValue;
  
  // Pre-allocated arrays holding digitizer data by channel
  Double_t arr0[200000][70]; //Preallocated for speed -- PMT // check to make sure rows (currently 70) match the actual file output
  Double_t arr1[200000][70]; //Preallocated for speed -- ref
  Double_t arr3[200000][70]; // G0 monitor PMT
  Double_t arr5[200000][70]; // G1 monitor PMT
  
  //Creates pointer to TTree contained in the input file and pointers to its relevant branches
  // Numbers refer to channels on digitizer. 
  // Channel     Data
  // 0           Primary PMT 
  // 1           Trigger signal
  // 2           Gantry0 Receiver PMT
  // 3           Gantry0 Monitor PMT
  // 4           Gantry1 Receiver PMT
  // 5           Gantry1 Monitor PMT
  
  TTree* T = (TTree*)f->Get("scan_tree");
  TBranch *branch0 = T->GetBranch("V1730_wave0"); //branch containing 2d array of samples for pmt signal
  TBranch *branch1 = T->GetBranch("V1730_wave1"); //branch containing 2d array of samples for reference pulse
  TBranch *branch3 = T->GetBranch("V1730_wave3"); // 2d array of samples for G0 monitor PMT pulse
  TBranch *branch5 = T->GetBranch("V1730_wave5"); // branch containing 2d array of samples for G1 monitor PMT pulse
  TBranch *points = T->GetBranch("num_points"); //gets the number of points in the scan
  
  // read in the chosen gantry data
  TBranch *gantry_x;
  TBranch *gantry_y;
  TBranch *gantry_tilt;
  TBranch *gantry_rot;
  if (whichGantry==0) {
    gantry_x = T->GetBranch("gantry0_x");
    gantry_y = T->GetBranch("gantry0_y");
    gantry_tilt = T->GetBranch("gantry0_tilt");
    gantry_rot = T->GetBranch("gantry0_rot");
    } else{
    gantry_x = T->GetBranch("gantry1_x");
    gantry_y = T->GetBranch("gantry1_y");
    gantry_tilt = T->GetBranch("gantry1_tilt");
    gantry_rot = T->GetBranch("gantry1_rot");
  }
  
    //defines total number of entries for indexing bound
  numEntries0 = branch0->GetEntries();
  numEntries1 = branch1->GetEntries();
  
  //Sets where the value read in by GetEntry() is stored
  gantry_x->SetAddress(&xposition);
  gantry_y->SetAddress(&yposition);
  gantry_tilt->SetAddress(&gantrytilt);
  gantry_rot->SetAddress(&gantryrot);
  points->SetAddress(&val_points);
  branch0->SetAddress(arr0);
  branch1->SetAddress(arr1);
  branch3->SetAddress(arr3);
  branch5->SetAddress(arr5);
  branch0->SetAutoDelete(kFALSE);
  branch1->SetAutoDelete(kFALSE);
  branch3->SetAutoDelete(kFALSE);
  branch5->SetAutoDelete(kFALSE);

  // ========================================================================
  //Create Histograms 
  // ========================================================================
  
  //output root file for all the histograms 
  TFile *Plots = new TFile("plots_run0" + runNumber + ".root","RECREATE");
  if(Plots->IsOpen())printf("File opened successfully\n");
  
  //Histograms for channel 0 and channel 1 waveforms (digitizer data histograms)
  TH1D* waveFormHist0 = new TH1D("WaveformHistogram0", "Histogram of Raw WaveForm 0 / PMT Signal", 70, 0, 70); // TH1D(name, title, # of bins, left edge, right edge) 
  TH1D* waveFormHist1 = new TH1D("WaveformHistogram1","Histogram of Raw Waveform 1 / Reference Trigger", 70, 0, 70); 
  
  // Histogram for channel 3 and 5 waveforms (G0 and G1 Monitor PMT)
  TH1D* waveFormMonG0 = new TH1D("WaveformMonG0", "Histogram of Raw Waveform 3 / Gantry0 Monitor PMT Signal", 70, 0, 70);
  TH1D* waveFormMonG1 = new TH1D("WaveformMonG1", "Histogram of Raw Waveform 5 / Gantry1 Monitor PMT Signal",70, 0, 70);
  
  // Charge spectrum plotting settings
  int numChargeBins;
  double adcSpecLowerBound;
  double adcSpecUpperBound;
  double monSpecLowerBound;
  double monSpecUpperBound;
  if (intensity == 0){ 
    numChargeBins = 117;
    adcSpecLowerBound = 9.5;
    adcSpecUpperBound = -3.5;
    monSpecLowerBound = 5;
    monSpecUpperBound = -2;
  }
  else{
    numChargeBins = 353;
    adcSpecLowerBound = 600;
    adcSpecUpperBound = -3.;
    monSpecLowerBound = 350;
    monSpecUpperBound = -3.;
  }
  
  //Histogram for timing data for a single point
  TH1D* timeHist = new TH1D("Plot of Transit Time", "TDCHist", 30, 260, 320); 
  TH1D *fallingTimeHist = new TH1D("Falling Edge Transit Time", "Falling TDCHist", 30, 240, 300); 
  TH1D *timeFitHist = new TH1D("Transit Time Distribution from fitted Waveforms", "timeFitHist", 60, 260, 320);

  // 1D Histograms for various PMT and Monitor PMT parameters
  TH1F *pmtGain1DHist; 
  TH1F *pmtGainCorr1DHist;
  TH1F *monHitEff1DHist; 
  TH1F *monGain1DHist;
  
  TH1F *monDet1DHist; 
  TH1F *pmtDet1DHist;
  TH1F *pmtDetCorr1DHist;
  TH1F *pmtTts1DHist;
  TH1F *pmtTtp1DHist;
  if (intensity == 0){
    pmtGain1DHist = new TH1F("pmtGain1DHist","Gain of PMT in ADC Counts", 300, 0, 5); 
    monHitEff1DHist = new TH1F("monHitEff1DHist", "Mon PMT Hit Eff", 300, 0., 0.5);
    monGain1DHist = new TH1F("monGain1DHist", "Mon PMT gain", 300, 0., 2.);
    monDet1DHist = new TH1F("monDet1DHist", "Mon PMT detection effeciency", 300, 0., 2.);
    pmtDet1DHist = new TH1F("pmtDet1DHist", "Det Eff of PMT", 300, 0, 0.5);
    pmtDetCorr1DHist = new TH1F("pmtDetCorr1DHist", "Corrected Det Eff of PMT", 300, 0., 0.5);
    pmtTts1DHist = new TH1F("pmtTts1DHist", "TTS of PMT", 300, 0., 6.);
    pmtTtp1DHist = new TH1F("pmtTtp1DHist", "TTP of PMT", 300, 270., 300.);
  } else {
    // have to set different parameters for high intensity scans
    pmtGain1DHist = new TH1F("pmtGain1DHist","Gain of PMT in ADC counts", 300, 150, 250.);
    pmtDet1DHist = new TH1F("pmtDet1DHist", "Det Eff of PMT", 300, 0., 1.1);  
    pmtTts1DHist = new TH1F("pmtTts1DHist", "TTS of PMT", 300, 0., 10.);
    pmtTtp1DHist = new TH1F("pmtTtp1DHist", "TTP of PMT", 300, 260., 320.);
    monHitEff1DHist = new TH1F("monHitEff1DHist", "Mon PMT Hit Eff", 300, 0., 1.1);
    monGain1DHist = new TH1F("monGain1DHist", "Mon PMT gain", 300,30, 50);
    monDet1DHist = new TH1F("monDet1DHist", "Mon PMT detection effeciency", 300, 0., 1.1);
    pmtGainCorr1DHist = new TH1F("pmtGainCorr1DHist", "corrected PMT Gain at high intensity", 300, 150, 250);
  }
  
  /* -----------------Create PMT Positional Histograms-----------------------------
     / gainPosCorrHist: Positional Gain after applying temperature corrections
     / gainPosHist: Gain as a function of position
     / detPosHist: Primary PMT Detection Efficiency as a function of position
     / detPosCorrHist: Primary PMT Det Eff after applying temperature corrections
     / binCheckHist: Makes sure no errors from double binning (precautionary)
     / pvRatioHist: Peak to Valley Ratio 
     / unusualpoints: Intended to check for strange data points or bad fits
     / ttsPosHist: Transit Time Spread
     / ttpPosHist: Relative Transit Time 
     / monDetPosHist: Monitor PMT Detection Efficiency
     / monHitPosHist: Monitor PMT Hit Efficiency (Percentage of incident photons that produce a pulse)
     / pmtHitPosHist: Primary PMT Hit Efficiency
     / -----------------------------------------------------------------------------*/


  //NEED TO EDIT THESE PARAMETERS TO CHANGE RESOLUTION! 
  
  // 2cm Resolution  
  // TH2D *gainPosCorrHist = new TH2D("gainPosCorrHist", "GainCorrectionHistogram; x-positions(m); y-position(m)", 37, 0., 0.749, 35, 0., 0.696);
  // TH2D *gainPosHist = new TH2D("gainPosHist", "Gain from Pedestal as a Function of Gantry Position (pC); x-position(m); y-position(m)",37, 0., 0.749, 35, 0., 0.696);
  // TH2D *detPosHist = new TH2D("detPosHist", "Relative Detection Efficiecy as a Function of Gantry Position; x-position(m); y-position(m)",      37, 0., 0.749, 35, 0., 0.696);
  // TH2D *detPosCorrHist = new TH2D("detPosCorrHist", "detCorrEffHistogram; x-positions(m); y-position(m)", 37, 0., 0.749, 35, 0., 0.696);
  // TH2D *binCheckHist = new TH2D("binningCheck","PositionBinningHistogram; x-position(m); y-position(m)" ,                  37, 0., 0.749, 35, 0., 0.696);
  // TH2D *pvRatioHist = new TH2D("pvRatioHist","peak to valley ratio as a function of Gantry position; x-position(m) ",        37, 0., 0.749, 35, 0., 0.696);
  // TH2D *unusualPoints = new TH2D("unusualPointshist","Null Entry Check Histogram; x-position(m);y-position(m)",37, 0., 0.749, 35, 0., 0.696);
  // TH2D *ttsPosHist = new TH2D("ttsPosHist", "Transit Time Spread in Sigmas (nsec); x-position(m); y-position(m)",37, 0., 0.749, 35, 0., 0.696);
  // TH2D *ttpPosHist = new TH2D("ttpPosHist", "Relative Transit Time (nsec); x-position(m);y-position(m)",  37, 0., 0.749, 35, 0., 0.696);
  // TH2D *monDetPosHist = new TH2D("monDetPosHist", "Monitor PMT Detection Efficiency; x-position(m); y-positon(m)", 37, 0., 0.749, 35, 0., 0.696);
  // TH2D *monHitPosHist = new TH2D("monHitPosHist", "Mon PMT Hit Efficiency; x-position(m); y-positon(m)",37, 0., 0.749, 35, 0., 0.696);
  // TH2D *pmtHitPosHist = new TH2D("pmtHitPosHist", "SK PMT Hit Efficiency; x-position(m); y-position(m)",37, 0., 0.749, 35, 0., 0.696);

  //1cms Resolution                                                                                        
  // TH2D *gainPosCorrHist = new TH2D("gainPosCorrHist", "GainCorrectionHistogram; x-positions(m); y-position(m)", 75, 0.0, 0.749, 70, 0.0, 0.696);
  // TH2D *gainPosHist = new TH2D("gainPosHist", "Gain from Pedestal as a Function of Gantry Position (pC); x-position(m); y-position(m)",75, 0.0, 0.749, 70, 0.0, 0.696);
  // TH2D *detPosHist = new TH2D("detPosHist", "Relative Detection Efficiecy as a Function of Gantry Position; x-position(m); y-position(m)",     75, 0.0, 0.749, 70, 0.0, 0.696);  
  // TH2D *detPosCorrHist = new TH2D("detPosCorrHist", "detCorrEffHistogram; x-positions(m); y-position(m)",  75, 0.0, 0.749, 70, 0.0, 0.696);
  // TH2D *binCheckHist = new TH2D("binningCheck","PositionBinningHistogram; x-position(m); y-position(m)" ,       75, 0.0, 0.749, 70, 0.0, 0.696);                 
  // TH2D *pvRatioHist = new TH2D("pvRatioHist","peak to valley ratio as a function of Gantry position; x-position(m) ", 75, 0.0, 0.749, 70, 0.0, 0.696);         
  // TH2D *unusualPoints = new TH2D("unusualPointshist","Null Entry Check Histogram; x-position(m);y-position(m)",75, 0.0, 0.749, 70, 0.0, 0.696);
  // TH2D *ttsPosHist = new TH2D("ttsPosHist", "Transit Time Spread in Sigmas (nsec); x-position(m); y-position(m)", 75, 0.0, 0.749, 70, 0.0, 0.696);
  // TH2D *ttpPosHist = new TH2D("ttpPosHist", "Relative Transit Time (nsec); x-position(m);y-position(m)",  75, 0.0, 0.749, 70, 0.0, 0.696);
  // TH2D *monDetPosHist = new TH2D("monDetPosHist", "Monitor PMT Detection Efficiency; x-position(m); y-positon(m)",  75, 0.0, 0.749, 70, 0.0, 0.696); 
  // TH2D *monHitPosHist = new TH2D("monHitPosHist", "Mon PMT Hit Efficiency; x-position(m); y-positon(m)",75, 0.0, 0.749, 70, 0.0, 0.696);
  // TH2D *pmtHitPosHist = new TH2D("pmtHitPosHist", "SK PMT Hit Efficiency; x-position(m); y-position(m)", 75, 0.0, 0.749, 70, 0.0, 0.696);
  

  //5mm Resolution 
  TH2D *gainPosCorrHist = new TH2D("gainPosCorrHist", "GainCorrectionHistogram; x-positions(m); y-position(m)",  149, 0.0, 0.749, 139, 0.0, 0.696); 
  TH2D *gainPosHist = new TH2D("gainPosHist", "Gain from Pedestal as a Function of Gantry Position (pC); x-position(m); y-position(m)", 149, 0.0, 0.749, 139, 0.0, 0.696);    
  TH2D *detPosHist = new TH2D("detPosHist", "Relative Detection Efficiecy as a Function of Gantry Position; x-position(m); y-position(m)", 149, 0.0, 0.749, 139, 0.0, 0.696);       
  TH2D *detPosCorrHist = new TH2D("detPosCorrHist", "detCorrEffHistogram; x-position(m); y-position(m)",  149, 0.0, 0.749, 139, 0.0, 0.696);  
  TH2D *binCheckHist = new TH2D("binningCheck","PositionBinningHistogram; x-position(m); y-position(m)" , 149, 0.0, 0.749, 139, 0.0, 0.696);                               
  TH2D *pvRatioHist = new TH2D("pvRatioHist","peak to valley ratio as a function of Gantry position; x-position(m) ", 149, 0.0, 0.749, 139, 0.0, 0.696);          
  TH2D *unusualPoints = new TH2D("unusualPointshist","Null Entry Check Histogram; x-position(m);y-position(m)",149,0.0, 0.749, 139, 0.0, 0.696); 
  TH2D *ttsPosHist = new TH2D("ttsPosHist", "Transit Time Spread in Sigmas (nsec); x-position(m); y-position(m)", 149, 0.0, 0.749, 139, 0.0, 0.696);
  TH2D *ttpPosHist = new TH2D("ttpPosHist", "Relative Transit Time from Peak(nsec); x-position(m);y-position(m)",  149, 0.0, 0.749, 139, 0.0, 0.696);
  TH2D *fallingEdgeTimePosHist = new TH2D("fallingEdgeTimePosHist", "Relative Transit Time from Falling Edge (nsec); x-position(m); y-position(m)", 149, 0.0, 0.749, 139, 0.0, 0.696);
  TH2D *fitTimePosHist = new TH2D("fitTimePosHist", "Relative Transit Time from Fitted Waveforms (nsec); x-position(m); y-position(m)", 149, 0.0, 0.749, 139, 0.0, 0.696);
  TH2D *fitSpreadPosHist = new TH2D("fitSpreadPosHist", "Transit Time Spread from Fitted Waveforms (nsec); x-position(m); y-position(m)", 149, 0.0, 0.749, 139, 0.0, 0.696);
  TH2D *monDetPosHist = new TH2D("monDetPosHist", "Monitor PMT Detection Efficiency; x-position(m); y-positon(m)", 149, 0.0, 0.749, 139, 0.0, 0.696);    
  TH2D *monHitPosHist = new TH2D("monHitPosHist", "Mon PMT Hit Efficiency; x-position(m); y-positon(m)", 149, 0.0, 0.749, 139, 0.0, 0.696); 
  TH2D *pmtHitPosHist = new TH2D("pmtHitPosHist", "SK PMT Hit Efficiency; x-position(m); y-position(m)", 149, 0.0, 0.749, 139, 0.0, 0.696); 
  

  // Define start point and end points of current analysis 
  // For full analysis: set 0 to numEntries0 
  // For partial analysis: set whatever you want
  int startPoint = 0;
  int endPoint = numEntries0;

  /* Long section of creating different directories for each analysis parameter
     / When saved, this allows looking at the analysis for every point individually in the resultant output file 
     / Presently, directories are subdivided into folders containing 200 points each*/

  // Directories for ADC charge spectra including smoothed spectra
  TH1D ** adcSpecHistperScan;
  TH1D ** smoothAdchistperScan;
  TDirectory * curdir = Plots->GetDirectory("/");
  TDirectory * adcspecdir = Plots->mkdir("AdcValueperScan_step");
  adcspecdir->cd();
  adcSpecHistperScan = new TH1D*[numEntries0];
  smoothAdchistperScan = new TH1D*[numEntries0];
  char adcname[100]={0};
  char adctitle[100]={0};
  char scanstepdir[10]={0};
  char smoothadcname[100]={0};
  char smoothadctitle[100]={0};
  for(int step = startPoint; step<endPoint; ++step){
    if(step% 200 == 0){
      sprintf (scanstepdir, "%05d",step);
      (adcspecdir->mkdir(scanstepdir))->cd();
    }
    sprintf(adcname,"scanstep%05d",step);
    sprintf(adctitle,"adcspec Value in the scan; ADCvalue;count/bin",step+1);
    adcSpecHistperScan[step] = new TH1D(adcname,adctitle, numChargeBins, adcSpecLowerBound, adcSpecUpperBound);
    sprintf(smoothadcname,"smoothscanstep%05d",step);
    sprintf(smoothadctitle,"adcspec Smooth Value in the scan; ADC calculated mean;count/bin",step+1);
    smoothAdchistperScan[step] = new TH1D(smoothadcname,smoothadctitle, numChargeBins, adcSpecLowerBound, adcSpecUpperBound);
  }
  curdir->cd();
  
    
  // Directories for monitor PMT ADC charge spectra
  TH1D ** monSpecHistperScan;
  TDirectory * curdir1 = Plots->GetDirectory("/");
  TDirectory * monspecdir = Plots->mkdir("MonValueperScan_step");
  monspecdir->cd();
  monSpecHistperScan = new TH1D*[numEntries0];
  char monname[100]={0};
  char montitle[100]={0};
  char scanstepdir1[10]={0};
  for(int step = startPoint; step<endPoint; ++step){
    if(step% 200 == 0){
      sprintf (scanstepdir1, "%05d",step);
      (monspecdir->mkdir(scanstepdir1))->cd();
    }
    sprintf(monname,"scanstep%05d",step);
    sprintf(montitle,"monspec Value for each step in the scan; MonValue; count/bin",step+1);
    monSpecHistperScan[step] = new TH1D(monname,montitle, numChargeBins, monSpecLowerBound, monSpecUpperBound);
  }
  curdir1->cd();
  
  // Directories for SK PMT time spectra
  TH1D ** timeHistperScan;
  TDirectory * curdir2 = Plots->GetDirectory("/");
  TDirectory * timedir = Plots->mkdir("TimeValueperScan_step");
  timedir->cd();
  timeHistperScan = new TH1D*[numEntries0];
  char timename[100]={0};
  char timetitle[100]={0};
  char scanstepdir2[10]={0};
  for(int step = startPoint; step<endPoint; ++step){
    if(step% 200 == 0){
      sprintf (scanstepdir2, "%05d",step);
      (timedir->mkdir(scanstepdir2))->cd();
    }
    sprintf(timename,"scanstep%05d",step);
    sprintf(timetitle,"Transit time Value for each step in the scan; TDC; count/bin",step+1);
    timeHistperScan[step] = new TH1D(timename,timetitle, 30., 260., 320.);
  }
  curdir2->cd();
    
  // Directories for monitor PMT time spectra  
  TH1D ** montimeHistperScan;
  TDirectory * curdir3 = Plots->GetDirectory("/");
  TDirectory * montimedir = Plots->mkdir(" monTimeValueperScan_step");
  montimedir->cd();
  montimeHistperScan = new TH1D*[numEntries0];
  char montimename[100]={0};
  char montimetitle[100]={0};
  char scanstepdir3[10]={0};
  for(int step = startPoint; step<endPoint; ++step){
    if(step% 200 == 0){
      sprintf (scanstepdir3, "%05d",step);
      (montimedir->mkdir(scanstepdir3))->cd();
    }
    sprintf(montimename,"scanstep%05d",step);
    sprintf(montimetitle,"Transit time Value for each step in the scan; TDC; count/bin",step+1);
    montimeHistperScan[step] = new TH1D(montimename,montimetitle, 30., 140., 100.);
  }
  curdir3->cd();
 
  // Initialize variables to hold data for analysis
  // Numbers refer to channels on digitizer. 
  // Channel     Data
  // 0           Primary PMT 
  // 1           Trigger signal
  // 2           Gantry0 Receiver PMT
  // 3           Gantry0 Monitor PMT
  // 4           Gantry1 Receiver PMT
  // 5           Gantry1 Monitor PMT
  Int_t maxBin0;
  Int_t maxBin1;
  Int_t maxBin3;
  Int_t maxBin5;
  Int_t sumSmooth;
  Double_t maximumVal;
  Double_t monMaximumVal;
  double threshold;

  // negative offsets applied to raw channel data in order to push them down to a baseline of 0 and allow integration
  double pmtOffset = -8135.4;
  double refOffset = -7500; // need to more accurately determine 
  double monitorOffset;

  // depending on intensity setting (high or low) change the threshold at which pulses are filtered between noise and PMT signal
  if (intensity == 0){
    threshold = 50;
  }
  else{
    threshold = 100;
  }
  
  // Slight change in monitor PMT offsets depending on which gantry is used
  if (whichGantry == 0) {
    monitorOffset = -8169;
  } else {
    monitorOffset = -8211;
  }
  
  // Count number of proper pulses/samples in each point
  Int_t pmtPulseCount = 0;
  Int_t monPulseCount = 0;
    
  // Define arrays for certain parameters and for viewing single point scans
  const int arrayLength = numEntries0;
  Double_t arr_x[arrayLength];
  for (int loop = 0; loop < arrayLength; loop++) {
    arr_x[loop] = loop*3; // points on x separated into 3 second slices // the actual number should be based on the time wait between points, 3 is simply the standard   
  }

  // Save x and y positions to array to produce correction histograms post-analysis
  Double_t x_posArray[arrayLength];
  Double_t y_posArray[arrayLength];
  
  // Needed for temperature corrections - arrays that hold parameters over time
   Double_t monHitEffSlice[arrayLength];
   Double_t monGainSlice[arrayLength];
   
   // for gain ratio April 20
   //Double_t pmtGainSlice[arrayLength];
  
  // Initialize variables for spectrum fitting and analysis
  char pedFit_name[40]; 
  char eventFit_name[40];
  char monPedFit_name[40];
  char monEventFit_name[40];
  char TDCFit_name[40];
  char monTimeFit_name[40];

  // Parameters related to monitor PMT and monitor PMT corrections
  double monWeight;
  double meanWeight;
  double monTime;
  double weightedSum;

  // holds integrals of PE events
  Double_t monitorIntegralEvent;
  Double_t totalIntegralEvent;
  double peakTime;
  double fallingEdgeTime;
  double timeSpread;
  double fitPeakTime;
  double fitTimeSpread;

  double pmtFitMean;
  double refFitMean;

  // functions for fitting charge/time spectra
  TF1* fPed;
  TF1* fEvent;
  TF1* monPed;
  TF1* fTTS;
  TF1* monEvent;
  TF1* fInter;
  TF1* monTimeFit;
  
  /*=====================================================
    /SETUP FINISHED. ANALYSIS BEGINS HERE
    /======================================================*/
  
  // Loops over all the points in the gantry space which is all the points in a scan if set to 0 to numEntries0
  // Analysis done in 2 main loops:
  // Loop 1: Going over every point and resetting variables
  //         Loop 2: Getting data from the TTree at that point and analyzing it for every digitized waveform sample
  // Back to Loop 1: Analyzing the point data after an accumulation of sample data for that point

  for(int i = startPoint; i < endPoint; i++){ 

    // Reset these variables for every loop
    monPulseCount = 0;
    pmtPulseCount = 0;
    //  fallingTimeHist->Reset();
    timeFitHist->Reset();
    
    // Get the data for the current point from the TTree
    T->GetEntry(i);

    // Limit x-y positions to inside the gantry space
    if( !(whichGantry==0 && xposition<=0.001 && (yposition<=0.001 || yposition>gantryYlimit)) && !(whichGantry==1 && xposition>gantryXlimit && (yposition<=0.001 || yposition>gantryYlimit)) ) {
      
      //debugging: output point number
      std::cout << i << std::endl;
      

      // Loop over all the waveform samples (val_points) in a gantry space point
      for(int j = 0; j < val_points; j++) { 
	 
	//Cleans the waveform histograms for the next waveform sample
	waveFormHist0->Reset();
	waveFormHist1->Reset();
	waveFormMonG0->Reset();
	waveFormMonG1->Reset();
	
	//Fills waveform histograms -- note size of 70 is tied to ROOT file's waveform array size which can be adjusted in the ROOT file conversion script
	for(int k = 0; k < 70; k++){
	  
	  waveFormHist0->Fill(k, -(arr0[j][k] + pmtOffset)); // apply offsets and fill waveform histograms
	  waveFormHist1->Fill(k, -(arr1[j][k] + refOffset)); 
	  // Fill appropriate monitor PMT waveforms based on gantry
	  if (whichGantry == 0) {    
	    waveFormMonG0->Fill(k,- (arr3[j][k] + monitorOffset));
	  }
	  if (whichGantry == 1) {
	    waveFormMonG1->Fill(k, -(arr5[j][k] + monitorOffset));
	  }
	}
	

	// Find the minimum bin for both SK PMT signal (channel 0) and reference trigger (channel 1) waveforms
	maxBin0 = waveFormHist0->GetMaximumBin();
	maxBin1 = waveFormHist1->GetMaximumBin();
	
	// Integrate every SK PMT waveform based on location of minimum bin. If there is no wave, the integration forms part of the Pedestal. Otherwise it will fill the Event portion of the spectrum.
	adcIntegralValue = 2*waveFormHist0->Integral(maxBin0 - 10, maxBin0 + 10)*voltageConversion; 
	adcSpecHistperScan[i]->Fill(adcIntegralValue);

	maximumVal = waveFormHist0->GetMaximum();
	
	// Monitor PMT charge integration divided into which monitor PMT is being used
	weightedSum = 0;
	monTime = 0;

	if (whichGantry == 0) {	    
	  maxBin3 = waveFormMonG0->GetMaximumBin();
	  monIntegralValue = 2*waveFormMonG0->Integral(maxBin3-5, maxBin3 + 5) * voltageConversion;
	  monMaximumVal = waveFormMonG0->GetMaximum();
	  
	  // Determing timing distribution of monitor PMT data
	  if (monMaximumVal > threshold && maxBin3 > 3) {
	    
	    // Timing for monitor PMT found by taking the charge weighted average of bins below threshold
	    // This is done because the wave width (4ns) is too small to allow proper Gaussian fits
	    for (int l = (maxBin3 - 3); l < (maxBin3 + 3); l++) {
	      double chargeDifference = waveFormMonG0->GetBinContent(l) - threshold;
	      if (chargeDifference > 0) {
		monTime += 2 * l * chargeDifference; // 2ns per bin
		weightedSum += fabs(chargeDifference);
	      }
	    }
	    // Must offfset time by hardcoded offset (see analyzer_convert_ptf_scan_to_rootTree.cxx) and subtract off reference trigger time to get relative time
	    montimeHistperScan[i]->Fill(monTime / weightedSum + 2*60 - 2*maxBin1);
	  }
	}	
	
	if (whichGantry == 1) {
	  
	  maxBin5 = waveFormMonG1->GetMaximumBin();
	  monIntegralValue = 2*waveFormMonG1->Integral(maxBin5 - 5, maxBin5 + 5) * voltageConversion;
	  monMaximumVal = waveFormMonG1->GetMaximum();
	  
	  // Determine timing distribution of monitor PMT data
	  if (monMaximumVal > threshold && maxBin5 > 3) {
	    for (int l = (maxBin5 - 3); l < (maxBin5 + 3); l++) {
	      double chargeDifference = waveFormMonG1->GetBinContent(l) - threshold;
	      if (chargeDifference > 0) {
		monTime += 2 * l * chargeDifference; // 2ns per bin
		weightedSum += fabs(chargeDifference);
	      }
	    }
	    montimeHistperScan[i]->Fill(monTime / weightedSum + 2*60 - 2*maxBin1); 
	  }
	}
	
	// Count the number of proper monitor PMT pulses
	if (monMaximumVal > threshold) {
	  monPulseCount++;
	}
	// Fill monitor charge integral value
	monSpecHistperScan[i]->Fill(monIntegralValue);
	
	// Now to find SK PMT timing
	// Threshold-filters SK PMT waves and checks to ensure minBin is not first bin of histogram. Also, checks that PMT wave height is within digitizer voltage range
	if( maximumVal > threshold && maxBin0 != 0 && maxBin1!=0) {
	
	  TF1 * waveFit = new TF1("waveFit", "gaus", 0, 70); 
	  waveFormHist0->Fit(waveFit, "Q", "", 0, 70);
	  pmtFitMean = waveFit->GetParameter(1);
	  waveFormHist1->Fit(waveFit, "Q", "", 0, 40);
	  refFitMean = waveFit->GetParameter(1);
	  // pulseSigma = pulseFitFunc->GetParamter(2);

	  // count number of 'proper' threshold-filtered 20 in. samples
	  pmtPulseCount++;
	  
	  // Fill in the SK PMT timing spectra
	  timeHistperScan[i]->Fill(2*maxBin0 + 2*130 - 2*maxBin1); 
	  timeFitHist->Fill(2*pmtFitMean + 260 - 2*refFitMean);

	  delete waveFit;

	  // double fallingEdgeThreshold = minimumVal * 0.1;
	  // int fallingBin = minBin0;
	  // while (waveFormHist0->GetBinContent(fallingBin) < fallingEdgeThreshold && fallingBin > 0) {
	  //   fallingBin--;
	  // }

	  // fallingTimeHist->Fill(2*fallingBin + 2*130 - 2*minBin1);
	}
      }
    
      /*============================================================================================
       * CHARGE SPECTRA FITTING AND ANALYSIS (WITH AND WITHOUT SMOOTHING)
       * Seems like it's best to fit smoothed event and unsmoothed pedestal (as of 22/Sep/2017)
       *=============================================================================================
       */
    
      // Charge analysis proceeds depending on user choice
      if (analysisType == 0 || analysisType == 2) {
	// Smoothing the SK PMT ADC spectra 3 bins at a time
	Int_t uBound = adcSpecHistperScan[i]->GetNbinsX()-2;
	for (Int_t indexSmoothing = 2; indexSmoothing < uBound; indexSmoothing++) {
	  sumSmooth = 0;
	  for (Int_t smoothRange = 0; smoothRange < 3; smoothRange++) {
	    sumSmooth += adcSpecHistperScan[i]->GetBinContent(indexSmoothing-2 + smoothRange);
	  }
	  smoothAdchistperScan[i]->Fill(adcSpecHistperScan[i]->GetBinCenter(indexSmoothing), sumSmooth/3.0);
	}
       	  
	// Find pedestal position and create fitting bounds
	binmax = adcSpecHistperScan[i]->GetMaximumBin();
	x_pos = adcSpecHistperScan[i]->GetXaxis()->GetBinCenter(binmax);
	
	p_lo_bound = x_pos - spread;
	p_hi_bound = x_pos + spread;
		  
	// Create fit objects for gaussians on adcSpecHist
	sprintf(pedFit_name,"gaus_%05d",i);
	sprintf(eventFit_name,"gaus_%05d",i);
	
	fPed = new TF1(pedFit_name, "gaus", adcSpecLowerBound, adcSpecUpperBound);
	fEvent = new TF1(eventFit_name, "gaus", adcSpecLowerBound, adcSpecUpperBound ); 

	// Different fitting for low and high intensity data because high intensity data has no Pedestal
	if(intensity == 0){
	  // Apply pedestal fit to the adc spectrum, event fit to the smoothed portion
	  adcSpecHistperScan[i]->Fit(fPed, "Q","", p_lo_bound, p_hi_bound);
	  smoothAdchistperScan[i]->Fit(fEvent, "Q","", x_pos+0.5, x_pos+5.5); // ranges are somewhat arbitrary and are best adjusted manually
	} else {
	  adcSpecHistperScan[i]->Fit(fEvent, "Q", "", 0, 500);
	}
	
	// Make fit to find intersection point between event and ped fit
	fInter = new TF1("fInter", "TMath::Abs([0]*TMath::Gaus(x,[1],[2],1)-[3]*TMath::Gaus(x,[4],[5],1))", x_pos, x_pos+2);
	fInter->SetParameter(0, fEvent->GetParameter(0));
	fInter->SetParameter(1, fEvent->GetParameter(1));
	fInter->SetParameter(2, fEvent->GetParameter(2));
	fInter->SetParameter(3, fPed->GetParameter(0));
	fInter->SetParameter(4, fPed->GetParameter(1));
	fInter->SetParameter(5, fPed->GetParameter(2));
	
	//Determine and extract fit parameters
	if(intensity == 0){
	  meanEvent = fEvent->GetParameter(1);
	  meanPed = fPed->GetParameter(1);
	}else{
	  meanEvent = fEvent->GetParameter(1); 
	}
	
	// determine dynamic integration bounds and integrate the event
	// for low intensity data, start from peaks of pedestal and event and step until an an appropriate bound is found
	// for high intensity data, start only from peak of event instead
	if(intensity == 0){
	  double upperIntegralBound = meanEvent;
	  double lowerIntegralBound = meanPed;
	  while (fPed->Eval(lowerIntegralBound, 0, 0, 0) >0.1) {lowerIntegralBound += 0.1;}
	  while (fEvent->Eval(upperIntegralBound, 0, 0, 0) > 0.1) {upperIntegralBound += 0.1;}
	  // NOTE: there was some ongoing discussion on whether integrating the fit or integrating the raw/smoothed bins was better. New student should consider this.
	  totalIntegralEvent = fEvent->Integral(lowerIntegralBound, upperIntegralBound)*numChargeBins/fabs(adcSpecUpperBound - adcSpecLowerBound); // * number of bins / histogram width // this ratio corrects the fit integral 
	} else {
	  double upperIntegralBound = meanEvent;
	  double lowerIntegralBound = meanEvent;
	  while (fEvent->Eval(lowerIntegralBound, 0, 0, 0) >0.1) {lowerIntegralBound -= 0.1;}
	  while (fEvent->Eval(upperIntegralBound, 0, 0, 0) > 0.1) {upperIntegralBound += 0.1;}
	  totalIntegralEvent = fEvent->Integral(lowerIntegralBound, upperIntegralBound)*numChargeBins/fabs(adcSpecUpperBound - adcSpecLowerBound);
	}
	 
	  // Do the same integration for Monitor PMT spectrum
	if(intensity == 0){
	  sprintf(monPedFit_name,"gaus_%05d",i);
	  sprintf(monEventFit_name,"gaus_%05d",i);
	  monPed = new TF1(monPedFit_name, "gaus", monSpecLowerBound, monSpecUpperBound );
	  monEvent = new TF1(monEventFit_name, "gaus", monSpecLowerBound, monSpecUpperBound );
	  monSpecHistperScan[i]->Fit(monPed, "Q","", -spread, spread);
	  monSpecHistperScan[i]->Fit(monEvent, "Q", "", 0.25, 3.5);
	  
	  monMeanEvent = monEvent->GetParameter(1);
	  monMeanPed = monPed->GetParameter(1);
	  
	  double monUpper = monEvent->GetParameter(1);
	  double monLower = monPed->GetParameter(1);
	  while (monPed->Eval(monLower, 0, 0, 0) > 0.1) {monLower += 0.1;}
	  while (monEvent->Eval(monUpper, 0, 0, 0) > 0.1) { monUpper += 0.1;}
	  monitorIntegralEvent = monEvent->Integral(monLower, monUpper)*numChargeBins/fabs(monSpecUpperBound - monSpecLowerBound); 
	} else {
	  monEvent = new TF1(monEventFit_name, "gaus", monSpecLowerBound, monSpecUpperBound );
	  monSpecHistperScan[i]->Fit(monEvent, "Q", "",0, 100);
	  monMeanEvent = monEvent->GetParameter(1);
	  monMeanPed = 0;
	  double monUpper = monEvent->GetParameter(1);
	  double monLower = monEvent->GetParameter(1);
	  while (monEvent->Eval(monLower, 0, 0, 0) > 0.1) {monLower -= 0.1;}
	  while (monEvent->Eval(monUpper, 0, 0, 0) > 0.1) { monUpper += 0.1;}
	  monitorIntegralEvent = monEvent->Integral(monLower, monUpper)*numChargeBins/fabs(monSpecUpperBound - monSpecLowerBound); 
	}
      }
    
      /*==============================================
	/TIME SPECTRA FITTING AND ANALYSIS
	/==============================================*/
	
      if (analysisType == 1 || analysisType == 2) {
	  sprintf(TDCFit_name,"gaus_%05d",i);
	  fTTS = new TF1(TDCFit_name, "gaus",260.,320.); 
	  timeHistperScan[i]->Fit(fTTS, "Q","", 260., 320.);
	  peakTime = fTTS->GetParameter(1);
	  timeSpread = fTTS->GetParameter(2);
	  
	  // fallingTimeHist->Fit(fTTS, "Q", "", 240., 300.);
	  // fallingEdgeTime = fTTS->GetParameter(1);
	  
	  timeFitHist->Fit(fTTS, "Q", "", 260., 320.);
	  fitPeakTime = fTTS->GetParameter(1);
	  fitTimeSpread = fTTS->GetParameter(2);
      }
      
      /*==============================================================
	/ SPECTRA FITTING FINISHED. NOW TO FILL PLOTS AND HISTOGRAMS
	/===============================================================*/
      
	// Correct x and y to absolute coordinates if doing a normal incidence run
	if(whichPlot==PMT_ABS_COORD){
	  
	  //Code to calculate point of incidence of laser beam on PMT:
	  gantryrot *= PI/180; //convert to radians
	  gantrytilt *= PI/180;
	  
	  if(whichGantry==0){
	    //Corrections for gantry 0 optical box
	    final_offset_x = g0_offset1*cos(gantryrot)*cos(gantrytilt) - g0_offset2*sin(gantryrot) + g0_offset3*cos(gantryrot)*sin(gantrytilt);
	    final_offset_y = g0_offset1*sin(gantryrot)*cos(gantrytilt) + g0_offset2*cos(gantryrot) + g0_offset3*sin(gantryrot)*sin(gantrytilt);
	  } else {
	    //Corrections for gantry 1 optical box
	    gantryrot+=PI;
	    final_offset_x = g1_offset1*cos(gantryrot)*cos(gantrytilt) - g1_offset2*sin(gantryrot) + g1_offset3*cos(gantryrot)*sin(gantrytilt);
	    final_offset_y = g1_offset1*sin(gantryrot)*cos(gantrytilt) + g1_offset2*cos(gantryrot) + g1_offset3*sin(gantryrot)*sin(gantrytilt);
	  }
	  
	  //Calculating the point of incidence of laser beam
	  if(beamLength*cos(gantrytilt)<0){
	    beamLength *= -1; //making sure that the value of beamLength*cos(gantrytilt) is always positive
	    //to ensure that the sign of the x and y components of the beam trajectory
	    //are only dictated by the rotation angle of gantry0
	  }
	  
	  x_inc = xposition + final_offset_x + beamLength*cos(gantryrot)*cos(gantrytilt);
	  y_inc = yposition + final_offset_y + beamLength*sin(gantryrot)*cos(gantrytilt);
	  
	  x = x_inc;
	  y = y_inc;
	} else { 
	  
	  //for vertical injection plots
	  x = xposition;
	  y = yposition;
	}
	
	x = roundToThousandths(x);
	y = roundToThousandths(y);

	// Unfortunately somewhat redundant but currently necessary for doing temperature corrections in the post-analysis
	x_posArray[i] = x;
	y_posArray[i] = y;
	

	currPoints = val_points;

	if(intensity != 0){ 
	  if (monMeanEvent < 0){
	    monGain1DHist->Fill(  -monMeanEvent );
	  }
	  monDet1DHist->Fill(  monitorIntegralEvent/currPoints);
	}
    
	// Fill plots and fit result histograms
	if (val_points != 0) {
	  
	  // histogram to correct for double binning
	  binCheckHist->Fill(x,y);
	  
	  
	  // Fill in monitor PMT plots
	  if (analysisType == 0 || analysisType == 2) {
	    monHitPosHist->Fill(x, y, monPulseCount/currPoints);
	    pmtHitPosHist->Fill(x, y, pmtPulseCount/currPoints);
	    monHitEffSlice[i] = (monPulseCount/currPoints);
	    monHitEff1DHist->Fill(monPulseCount/currPoints);
	    if (monitorIntegralEvent/currPoints > 0.1) {
	      monDetPosHist->Fill(x, y, monitorIntegralEvent/currPoints);
	     
	    monGainSlice[i] = -monMeanEvent;
	  } else {
	    monDetPosHist->Fill(x, y, 0.001);
	    monGainSlice[i] = 0;
	  }
	}
	
	// Parameters for filtering out points that are not on the PMT circle
	double radiusGuess = 0.26;
	double xCenterGuess = 0.417974;
	double yCenterGuess = 0.395633;
	
	// Fill in SK PMT 2D charge plots
	if (analysisType == 0 || analysisType == 2) {
	  
	  // Parameters for filling pvRatioHist
	  xVal = fInter->GetMinimumX();
	  yVal = fEvent->Eval(xVal, 0, 0, 0);
	  yPeak = fEvent->GetMaximum(xVal-10, xVal+10, 1.E-10, 100, false);
	  
	  if (totalIntegralEvent/currPoints>0.025 
	      && fEvent->GetChisquare()/fEvent->GetNDF() < 2.5
	      && (((x-xCenterGuess)*(x-xCenterGuess))+((y-yCenterGuess)*(y-yCenterGuess))<=(radiusGuess*radiusGuess))) { 
	    
	    // Gain is filled differently at high intensity because the Pedestal does not exist
	    if (intensity == 0) {
	      gainPosHist->Fill(x, y,  meanPed - meanEvent); 
	      pmtGain1DHist->Fill(meanPed - meanEvent); 
	    } else {
	      gainPosHist->Fill(x, y, -meanEvent);
	      pmtGain1DHist->Fill(-meanEvent);
	    }
	    detPosHist->Fill(x, y, totalIntegralEvent/currPoints);
	    pvRatioHist->Fill(x, y, yPeak/yVal);
	    pmtDet1DHist->Fill(totalIntegralEvent/currPoints);
	  } else {
	    gainPosHist->Fill(x, y, 0.01);
	    detPosHist->Fill(x, y, 0.001);
	    pvRatioHist->Fill(x, y, 0.001);
	  } 
	}
	
	// Fill in SK PMT 2D time plots
	if ((analysisType == 1 || analysisType == 2) ) {
	  if (timeHistperScan[i]->Integral(timeHistperScan[i]->GetXaxis()->FindBin(260), timeHistperScan[i]->GetXaxis()->FindBin(320))/currPoints > 0.025
	      && fTTS->GetParameter(2) < 10
	      && (((x-xCenterGuess)*(x-xCenterGuess))+((y-yCenterGuess)*(y-yCenterGuess))<=(radiusGuess*radiusGuess))) {
	    ttsPosHist->Fill(x,y, timeSpread);
	    ttpPosHist->Fill(x, y,peakTime);
	    //fallingEdgeTimePosHist->Fill(x, y, fallingEdgeTime);
	    fitTimePosHist->Fill(x, y, fitPeakTime);
	    fitSpreadPosHist->Fill(x, y, fitTimeSpread);
	  } else {
	    ttsPosHist->Fill(x, y, 0.01);
	    ttpPosHist->Fill(x, y,0.01);	  
	    //  fallingEdgeTimePosHist->Fill(x, y, 0.01);
	    fitTimePosHist->Fill(x, y, 0.01);
	    fitSpreadPosHist->Fill(x, y, 0.01);
	  }
	}
	}
    }
  }
  
// Try doing det eff and gain temperature corrections post-analysis
  // det eff corrections are for low intensity only (where pedestal still exists)
  // gain corrections are for high intensity  
  double temp = 0.0;
  int binx;
  int biny;
  int badElements; 
  int pointsToAverage = 20;
  /* Both SK PMT and monitor PMT Det Eff correlate strongly with temperature in PTF room. The following algorithm normalizes the SK PMT Det Eff to the light seen by the Monitor PMT (which is always the same at any position).
     / First a constant baseline is selected for the monitor PMT. Then the monitor PMT Hit Eff is averaged by 20 points (1 point/3 sec = 1 minute average) to reduce statistical fluctuation.
     / The ratio between the constant baseline and mon PMT Hit Eff for that minute is found and becomes the weighting to normalize the SK PMT Det Eff.
  */
  
  if (intensity == 0 && corrections == 1){
    // Have to default weighting to 1 for the first minute of correction
    monWeight = 1;   
    
    // Select baseline as maximum bin
    
    double monBaseline = monHitEff1DHist->GetXaxis()->GetBinCenter(monHitEff1DHist->GetMaximumBin());
    std::cout << "monBaseline: " << monBaseline << std::endl;
    Double_t averageMonHitEff[arrayLength];
  
    std::cout << "double bins at these points: " << std::endl;

    // At every (20)th point calculate the monitor PMT Hit Eff average and weighting for that minute 
    for (int u = startPoint; u < endPoint; u++) {
      badElements = 0;
      if (u % pointsToAverage == 0 && u < numEntries0 - pointsToAverage) {
	for (int o = 0; o  < pointsToAverage; o++) {
	  if (monHitEffSlice[u+o] > 0.01 && monHitEffSlice[u+o] < 1) { // Check to make sure data is reasonable before calculating average 
	    averageMonHitEff[u + pointsToAverage/2] += monHitEffSlice[u+o];
	  } else {
	    // std::cout << "bad element at : " << u+o << std::endl; // The exact reason for why badElements occur is not entirely known - probably a bug in data collection
	    badElements++;
	  }
	}
	averageMonHitEff[u + pointsToAverage/2] /= (pointsToAverage - badElements);
	monWeight = monBaseline / averageMonHitEff[u + pointsToAverage/2]; 
      }
      binx = detPosHist->GetXaxis()->FindBin(x_posArray[u]);
      biny = detPosHist->GetYaxis()->FindBin(y_posArray[u]);
      temp = detPosHist->GetBinContent(binx, biny);

	// TEST: output to terminal double bin positions
	if (binCheckHist->GetBinContent(binx, biny) == 2) {
	  std::cout << u << std::endl;
	}
       
	if (monWeight < 0.8 || monWeight > 1.2) {// Another check for unreasonable weights
	  monWeight = 1;
	  //  std::cout <<"monWeight set to 1"<< std::endl;
	}

	// Fill corrected plots
	detPosCorrHist->Fill(x_posArray[u], y_posArray[u], temp * monWeight);
	pmtDetCorr1DHist->Fill(temp * monWeight);
      }
  } else if (intensity != 0 && corrections == 1) { // high intensity temperature corrections for Gain that act in a very similar way
      meanWeight = 1;   
      double meanBaseline =  monGain1DHist->GetXaxis()->GetBinCenter(monGain1DHist->GetMaximumBin());
      Double_t monAvgGain[arrayLength];
    
      for (int u = startPoint; u < endPoint; u++) {
	badElements = 0;
	if (u % pointsToAverage == 0 && u < numEntries0 - pointsToAverage) {
	  for (int o = 0; o  < pointsToAverage; o++) {
	    if (monGainSlice[u+o] > 40.  && monGainSlice[u+o] < 50.) { 
	      monAvgGain[u + pointsToAverage/2] += monGainSlice[u+o];
	    } else {
	      badElements++;
	      std::cout << "bad element at : " << u+o << std::endl;
	    }
	  }
	  monAvgGain[u + pointsToAverage/2] /= (pointsToAverage - badElements);
	  meanWeight = meanBaseline / monAvgGain[u + pointsToAverage/2]; 
	}
	
     	if (meanWeight < 0.8 || meanWeight > 1.2) {
	  meanWeight = 1;
	  // std::cout <<"meanWeight set to 1"<< std::endl;
	}
	
	binx = gainPosHist->GetXaxis()->FindBin(x_posArray[u]); 
	biny = gainPosHist->GetYaxis()->FindBin(y_posArray[u]);
	temp = gainPosHist->GetBinContent(binx,biny);

	pmtGainCorr1DHist->Fill(temp * meanWeight);
	gainPosCorrHist->Fill(x_posArray[u], y_posArray[u], temp * meanWeight);
      }
      
      // for plots of single point scans
      // TGraph *pmtGainCorrGraph = new TGraph(arrayLength, arr_x, pmtGainCorrSlice);
      // TGraph *monGainAvgGraph = new TGraph (arrayLength, arr_x, monAvgGain);
      // TGraph *monGainGraph = new TGraph(arrayLength, arr_x, monGainSlice);
      // TGraph *pmtGainGraph = new TGraph (arrayLength, arr_x, pmtGainSlice);
      // pmtGainCorrGraph->Write();
      // monGainAvgGraph->Write();
      // monGainGraph->Write();
      // pmtGainGraph->Write();
   
      
  }

   
    
    /*==============================================================
     * DISPLAY PLOTS
     *=========================================================
     */
    
   
	    // Use binCheckHist to correct double binning before writing
  gainPosHist->Divide(binCheckHist);
  gainPosCorrHist->Divide(binCheckHist);
  detPosHist->Divide(binCheckHist);
  detPosCorrHist->Divide(binCheckHist);
  ttsPosHist->Divide(binCheckHist);
  ttpPosHist->Divide(binCheckHist);
  monDetPosHist->Divide(binCheckHist);
  monHitPosHist->Divide(binCheckHist);
  pmtHitPosHist->Divide(binCheckHist);
  
  Plots->Write();
  Plots->Close();
  
}

int main(){
  digiAna_v4_testGaussianFits();
  return 0;
}
