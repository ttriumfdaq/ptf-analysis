/**
 * Initial Test of Waveform Digitizer Analysis
 *
 *
 **/

#include "TAttFill.h"
#include "THStack.h"
#include "TTree.h"
#include "TBranch.h"
#include "time.h"
#include "stdio.h"
#include "unistd.h"
#include <iostream>
#include <sstream>
#include "TGraph.h"
#include "TH1F.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TH2D.h"
#include "TColor.h"
#include "TStyle.h"
#include "TString.h"
#include "TMath.h"
#include "TH1D.h"

Double_t roundToThousandths(Double_t x){

    x*=1000;
	
    return floor(x+0.5)/1000;

}

Double_t pulseFitFunc(Double_t * x, Double_t * par){

    Double_t g1 = 0.0;

    Double_t arg1 = (par[2] != 0.0) ? (x[0] - par[1])/(par[2]) : 0.0;

    g1 = par[0]*exp(-0.5*arg1*arg1);

    return g1;

}

Double_t refPulseFitFunc(Double_t * x, Double_t * par){

    Double_t g1 = 0.0;

    Double_t arg1 = (par[2] != 0.0) ? (x[0] - par[1])/(par[2]) : 0.0;

    g1 = par[3] + par[0]*exp(-0.5*arg1*arg1);

    return g1;

}

void digiAna_v2(){
    
    //Initialize and define global constants *some of these are temporary
    const double PI = 3.141592653589793238463;
    const int GANTRY_POS = 0;
    const int PMT_ABS_COORD = 1;
    const double voltageConversion = 2000.0/(50*16384);
    
    Int_t whichGantry = 1;
    Int_t whichPlot = 1;
    
    TString gainPlotTitle = "Gain vs. PMT Position";
    TString detPlotTitle = "Detection Efficiency vs. PMT Position";
    
    //Gets the run to be analyzed as input from the user
    TString runNumber;
    
    std::cout<<"\nEnter run number for scan using Gantry 1:"<<std::endl;
    std::cin>>runNumber;

    /*============================================================================================
    / Long Section of Histogram Creation, Titles and Axes setup
    / ==========================================================================================*/

    //Canvases for test data fitting
    
    //Set standard width and height of canvases for data display
    Double_t w = 800;
    Double_t h = 600;
    
    
    TCanvas *c1 = new TCanvas("c1","ADC Spectrum", w, h);
    TCanvas *c2 = new TCanvas("c2","ADC Spectrum from Fitted Charge", w, h);
    TCanvas *c3 = new TCanvas("c3","Time Spectrum", w, h);
    TCanvas *c31 = new TCanvas("c31","Time Spectrum from Fit", w, h);
    TCanvas *c4 = new TCanvas("c4","Waveform Pre-Processing", w, h);
    TCanvas *c5 = new TCanvas("c5","Waveform Post-Processing", w, h);
    c1->Divide(1,1);
    c2->Divide(1,1);
    c3->Divide(1,1);
    c31->Divide(1,1);
    c4->Divide(1,1);
    c5->Divide(1,1);

    //Canvases for PMT positional data
    TCanvas *c6 = new TCanvas("c6","Results of Gaussian Fit to Pedestal", w, h);
    TCanvas *c7 = new TCanvas("c7","Plot of Positional Data from PMT Run",800,800);
    TCanvas *c8 = new TCanvas("c8","Plot of Positional Data from PMT Run",800,800);
    TCanvas *c9 = new TCanvas("c9","Plot of Positional Data from PMT Run",800,800);    
    c6->Divide(2,3);
    c7->Divide(1,1);
    c8->Divide(1,1);
    c9->Divide(1,1);

    //Create Histograms for 1d variables
    
    //Histograms for channel 0 and channel 1 waveforms (digitizer data histograms)
    TH1D* waveFormHist0 = new TH1D("WaveformHistogram0", "Histogram of Raw WaveForm 0", 70, 0, 70); 
    TH1D* waveFormHist1 = new TH1D("WaveformHistogram1","Histogram of Raw Waveform 1", 50, 6, 56);
    
    //Plot showing charge distribution
    TH1D* adcSpecHist = new TH1D("Plot of Actual ADC values", "ADC", 39, -9.5, 3.5);
    TH1D* waveFitHist = new TH1D("Plot of Fitted ADC values", "wfHist", 39, -9.5, 3.5);

    //Histogram for timing correction between trigger pulse and actual PMT Pulse time *to show correction for the latching uncertainty*
    TH2F* doubleHist = new TH2F("Plot of integral vs rise time", "minbins", 55, -6, 3, 55, 240, 350);

    //Histogram for timing data for a single point
    TH1D* timeHist = new TH1D("Plot of Transit Time", "TDCHist", 30, 260, 320);
    TH1D* timeFitHist = new TH1D("Plot of Fitted Transit Time","TDCFit", 30, 260, 320);

    //Histograms don't do anything yet
    //TH1F* processedWave = new TH1F("ProcessedWaveForm", "procWave", 1000, 0, 1000);
    //TH1F *pulseFitHist = new TH1F("Plot of Pulse Integral", "PulseSpec", 110, 100, 210);
    
    /* -----------------Create PMT Positional Histograms-----------------------------
    / gainPositionHist: Gain as a Function of position
    / detPositionHist: Detection Efficiency as a function of position
    / binCheckHist: Makes sure no errors from double binning (precautionary)
    / pvRatioHist: Peak to Valley Ratio as a Function of position
    / -----------------------------------------------------------------------------*/
    
    //NEED TO EDIT THESE PARAMETERS TO CHANGE RESOLUTION!                                                    

    //2cm Resolution                                                                                         
    TH2D *gainPositionHist = new TH2D("gainHist", gainPlotTitle, 37, 0.0, 0.74, 37, 0.0, 0.74);    
    TH2D *detPositionHist = new TH2D("detHist", detPlotTitle, 37, 0.0, 0.74, 37, 0.0, 0.74);      
    TH2D *binCheckHist = new TH2D("binningCheck","PositionBinningHisto" ,37, 0.0, 0.74, 37, 0.0, 0.74);                               
    TH2D *pvRatioHist = new TH2D("pvRatioHist","pvRatioHist", 37, 0.0, 0.74, 37, 0.0, 0.74);   

    //1cm Resolution                                                                                         
    //TH2D *gainPositionHist = new TH2D("gainHist", gainPlotTitle, 75, -0.005, 0.745, 75, -0.005, 0.745);    
    //TH2D *detPositionHist = new TH2D("detHist", detPlotTitle, 75, -0.005, 0.745, 75, -0.005, 0.745);       
    //TH2D *binCheckHist = new TH2D("binningCheck","PositionBinningHisto" ,75, -0.005, 0.745, 75, -0.005, 0.745);                               
    //TH2D *pvRatioHist = new TH2D("pvRatioHist","pvRatioHist",75,-0.005,0.745,75,-0.005,0.745);                                              
    
    //5mm Resolution
    //TH2D *gainPositionHist = new TH2D("gainHist", "GainHisto",149, -0.0025, 0.7475,149, -0.0025, 0.7475);
    //TH2D *detPositionHist = new TH2D("detHist", "detEffHisto", 149, -0.0025, 0.7475, 149, -0.0025, 0.7475);
    //TH2D *binCheckHist = new TH2D("binningCheck","PositionBinningHisto", 149, -0.0025, 0.7475, 149, -0.0025, 0.7475);
    //TH2D *pvRatioHist = new TH2D("pvRatioHist","pvRatioHist",149,-0.0025,0.7475,149,-0.0025,0.7475);

    /*============================================================================================
    / Long Section of Colormap definitions (Beautification of Plots)
    / ==========================================================================================*/

    //Segev BenZvi's Colour palette (http://ultrahigh.org/2007/08/20/making-pretty-root-color-palettes/)                                          
    const int NRGBs = 5;
    const int NCont = 255;
    
    double stops[NRGBs] = { 0.00, 0.34, 0.61, 0.84, 1.00 };
    double red[NRGBs]   = { 0.00, 0.00, 0.87, 1.00, 0.51 };
    double green[NRGBs] = { 0.00, 0.81, 1.00, 0.20, 0.00 };
    double blue[NRGBs]  = { 0.51, 1.00, 0.12, 0.00, 0.00 };
    TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
    gStyle->SetNumberContours(NCont);
    
    // Pad & Canvas Options
    gStyle->SetPadColor(10);
    gStyle->SetPadTickX(1);
    gStyle->SetPadTickY(1);
    gStyle->SetCanvasColor(10);
    gStyle->SetOptStat(0);
    gStyle->SetPadGridX(kTRUE);
    gStyle->SetPadGridY(kTRUE);
    
    // Frame Options
    gStyle->SetFrameLineWidth(1);
    gStyle->SetFrameFillColor(10);
    
    // Marker Options
    gStyle->SetMarkerStyle(20);
    gStyle->SetMarkerSize(0.8);
    
    gStyle->SetTitleSize(0.05,"x");//X-axis title size
    gStyle->SetTitleSize(0.05,"y");
    gStyle->SetTitleSize(0.05,"z");
    gStyle->SetLabelSize(0.05,"x");//X-axis title size
    gStyle->SetLabelSize(0.05,"y");
    gStyle->SetLabelSize(0.05,"z");

    /*============================================================================================
    / Long Section of Axes Naming and Plot Tinkering (most formatting is in here)
    / ==========================================================================================*/

    waveFitHist->SetTitle("Charge Difference (Fitted vs. Unfitted)");

    waveFitHist->GetXaxis()->SetTitle("Charge Difference (pC)");
    waveFitHist->GetXaxis()->CenterTitle();
    waveFitHist->GetXaxis()->SetTitleSize(0.04);
    waveFitHist->GetXaxis()->SetLabelSize(0.03);
    
    waveFitHist->GetYaxis()->SetTitle("Count");
    waveFitHist->GetYaxis()->SetTitleSize(0.04);
    waveFitHist->GetYaxis()->SetTitleOffset(1.2);
    waveFitHist->GetYaxis()->SetLabelSize(0.03);
    waveFitHist->GetYaxis()->CenterTitle();
    waveFitHist->GetZaxis()->SetRangeUser(0,300);

    timeFitHist->SetTitle("Position of Minimum Bin");
    
    timeFitHist->GetXaxis()->SetTitle("Bin Number (Time ns)");
    timeFitHist->GetXaxis()->CenterTitle();
    timeFitHist->GetXaxis()->SetTitleSize(0.04);
    timeFitHist->GetXaxis()->SetLabelSize(0.03);
    
    timeFitHist->GetYaxis()->SetTitle("Count");
    timeFitHist->GetYaxis()->SetTitleSize(0.04);
    timeFitHist->GetYaxis()->SetTitleOffset(1.2);
    timeFitHist->GetYaxis()->SetLabelSize(0.03);
    timeFitHist->GetYaxis()->CenterTitle();

    doubleHist->SetTitle("Charge vs. PMT Pulse Time");
    
    doubleHist->GetXaxis()->SetTitle("PMT Pulse Integrated Charge");
    doubleHist->GetXaxis()->CenterTitle();
    doubleHist->GetXaxis()->SetTitleSize(0.04);
    doubleHist->GetXaxis()->SetLabelSize(0.03);
    
    doubleHist->GetYaxis()->SetTitle("PMT Transit Time (ns)");
    doubleHist->GetYaxis()->SetTitleSize(0.04);
    doubleHist->GetYaxis()->SetTitleOffset(1.2);
    doubleHist->GetYaxis()->SetLabelSize(0.03);
    doubleHist->GetYaxis()->CenterTitle();

    adcSpecHist->SetTitle("Charge Distribution");
    
    adcSpecHist->GetXaxis()->SetTitle("Total Integrated Charge (pC)");
    adcSpecHist->GetXaxis()->CenterTitle();
    adcSpecHist->GetXaxis()->SetTitleSize(0.04);
    adcSpecHist->GetXaxis()->SetLabelSize(0.03);
    
    adcSpecHist->GetYaxis()->SetTitle("Count");
    adcSpecHist->GetYaxis()->SetTitleSize(0.04);
    adcSpecHist->GetYaxis()->SetTitleOffset(1.2);
    adcSpecHist->GetYaxis()->SetLabelSize(0.03);
    adcSpecHist->GetYaxis()->CenterTitle();
    adcSpecHist->GetZaxis()->SetRangeUser(0,300);
    
    timeHist->SetTitle("Transit Time Distribution");
    
    timeHist->GetXaxis()->SetTitle("Transit Time (ns)");
    timeHist->GetXaxis()->CenterTitle();
    timeHist->GetXaxis()->SetTitleSize(0.04);
    timeHist->GetXaxis()->SetLabelSize(0.03);
    
    timeHist->GetYaxis()->SetTitle("Count");
    timeHist->GetYaxis()->SetTitleSize(0.04);
    timeHist->GetYaxis()->SetTitleOffset(1.2);
    timeHist->GetYaxis()->SetLabelSize(0.03);
    timeHist->GetYaxis()->CenterTitle();

    //Make Graph to check if signal dependant on position
    //pvRatioHist->SetTitle(pvPlotTitle);
    
    pvRatioHist->GetXaxis()->SetTitle("X Position (m)");
    pvRatioHist->GetXaxis()->CenterTitle();
    pvRatioHist->GetXaxis()->SetTitleSize(0.04);
    pvRatioHist->GetXaxis()->SetLabelSize(0.03);
    
    pvRatioHist->GetYaxis()->SetTitle("Y Position (m)");
    pvRatioHist->GetYaxis()->SetTitleSize(0.04);
    pvRatioHist->GetYaxis()->SetTitleOffset(1.2);
    pvRatioHist->GetYaxis()->SetLabelSize(0.03);
    pvRatioHist->GetYaxis()->CenterTitle();
    
    //Set axis limits
    pvRatioHist->GetXaxis()->SetRangeUser(0, 0.75);
    pvRatioHist->GetYaxis()->SetRangeUser(0, 0.75);
    pvRatioHist->GetZaxis()->SetRangeUser(0,10);
    pvRatioHist->GetZaxis()->SetLabelSize(0.02);
    
    //Make Graph to check if signal dependant on position
    gainPositionHist->SetTitle(gainPlotTitle);
    
    gainPositionHist->GetXaxis()->SetTitle("X Position (m)");
    gainPositionHist->GetXaxis()->CenterTitle();
    gainPositionHist->GetXaxis()->SetTitleSize(0.04);
    gainPositionHist->GetXaxis()->SetLabelSize(0.03);
    
    gainPositionHist->GetYaxis()->SetTitle("Y Position (m)");
    gainPositionHist->GetYaxis()->SetTitleSize(0.04);
    gainPositionHist->GetYaxis()->SetTitleOffset(1.2);
    gainPositionHist->GetYaxis()->SetLabelSize(0.03);
    gainPositionHist->GetYaxis()->CenterTitle();
    
    //Set axis limits
    gainPositionHist->GetXaxis()->SetRangeUser(0, 0.75);
    gainPositionHist->GetYaxis()->SetRangeUser(0,0.75);
    //gainPositionHist->GetZaxis()->SetRangeUser(0,35);
    gainPositionHist->GetZaxis()->SetLabelSize(0.02);
        
    //Second graph for detection efficiency
    //detPositionHist->SetTitle(detPlotTitle);
    
    detPositionHist->GetXaxis()->SetTitle("X Position (m)");
    detPositionHist->GetXaxis()->CenterTitle();
    detPositionHist->GetXaxis()->SetTitleSize(0.04);
    detPositionHist->GetXaxis()->SetLabelSize(0.03);
    
    detPositionHist->GetYaxis()->SetTitle("Y Position (m)");
    detPositionHist->GetYaxis()->SetTitleSize(0.04);
    detPositionHist->GetYaxis()->SetTitleOffset(1.2);
    detPositionHist->GetYaxis()->SetLabelSize(0.03);
    detPositionHist->GetYaxis()->CenterTitle();
    
    //Set axis limits
    detPositionHist->GetXaxis()->SetRangeUser(0,0.75);
    detPositionHist->GetYaxis()->SetRangeUser(0,0.75);
    //detPositionHist->GetZaxis()->SetRangeUser(0,0.14);
    detPositionHist->GetZaxis()->SetLabelSize(0.02);

    /*===============================================================================================================
    / End of Formatting Section
    / Start of Actual Analysis Section
    / =============================================================================================================*/

    //local variable creation for addressing of branch values from TTree
    
    /*===============================================================================================================
    ** Arrays for waveforms: 
    ** Each row corresponds to one sample
    ** Each column is the time point in 2ns intervals
    **=============================================================================================================*/
    
    Double_t arr0[200000][70]; //Preallocated for speed -- PMT
    Double_t arr1[200000][70]; //Preallocated for speed
    
    //Variable holding the number of samples at a given position (one sample is one row in arrays above
    //Should be less than 200000
    Int_t val_points;
    
    //Variable holding number of positions
    Int_t numEntries0;
    Int_t numEntries1;
    
    //Positional Variables 
    Double_t xVal = 0;
    Double_t yVal = 0;
    Double_t xposition, yposition; //gantry0 position
    Double_t gantrytilt;
    Double_t gantryrot;
    Double_t x_inc, y_inc; //point of incidence of laser beam
    Double_t x, y; //point plotted on gain and detection efficiency plots
    Double_t final_offset_x, final_offset_y; //Corrections for optical box
    Double_t beamLength = 0.05; // (26Apr2017): changed from 0.1m to 0.05m
    
    //Hard limits of gantry space in x and y
    Double_t gantryXlimit = 0.749;
    Double_t gantryYlimit = 0.696;
    
    //offset in laser aperture with respect to gantry0 position
    Double_t g0_offset1 = 0.115;
    Double_t g0_offset2 = 0.042;
    Double_t g0_offset3 = 0.0365;

    //offset in laser aperture with respect to gantry0 position
    Double_t g1_offset1 = 0.115;
    Double_t g1_offset2 = 0.052;
    Double_t g1_offset3 = 0.0395;
    
    //Set up File I/O
    
    TFile* f = new TFile("~/online/rootfiles/out_run0" + runNumber + ".root");
    
    //Creates pointer to TTree contained in the input file and pointers to its relevant branches
    TTree* T = (TTree*)f->Get("scan_tree");
    TBranch *branch0 = T->GetBranch("V1730_wave0"); //branch containing 2d array of samples for pmt signal
    TBranch *branch1 = T->GetBranch("V1730_wave1"); //branch containing 2d array of samples for reference pulse
    TBranch *points = T->GetBranch("num_points"); //gets the number of points in the scan
    
    TBranch *gantry_x = T->GetBranch("gantry1_x");
    TBranch *gantry_y = T->GetBranch("gantry1_y");
    TBranch *gantry_tilt = T->GetBranch("gantry1_tilt");
    TBranch *gantry_rot = T->GetBranch("gantry1_rot");
    
    //defines total number of entries for indexing bound
    numEntries0 = branch0->GetEntries();
    numEntries1 = branch1->GetEntries();
    
    //std::cout << numEntries0 << std::endl;
    
    //Sets where the value read in by GetEntry() is stored
    gantry_x->SetAddress(&xposition);
    gantry_y->SetAddress(&yposition);
    gantry_tilt->SetAddress(&gantrytilt);
    gantry_rot->SetAddress(&gantryrot);
    points->SetAddress(&val_points);
    branch0->SetAddress(arr0);
    branch1->SetAddress(arr1);
    branch0->SetAutoDelete(kFALSE);
    branch1->SetAutoDelete(kFALSE);
    
    // FITTING PARAPHERNALIA THAT DOES MATTER NOW
    Double_t integralValue;
    Int_t integralRange = 40;
    
    Int_t min_range = 6;
    Int_t max_range = 56;
    Int_t nParPulse = 3;
    Int_t nParRef = 4;
    
    TF1 * pulseFitFunc = new TF1("pulseFitFunc", pulseFitFunc, min_range, max_range, nParPulse);
    TF1 * refPulseFitFunc = new TF1("refPulseFitFunc", refPulseFitFunc, min_range, max_range, nParRef);
    
    refPulseFitFunc->SetParName(0, "height");
    refPulseFitFunc->SetParName(1, "mu");
    refPulseFitFunc->SetParName(2, "sigma");
    refPulseFitFunc->SetParName(3, "offset");

    pulseFitFunc->SetParName(0, "height");
    pulseFitFunc->SetParName(1, "mu");
    pulseFitFunc->SetParName(2, "sigma");
    
    refPulseFitFunc->SetParameters(-4000, 21, 6, 7500);
    pulseFitFunc->SetParameters(-53.0, 21, 3.1);

    refPulseFitFunc->SetParLimits(0, -8000, -31.0);
    refPulseFitFunc->SetParLimits(1, 6, 45);
    refPulseFitFunc->SetParLimits(2, 0.01, 20);
    refPulseFitFunc->SetParLimits(3, 7100, 7700);

    pulseFitFunc->SetParLimits(0, -200.0, -31.0);
    pulseFitFunc->SetParLimits(1, 15, 70);
    pulseFitFunc->SetParLimits(2, 2.0, 6.00);
    
    Float_t integralFitValue;
    
    //END OF PARAPHERNALIA RELATED TO FITTING
    
    //c3->cd(1);
    //gPad->SetLogy();
    //adcSpecHist->Draw();
    //c4->cd(1);
    //doubleHist->Draw("colz");
    
    c3->cd(1);
    
    THStack *hs = new THStack("hs","");

    THStack *ts = new THStack("ts","");
    
    // adcSpecHist->SetFillStyle(3004);
    // adcSpecHist->SetFillColor(kBlue);
    // hs->Add(adcSpecHist);
    // waveFitHist->SetFillStyle(3005);
    // waveFitHist->SetFillColor(kRed);
    // hs->Add(waveFitHist);    
    // hs->Draw("nostack");
    
    // hs->SetTitle("Charge Spectrum");
    
    // hs->GetXaxis()->SetTitle("Total Integrated Charge (pC)");
    // hs->GetXaxis()->CenterTitle();
    // hs->GetXaxis()->SetTitleSize(0.04);
    // hs->GetXaxis()->SetLabelSize(0.03);
    
    // hs->GetYaxis()->SetTitle("Count");
    // hs->GetYaxis()->SetTitleSize(0.04);
    // hs->GetYaxis()->SetTitleOffset(1.2);
    // hs->GetYaxis()->SetLabelSize(0.03);
    // hs->GetYaxis()->CenterTitle();

    // gPad->SetLogy();

    
    

    
    c31->cd(1);

    timeHist->SetFillStyle(3004);
    timeHist->SetFillColor(kBlue);
    ts->Add(timeHist);
    timeFitHist->SetFillStyle(3005);
    timeFitHist->SetFillColor(kRed);
    ts->Add(timeFitHist);    
    ts->Draw("nostack");
    
    ts->SetTitle("Timing Distribution");
    
    ts->GetXaxis()->SetTitle("Time (ns)");
    ts->GetXaxis()->CenterTitle();
    ts->GetXaxis()->SetTitleSize(0.04);
    ts->GetXaxis()->SetLabelSize(0.03);
    
    ts->GetYaxis()->SetTitle("Count");
    ts->GetYaxis()->SetTitleSize(0.04);
    ts->GetYaxis()->SetTitleOffset(1.2);
    ts->GetYaxis()->SetLabelSize(0.03);
    ts->GetYaxis()->CenterTitle();
    

    Int_t minBin0;
    Int_t minBin1;
    Double_t minimumVal;
    
    //Loops over all the points in the gantry space

    //c3->cd(1);
    //waveFormHist0->Draw();
    
    for(int i = 0; i < numEntries0; i++){
	
	//writes the data fields contained at the entry 'i' to the variables defined above
	T->GetEntry(i);

	//limits xy space of the gantry
	if( !(whichGantry==0 && xposition<=0.001 && (yposition<=0.001 || yposition>gantryYlimit))
	    && !(whichGantry==1 && xposition>gantryXlimit && (yposition<=0.001 || yposition>gantryYlimit)) ) {
	    
	    //debugging (read: unnecessary)
	    //std::cout << i << std::endl;
	    
	    for(int j = 0; j < val_points; j++){
		
		//Cleans the waveform histograms for the next waveform sample
		waveFormHist0->Reset();
		waveFormHist1->Reset();
		
		//Fills waveforms
		for(int k = 6; k < 56; k++){
		    
		    waveFormHist0->Fill(k, arr0[j][k]-8135.4);
		    waveFormHist1->Fill(k, arr1[j][k]); 
		    
		}

		//Calculates important values from waveforms for speed once
		minBin0 = waveFormHist0->GetMinimumBin();
		minBin1 = waveFormHist1->GetMinimumBin();
		minimumVal = waveFormHist0->GetMinimum();
		integralValue = 2*waveFormHist0->Integral(minBin0 - 10, minBin0 + 10)*voltageConversion;
		adcSpecHist->Fill(integralValue);
		
		//Discriminates pulses less than ~3mV in magnitude and minimum bin of pmt pulse is not first bin of histogram
		if(minimumVal < -30 && minBin0 != 7 && minBin1!=0){
		    waveFormHist0->Fit(pulseFitFunc, "FMNOQ");
		    waveFormHist1->Fit(refPulseFitFunc, "FMNOQ");
		    //pulseFitFunc->Draw("same");
		    waveFitHist->Fill(2*pulseFitFunc->Integral(0,60)*voltageConversion);

		    //refPulseFitFunc->Draw("same");

		    //12 offset is from 6 bin (2ns / bin) offset between fitted histogram indices and fit function indices.  272 is 136 bin offset between both pulses in digitizer time
		    timeHist->Fill(2*minBin0 + 272 - 2*minBin1 - 12);
		    timeFitHist->Fill(2*pulseFitFunc->GetParameter(1) + 272 - 2*refPulseFitFunc->GetParameter(1));
		    //std::cout << minBin0 << std::endl;
		    
		    //gPad->Modified();
		    //gPad->Update();
		    //usleep(100000);
		    		    
		}
	    }
	    
	    if(whichPlot==PMT_ABS_COORD){
		
		//Code to calculate point of incidence of laser beam on PMT:
		gantryrot *= PI/180; //convert to radians
		gantrytilt *= PI/180;
		
		if(whichGantry==0){
		    //Corrections for gantry 0 optical box
		    final_offset_x = g0_offset1*cos(gantryrot)*cos(gantrytilt) - g0_offset2*sin(gantryrot) + g0_offset3*cos(gantryrot)*sin(gantrytilt);
		    final_offset_y = g0_offset1*sin(gantryrot)*cos(gantrytilt) + g0_offset2*cos(gantryrot) + g0_offset3*sin(gantryrot)*sin(gantrytilt);
		}
		else{
		    //Corrections for gantry 1 optical box
		    gantryrot+=PI;
		    final_offset_x = g1_offset1*cos(gantryrot)*cos(gantrytilt) - g1_offset2*sin(gantryrot) + g1_offset3*cos(gantryrot)*sin(gantrytilt);
		    final_offset_y = g1_offset1*sin(gantryrot)*cos(gantrytilt) + g1_offset2*cos(gantryrot) + g1_offset3*sin(gantryrot)*sin(gantrytilt);
		}
		
		//Calculating the point of incidence of laser beam
		if(beamLength*cos(gantrytilt)<0){
		    beamLength *= -1; //making sure that the value of beamLength*cos(gantrytilt) is always positive
		    //to ensure that the sign of the x and y components of the beam trajectory
		    //are only dictated by the rotation angle of gantry0
		}
		
		x_inc = xposition + final_offset_x + beamLength*cos(gantryrot)*cos(gantrytilt);
		y_inc = yposition + final_offset_y + beamLength*sin(gantryrot)*cos(gantrytilt);
		
		x = x_inc;
		y = y_inc;
	    }
	    else{ //Plots of gantry position
		x = xposition;
		y = yposition;
	    }
	    
	    if(val_points != 0){
		//gainPositionHist->Fill(roundToThousandths(x), roundToThousandths(y), );
		//std::cout << adcSpecHist->GetMaximumBin() << std::endl;		
		//std::cout << adcSpecHist->Integral(0, adcSpecHist->GetMaximumBin()-2) << std::endl;
		//std::cout << val_points << std::endl;
		
		detPositionHist->Fill(roundToThousandths(x)+0.002, roundToThousandths(y)+0.002, ((Double_t) adcSpecHist->Integral(2, adcSpecHist->GetMaximumBin()-3))/val_points);
	    }
	    	    
	}

	
	
	gPad->Modified();
	gPad->Update();
	usleep(100000);
	
	adcSpecHist->Reset();
	waveFitHist->Reset();
	timeHist->Reset();
	timeFitHist->Reset();
	
	//std::cout << i << std::endl;

    }
    
    //c5->cd(1);
    //smoothHist->Draw();
    
    c4->cd(1);
    doubleHist->Draw("colz");

    
    
    //c2->cd(1);
    //integratedWaveFormHist->Draw();
    
    //c1->cd(1);
    //doubleHist->Draw();

    c7->cd(1);
    gainPositionHist->Draw("colz");
    
    c8->cd(1);
    detPositionHist->Draw("colz");
    //c1->Update();
    
}

int main(){
    digiAna_v2();

    return 0;
}
