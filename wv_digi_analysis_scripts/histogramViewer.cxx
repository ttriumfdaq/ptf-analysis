// A simple script that loads in analyzed histograms and re-displays them in the standard color scheme.
// Additional operations can be performed on the histogram data if they need to be manipulated
// Author: Kevin Xie
#include "TAttFill.h"
#include "THStack.h"
#include "TTree.h"
#include "TBranch.h"
#include "time.h"
#include "stdio.h"
#include "unistd.h"
#include <iostream>
#include <sstream>
#include "TGraph.h"
#include "TH1F.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TH2D.h"
#include "TColor.h"
#include "TStyle.h"
#include "TString.h"
#include "TMath.h"
#include "TH1D.h"

void histogramViewer() {

    TCanvas *c1 = new TCanvas("c1", "Parameter", 800, 800);
    c1->Divide(1,1);
    TCanvas *c2 = new TCanvas("c2", "Parameter", 800, 800);
  c2->Divide(1,1);
    TCanvas *c3 = new TCanvas("c3", "Parameter", 800, 800);  
c3->Divide(1,1);
    TCanvas *c4 = new TCanvas("c4", "Parameter", 800, 800);  
c4->Divide(1,1);
    TCanvas *c5 = new TCanvas("c5", "Parameter", 800, 800);  
c5->Divide(1,1);
    TCanvas *c6 = new TCanvas("c6", "Parameter", 800, 800);  
c6->Divide(1,1);
    TCanvas *c7 = new TCanvas("c7", "Parameter", 800, 800);
  c7->Divide(1,1);
    TCanvas *c8 = new TCanvas("c8", "Parameter", 800, 800);
  c8->Divide(1,1);
    TCanvas *c9 = new TCanvas("c9", "Parameter", 800, 800); 
 c9->Divide(1,1);

  /*============================================================================================
    / Long Section of Colormap definitions (Beautification of Plots)
    / ==========================================================================================*/

    //Segev BenZvi's Colour palette (http://ultrahigh.org/2007/08/20/making-pretty-root-color-palettes/)                                          
    const int NRGBs = 5;
    const int NCont = 255;
    
    double stops[NRGBs] = { 0.00, 0.34, 0.61, 0.84, 1.00 };
    double red[NRGBs]   = { 0.00, 0.00, 0.87, 1.00, 0.51 };
    double green[NRGBs] = { 0.00, 0.81, 1.00, 0.20, 0.00 };
    double blue[NRGBs]  = { 0.51, 1.00, 0.12, 0.00, 0.00 };
    TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
    gStyle->SetNumberContours(NCont);
    
    // Pad & Canvas Options
    gStyle->SetPadColor(10);
    gStyle->SetPadTickX(1);
    gStyle->SetPadTickY(1);
    gStyle->SetCanvasColor(10);
    gStyle->SetOptStat(0);
    gStyle->SetPadGridX(kTRUE);
    gStyle->SetPadGridY(kTRUE);
    
    // Frame Options
    gStyle->SetFrameLineWidth(1);
    gStyle->SetFrameFillColor(10);
    
    // Marker Options
    gStyle->SetMarkerStyle(20);
    gStyle->SetMarkerSize(0.8);
    
    gStyle->SetTitleSize(0.05,"x");//X-axis title size
    gStyle->SetTitleSize(0.05,"y");
    gStyle->SetTitleSize(0.05,"z");
    gStyle->SetLabelSize(0.05,"x");//X-axis title size
    gStyle->SetLabelSize(0.05,"y");
    gStyle->SetLabelSize(0.05,"z");

    // Open input file and whatever histograms/data you want to see
    TFile *ifile = new TFile("plots_run03619.root");

    // deprecated histogram names before run 3624
    TH2D * gain = (TH2D*) ifile->Get("gainPosHist");
    TH2D *det = (TH2D*) ifile->Get("detHist");
    TH2D *tts = (TH2D*) ifile->Get("ttsPositionHist");
    TH2D *ttp = (TH2D*) ifile->Get("peakPositionHist");
     TH2D *detCorr = (TH2D*) ifile->Get("detCorrectedHist");
    TH2D * binCheck = (TH2D*) ifile->Get("binningCheck");
    TH2D * monHitEff2D = (TH2D*) ifile->Get("monCountHist");
    TH2D * monDetEff2D = (TH2D*) ifile->Get("monPositionHist");
    TH2D * pmtHitEff2D = (TH2D*) ifile->Get("pmtCountHist");
    TH1D * monHitEff1D = (TH1D*) ifile->Get("monCount1DHist");

    // TH2D * gain = (TH2D*) ifile->Get("gainPosHist");
    // TH2D *det = (TH2D*) ifile->Get("detPosHist");
    // TH2D *tts = (TH2D*) ifile->Get("ttsPosHist");
    // TH2D *ttp = (TH2D*) ifile->Get("ttpPosHist");
    // TH2D *fallingTime = (TH2D*) ifile->Get("fallingEdgeTimePosHist");
    //  TH2D *detCorr = (TH2D*) ifile->Get("detPosCorrHist");
    // TH2D * binCheck = (TH2D*) ifile->Get("binningCheck");
    // TH2D * monHitEff2D = (TH2D*) ifile->Get("monHitPosHist");
    // TH2D * monDetEff2D = (TH2D*) ifile->Get("monDetPosHist");
    // TH2D * pmtHitEff2D = (TH2D*) ifile->Get("pmtHitPosHist");
    // TH1D * monHitEff1D = (TH1D*) ifile->Get("monHitEff1DHist");
  

    // An example of how the histograms can be manipulated 
    // in this case checking and averaging double-binned data points
    for (int binx = 0; binx < gain->GetNbinsX(); binx++) {
      for (int biny = 0; biny < gain->GetNbinsY(); biny++) {
	if (binCheck->GetBinContent(binx, biny) == 2) {
	  gain->SetBinContent(binx, biny, gain->GetBinContent(binx, biny)/2.0);
	  det->SetBinContent(binx, biny, det->GetBinContent(binx, biny)/2.0);
	  tts->SetBinContent(binx, biny, tts->GetBinContent(binx, biny)/2.0); 
	  ttp->SetBinContent(binx, biny, ttp->GetBinContent(binx, biny)/2.0);
	  detCorr->SetBinContent(binx, biny, detCorr->GetBinContent(binx, biny)/2.0);
	  monHitEff2D->SetBinContent(binx, biny, monHitEff2D->GetBinContent(binx, biny)/2.0);
	  monDetEff2D->SetBinContent(binx, biny, monDetEff2D->GetBinContent(binx, biny)/2.0);
	  pmtHitEff2D->SetBinContent(binx, biny, pmtHitEff2D->GetBinContent(binx, biny)/2.0);
	}
      }
    }
    


    // output the plots
    c1->cd(1);
    gain->Draw("colz");
     c2->cd(1);
    det->Draw("colz");
    c3->cd(1);
    tts->Draw("colz");
    c4->cd(1);
    ttp->Draw("colz");
    c5->cd(1);
    detCorr->Draw("colz");
    c6->cd(1);
    binCheck->Draw("colz");
    c7->cd(1);
    monHitEff2D ->Draw("colz");
    c8->cd(1);
    monDetEff2D->Draw("colz");
    c9->cd(1);
    pmtHitEff2D->Draw("colz");








}
