from normal_incidence_py import calculateGantryPosition
import matplotlib.pyplot as plt
import numpy as np
import time
import math
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator

#Setting up current axes for plot of gantry position
fig = plt.figure()
ax = fig.gca(projection = '3d')
ax.set_xlim3d(0, 0.7)
ax.set_ylim3d(0,0.7)
ax.set_zlim3d(0,0.7)

ax.set_xlabel('x')
ax.set_ylabel('y')
ax.set_title('Gantry 0 Position:  Offset in Red, Laser Beam in Blue')

#defines length of laser beam (distance between laser aperture and PMT surface)
beamLength = 0.1

x = 0.088
y = -0.01

#print(x,y)
#calculates gantry position for desired (center x, center y, radius, beam length, x offset, y offset, incidence angle, azimuth angle) respectively
values = calculateGantryPosition(0.335, 0.397, 0.31, beamLength, x, y, 0, 0) 

#print(values)

#quivers vectors with pivot at tip which represent laser beams, they have the length correctly as calculated
ax.quiver(values['x_o'], values['y_o'], values['z_o'], (-1)*values['x_i'], (-1)*values['y_i'], (-1)*values['z_i'],length = beamLength)

#quivers vector between rot/tilt axis intersection and laser aperture, and chooses whether to use gantry 0 or 1
if values['gantry'] == 0:
    ax.quiver(values['x'], values['y'], values['z'], (-1)*values['g0_ox'],(-1)*values['g0_oy'], (-1)*values['g0_oz'], length = values['g0_mag'], color = 'r', pivot = 'tail')
if values['gantry'] == 1:
    ax.quiver(values['x'], values['y'], values['z'], (-1)*values['g1_ox'],(-1)*values['g1_oy'], (-1)*values['g1_oz'], length = values['g1_mag'], color = 'k', pivot = 'tail')

print(str(values['x_o'])+' '+str(values['y_o'])+' ' +str(values['z_o'])+' '+str(values['x'])+' '+str(values['y'])+' '+str(0.875 + 0.04 - values['z'])+' '+str(math.degrees(values['rot']))+' '+str((-1)*math.degrees(values['tilt']))+'\n\n')
            
#Plots 3d sphere for reference which is same radius and position as spherical cap
u = np.linspace(0, 2 * np.pi, 100)
v = np.linspace(0, np.pi, 100)

x1 = 0.31 * np.outer(np.cos(u), np.sin(v)) + 0.335
y2 = 0.31 * np.outer(np.sin(u), np.sin(v)) + 0.397
z3 = 0.31 * np.outer(np.ones(np.size(u)), np.cos(v))
ax.plot_surface(x1, y2, z3, rstride=4, cstride=4, color='w', alpha = 0.2)

plt.show()

#The following lines of code vary position and keep the incidence and azimuth angles fixed

fig = plt.figure()
ax = fig.gca(projection = '3d')
ax.set_xlim3d(0,0.7)
ax.set_ylim3d(0,0.7)
ax.set_zlim3d(0,0.7)

ax.set_xlabel('x')
ax.set_ylabel('y')
ax.set_title('Gantry Position:  Gantry Offset in Red/Black, Laser Beam in Blue')

squareRange = np.linspace(-0.19, 0.19 , num = 15)

#Plots same sphere as earlier
ax.plot_surface(x1, y2, z3, rstride=4, cstride=4, color='w', alpha = 0.2)

for x  in squareRange:
    for y in squareRange:

        values = calculateGantryPosition(0.335, 0.397, 0.31, beamLength, x, y, 0, 0)

        ax.quiver(values['x_o'], values['y_o'], values['z_o'], (-1)*values['x_i'], (-1)*values['y_i'], (-1)*values['z_i'],length = beamLength)

        if values['gantry'] == 0:
            ax.quiver(values['x'], values['y'], values['z'], (-1)*values['g0_ox'],(-1)*values['g0_oy'], (-1)*values['g0_oz'], length = values['g0_mag'], color = 'r', pivot = 'tail')

        if values['gantry'] == 1:
            ax.quiver(values['x'], values['y'], values['z'], (-1)*values['g1_ox'],(-1)*values['g1_oy'], (-1)*values['g1_oz'], length = values['g1_mag'], color = 'k', pivot = 'tail')

    #print(str(values['x_o'])+' '+str(values['y_o'])+' ' +str(values['z_o'])+' '+str(values['x'])+' '+str(values['y'])+' '+str(0.875 - values['z'])+' '+str(math.degrees(values['rot']))+' '+str((-1)*math.degrees(values['tilt']))+'\n\n')

plt.show()



#The following lines of code do the same thing as the above lines of code, except varying incidence and azimuth angle and keeping the point on the surface fixed

#fig = plt.figure()
#ax = fig.gca(projection = '3d')
#ax.set_xlim3d(0,0.7)
#ax.set_ylim3d(0,0.7)
#ax.set_zlim3d(0,0.7)

#ax.set_xlabel('x')
#ax.set_ylabel('y')
#ax.set_title('Gantry 0 Position:  Offset in Red, Laser Beam in Blue')

#roundRange = np.linspace(-25, 25 , num = 3)

#Plots same sphere as earlier
#ax.plot_surface(x1, y2, z3, rstride=4, cstride=4, color='w', alpha = 0.2)

#for theta in roundRange:

    #values = calculateGantryPosition(0.33, 0.38, 0.31, beamLength, 0.005, -0.076, theta, 0)

    #ax.quiver(values['x_o'], values['y_o'], values['z_o'], (-1)*values['x_i'], (-1)*values['y_i'], (-1)*values['z_i'],length = beamLength)

    #if values['gantry'] == 0:
        #ax.quiver(values['x'], values['y'], values['z'], (-1)*values['g0_ox'],(-1)*values['g0_oy'], (-1)*values['g0_oz'], length = values['g0_mag'], color = 'r', pivot = 'tail')

    #if values['gantry'] == 1:
        #ax.quiver(values['x'], values['y'], values['z'], (-1)*values['g1_ox'],(-1)*values['g1_oy'], (-1)*values['g1_oz'], length = values['g1_mag'], color = 'k', pivot = 'tail')

    #print(str(values['x_o'])+' '+str(values['y_o'])+' ' +str(values['z_o'])+' '+str(values['x'])+' '+str(values['y'])+' '+str(0.875 - values['z'])+' '+str(math.degrees(values['rot']))+' '+str((-1)*math.degrees(values['tilt']))+'\n\n')

#plt.show()
         
