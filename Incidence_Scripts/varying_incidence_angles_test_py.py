from normal_incidence_py import calculateGantryPosition
#import matplotlib.pyplot as plt
#import numpy as np
import time
import math
#from mpl_toolkits.mplot3d import Axes3D
#from matplotlib import cm
#from matplotlib.ticker import LinearLocator

#defines length of laser beam (distance between laser aperture and PMT surface)
beamLength = 0.1

x = 0
y = 0

for num in range(1,8):
    #calculates gantry position for desired (center x, center y, radius, beam length, x offset, y offset, incidence angle, azimuth angle) respectively
    values = calculateGantryPosition(0.354, 0.411, 0.323, beamLength, x, y, num*10, 0) 
    print('Incidence = '+str(num*10)+': '+str(values['x'])+' '+str(values['y'])+' '+str(0.9 - values['z'])+' '+str(math.degrees(values['rot']))+' '+str((-1)*math.degrees(values['tilt']))+'\n\n')
    values = calculateGantryPosition(0.354, 0.411, 0.323, beamLength, x, y, 45, num*10)  
    print('Azimuth = '+str(num*10)+': '+str(values['x'])+' '+str(values['y'])+' '+str(0.9 - values['z'])+' '+str(math.degrees(values['rot']))+' '+str((-1)*math.degrees(values['tilt']))+'\n\n')
    values = calculateGantryPosition(0.354, 0.411, 0.323, beamLength, x, y, 45, num*(-1)*10)  
    print('Azimuth = '+str(num*10)+': '+str(values['x'])+' '+str(values['y'])+' '+str(0.9 - values['z'])+' '+str(math.degrees(values['rot']))+' '+str((-1)*math.degrees(values['tilt']))+'\n\n')


         
