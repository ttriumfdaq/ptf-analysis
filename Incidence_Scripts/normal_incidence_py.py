#Script to compute position of gantry to achieve desired incidence 
#and azimuth angle at a specified surface point

import math

def calculateGantryPosition(center_x, center_y, radius, beam, x, y, incidence, azimuth):

#calculates z position of point to look at on the PMT
    z = radius*math.cos(math.asin(math.sqrt(x*x+y*y)/radius))

#converts incidence and azimuth angles to radians
    incidence = math.radians(incidence)
    azimuth = math.radians(azimuth)
    
#creation of angles describing rotation in the frame of the PMT
    gamma = math.atan2(y,x) #angle of surface point from x - axis about z - axis
    alpha = math.asin(math.sqrt(x*x+y*y)/radius) #angle of rotation from central vertical axis to surface point
    
#defines point that will be examined at different angles
    x_origin = x + center_x
    y_origin = y + center_y
    z_origin = z
    
#conversion of spherical coordinates to cartesian
    x_inc = beam*math.sin(incidence)*math.cos(azimuth)
    y_inc = beam*math.sin(incidence)*math.sin(azimuth)
    z_inc = beam*math.cos(incidence)
    
    #print "x initial:  " + str(x_inc)+"\n\n" + "y initial:  " + str(y_inc)+ "\n\n" + "z_initial:  " + str(z_inc) + "\n\n"
    
#rotation about y-axis of PMT
    x_inc_temp = x_inc
    x_inc = math.cos(alpha)*x_inc + math.sin(alpha)*z_inc
    z_inc = (-1)*math.sin(alpha)*x_inc_temp + math.cos(alpha)*z_inc
    
#rotation about z-axis of PMT
    x_inc_temp = x_inc
    x_inc = math.cos(gamma)*x_inc - math.sin(gamma)*y_inc
    y_inc = math.sin(gamma)*x_inc_temp + math.cos(gamma)*y_inc
    
#superposition of coordinates
    x = x_origin + x_inc
    y = y_origin + y_inc
    z = z_origin + z_inc
    
#print "\n\nX Position:  " + str(x) + "\nY Position:  " + str(y) + "\nZ Position:  " + str(z)

    rot_angle = math.atan2(y_inc,x_inc)

    tilt_angle = math.atan2(z_inc, math.sqrt(x_inc*x_inc + y_inc*y_inc))

    
#-----------------------------------------------------------------------------------------------------------------------------------------------#
#Corrections for Gantry Optical Box Offsets

#work in progress, calculations above assume ideal point source
    
#Gantry Specific offsets below are estimates +/- 0.005m, need to update these if any geometry changes

    tilt_angle = tilt_angle

    if rot_angle < 0:
        rot_angle = rot_angle + math.pi
    else:
        rot_angle = rot_angle - math.pi
    
#Gantry 0 specific offsets

    offset1_g0 = 0.115
    offset2_g0 = 0.051
    offset3_g0 = (-1)*0.038
    g0_o_mag = math.sqrt(offset1_g0**2 + offset2_g0**2 + offset3_g0**2)

#Gantry 1 specific offsets
    
    offset1_g1 = 0.115
    offset2_g1 = 0.052
    offset3_g1 = (-1)*0.044
    g1_o_mag = math.sqrt(offset1_g1**2 + offset2_g1**2 + offset3_g1**2)
#NOTE:  Gantry 1 and 0 are offset by 180 degrees from each other (gantry 0 coordinate system is rotated 180 degrees CCW from gantry 1)

#Calculation of gantry 0 offsets with rotation and tilt

    g0_final_offset_x = offset1_g0*math.cos(rot_angle)*math.cos(tilt_angle) - offset2_g0*math.sin(rot_angle) + offset3_g0*math.cos(rot_angle)*math.sin(tilt_angle)

    g0_final_offset_y = offset1_g0*math.sin(rot_angle)*math.cos(tilt_angle) + offset2_g0*math.cos(rot_angle) + offset3_g0*math.sin(rot_angle)*math.sin(tilt_angle)

    g0_final_offset_z = (-1)*offset1_g0*math.sin(tilt_angle) + offset3_g0*math.cos(tilt_angle)

#Calculation of gantry 1 offsets with rotation and tilt
    
    g1_final_offset_x = offset1_g1*math.cos(rot_angle)*math.cos(tilt_angle) - offset2_g1*math.sin(rot_angle) + offset3_g1*math.cos(rot_angle)*math.sin(tilt_angle)

    g1_final_offset_y = offset1_g1*math.sin(rot_angle)*math.cos(tilt_angle) + offset2_g1*math.cos(rot_angle) + offset3_g1*math.sin(rot_angle)*math.sin(tilt_angle)

    g1_final_offset_z = (-1)*offset1_g1*math.sin(tilt_angle) + offset3_g1*math.cos(tilt_angle)
   
    if math.fabs(rot_angle) <= math.radians(110):
        x = x - g0_final_offset_x
        y = y - g0_final_offset_y
        z = z - g0_final_offset_z
        whichGantry = 0
        output_rot_angle = rot_angle
        
    else:
        
        x = x - g1_final_offset_x
        y = y - g1_final_offset_y
        z = z - g1_final_offset_z

        if rot_angle >= math.pi/2:
            output_rot_angle = rot_angle - math.pi

        else:
            output_rot_angle = math.pi - rot_angle
        whichGantry = 1 
        
   # print whichGantry + str(x) + " <- x\n" + str(y) + " <- y\n" + str(z) + " <- z\n"
        
    #print "\nRotation Angle:  " + str(math.degrees(rot_angle)) + "\nTilt Angle:  " +  str((-1)*math.degrees(tilt_angle)) + "\n\n"

    return {'x':x,'y':y,'z':z,'rot':rot_angle,'tilt':tilt_angle,'x_o':x_origin,'y_o':y_origin,'z_o':z_origin,'x_i':x_inc,'y_i':y_inc,'z_i':z_inc, 'g0_ox':(-1)*g0_final_offset_x, 'g0_oy':(-1)*g0_final_offset_y, 'g0_oz':(-1)*g0_final_offset_z,'g1_ox':(-1)*g1_final_offset_x, 'g1_oy':(-1)*g1_final_offset_y, 'g1_oz':(-1)*g1_final_offset_z, 'g1_mag': g1_o_mag, 'g0_mag':g0_o_mag, 'gantry': whichGantry}

