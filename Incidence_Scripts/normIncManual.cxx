#include <cstdlib>
#include <cmath>
#include <iostream>
#include <sstream>
#include <vector>

void CalculateNormIncidence(std::vector<double>, double, double, double, double, double, int);

int main(int argc, char *argv[])
{

    double pmtCenterX = 0.348;
    double pmtCenterY = 0.366;
    double beamLength = 0.05;
    double azimuthAngle = 0;

    double pmtPosX;
    double pmtPosY;
    double pmtPosIncidence;

    int gantry = 1;

    std::vector<double> finalGantryPosition(10);

    if(argc <= 1)
    {
	
	// On some operating systems, argv[0] can end up as an empty string instead of the program's name.
	// We'll conditionalize our response on whether argv[0] is empty or not.
	if (argv[0])
	    std::cout << "Usage: " << argv[0] << " <number>" << '\n';
	else
	    std::cout << "Usage: <program name> <number>" << '\n';
	
	exit(1);
    }

    std::stringstream pmtX(argv[1]);
    std::stringstream pmtY(argv[2]);
    std::stringstream pmtInc(argv[3]);
    
    if(!(pmtX >> pmtPosX))
	pmtPosX = pmtCenterX;
    if(!(pmtY >> pmtPosY))
	pmtPosY = pmtCenterY;
    if(!(pmtInc >> pmtPosIncidence))
	pmtPosIncidence = 0;

    finalGantryPosition[0] = pmtPosX;
    finalGantryPosition[5] = pmtPosX;

    finalGantryPosition[1] = pmtPosY;
    finalGantryPosition[6] = pmtPosY;

    CalculateNormIncidence(finalGantryPosition, pmtCenterX, pmtCenterY, beamLength, pmtPosIncidence, azimuthAngle, gantry);

    for(std::vector<double>::const_iterator i = finalGantryPosition.begin(); i!=finalGantryPosition.end(); ++i){
	std::cout<< *i <<endl;
    }

    return 0;
    
}

//----------------------------------------------------------
void CalculateNormIncidence(std::vector<double> &point, double center_x, double center_y, double beam, double incidence, double azimuth, int gantry)
{
//----------------------------------------------------------
    
/** 
 *  Parameters:
 *      point    - vector of size 10 where the gantry position will eventually be stored;
 *                 when passed to the function, the vector should contain:
 *                   point[0] = x coordinate of point on PMT (if gantry==GANTRY0)
 *                   point[1] = y coordinate of point on PMT (if gantry==GANTRY0)
 *                   point[5] = x coordinate of point on PMT (if gantry==GANTRY1)
 *                   point[6] = y coordinate of point on PMT (if gantry==GANTRY1)
 *      center_x, center_y
 *               - coordinates of PMT center
 *      beam     - desired beamlength of laser
 *      incidence- desired angle of incidence of laser at the specified point on the PMT;
 *      azimuth  - desired azimuth angle
 *      gantry   - choice of gantry to be used for the scan
 **/


/* General idea: - Gradient of spherical surface function evaluated at a point on the sphere gives its normal vector.
                 - for a circle, sphere or sum of spheres (good model for PMT with azimuthal symm): 
		   => Normal is ALWAYS line going through center (radius is perpendicular with tangential plane!!)
 */  

    const double PI  = 3.141592653589793238463;
    
    double radius = 0.323;
    double pmtHeight = 0.390;
    
    double x, y, z, gamma, alpha, x_origin, y_origin, z_origin,
	x_inc, y_inc, z_inc, x_inc_temp, y_inc_temp,
	tilt_angle, rot_angle, offset1_g0, offset2_g0,
	offset3_g0, offset1_g1, offset2_g1, offset3_g1,
	g0_o_mag, g1_o_mag, g0_final_offset_x, g0_final_offset_y,
	g0_final_offset_z, g1_final_offset_x, g1_final_offset_y, g1_final_offset_z,
	output_rot_angle;
    
    //Offsets for Gantry 0
    offset1_g0 = 0.115; //TODO: determine accuracy
    offset2_g0 = 0.042; //- 0.003*beam*10; //0.042 +- 0.002 correction factor: -0.003*beam*10 added Feb24, 2017 (4pm)-> edited to -0.003*beam*10 Mar6,2017
    offset3_g0 = -0.0365; //0.037 +/- 0.002 Mar6,2017
    g0_o_mag = sqrt(offset1_g0*offset1_g0 + offset2_g0*offset2_g0 + offset3_g0*offset3_g0);
    
    //Offsets for Gantry 1
    offset1_g1 = 0.115;
    offset2_g1 = 0.052; //- 0.002*beam*10; //correction factor: -0.002*beam*10 added Mar9,2017
    offset3_g1 = -0.0395;
    g1_o_mag = sqrt(offset1_g1*offset1_g1 + offset2_g1*offset2_g1 + offset3_g1*offset3_g1);
    
    
    
    //Converts gantry position in absolute coordinates to relative coordinates
    if(gantry==0){
	x = point[0] - center_x;
	y = point[1] - center_y;
    } else{
	x = point[5] - center_x;
	y = point[6] - center_y;
    }
    
    if(sqrt(x*x + y*y) < radius){
	//Calculates z position on PMT surface
	z = radius*cos(asin(sqrt(x*x + y*y)/radius));
	
	//Converts angular arguments to radians for use with trig functions
	incidence = PI*incidence/180;
	azimuth = PI*azimuth/180;
	
	//Calculates spherical coordinate angles
	gamma = atan2(y,x);
	alpha = asin(sqrt(x*x + y*y)/radius);
	
	//Defines point that will be examined at different angles
	x_origin = x + center_x;
	y_origin = y + center_y;
	z_origin = z;
	
	//Conversion of spherical coordinates to cartesian
	x_inc = beam*sin(incidence)*cos(azimuth);
	y_inc = beam*sin(incidence)*sin(azimuth);
	z_inc = beam*cos(incidence);
	
	//Rotation about y-axis of PMT
	x_inc_temp = x_inc;
	x_inc = cos(alpha)*x_inc + sin(alpha)*z_inc;
	z_inc = (-1)*sin(alpha)*x_inc_temp +cos(alpha)*z_inc;
	
	//Rotation about x-axis of PMT
	x_inc_temp = x_inc;
	x_inc = cos(gamma)*x_inc - sin(gamma)*y_inc;
	y_inc = sin(gamma)*x_inc_temp + cos(gamma)*y_inc;
	
	//Superposition of coordinates to achieve point source positioning
	x = x_origin + x_inc;
	y = y_origin + y_inc;
	z = z_origin + z_inc;
	
	//Calculates rotation and tilt angle that describe the beam from the gantry
	rot_angle = atan2(y_inc, x_inc); //Range for atan2 is (-PI,+PI) since it takes into account sign of arguments to determine quadrant
	tilt_angle = atan2(z_inc, sqrt(x_inc*x_inc + y_inc*y_inc)); //tilt_angle comes out positive
	
	//Converts beam rotation about point of laser incidence (x_origin, y_origin) to rotation angle of Gantry 0
	if(rot_angle < 0)
	    {
		rot_angle = rot_angle + PI;
	    }
	else
	    {
		rot_angle = rot_angle - PI;
	    }
    }
    else{ //The point (x,y) specified is not on the PMT surface, so do a vertical scan
	if(gantry==0){
	    x = point[0];
	    y = point[1];
	    z = pmtHeight + radius - offset1_g0; //Distance away from PMT cover centre of curvature - optical box height
	}
	else{
	    x = point[5];
	    y = point[6];
	    z = pmtHeight + radius - offset1_g1; //Distance away from PMT cover centre of curvature
	}
	rot_angle = 0.0;
	tilt_angle = 90.0*PI/180;
    }
    //Corrections for gantry 1 and gantry 0 optical boxes
    //Calculations for placement of point source
    g0_final_offset_x = offset1_g0*cos(rot_angle)*cos(tilt_angle) - offset2_g0*sin(rot_angle) + offset3_g0*cos(rot_angle)*sin(tilt_angle);
    g0_final_offset_y = offset1_g0*sin(rot_angle)*cos(tilt_angle) + offset2_g0*cos(rot_angle) + offset3_g0*sin(rot_angle)*sin(tilt_angle);
    g0_final_offset_z = (-1)*offset1_g0*sin(tilt_angle) + offset3_g0*cos(tilt_angle);
    
    g1_final_offset_x = offset1_g1*cos(rot_angle)*cos(tilt_angle) - offset2_g1*sin(rot_angle) + offset3_g1*cos(rot_angle)*sin(tilt_angle);
    g1_final_offset_y = offset1_g1*sin(rot_angle)*cos(tilt_angle) + offset2_g1*cos(rot_angle) + offset3_g1*sin(rot_angle)*sin(tilt_angle);
    g1_final_offset_z = (-1)*offset1_g1*sin(tilt_angle) + offset3_g1*cos(tilt_angle);
    
    if (gantry==0)
	{
	    x = x - g0_final_offset_x;
	    y = y - g0_final_offset_y;
	    z = z - g0_final_offset_z;
	    output_rot_angle = rot_angle;
	}
    else
	{
	    x = x - g1_final_offset_x;
	    y = y - g1_final_offset_y;
	    z = z - g1_final_offset_z;
	    
	    if( rot_angle > 0.0) //Updated from (rot_angle > 90*PI/180) in order to fix problem of illegal angles being produced - Mar 1, 2017
		{
		    output_rot_angle = rot_angle - PI;
		}
	    else
		{
		    output_rot_angle = rot_angle + PI;
		} 
	}
    
    //Final conversions to gantry frame
    z =  pmtHeight + radius - z; //0.712 == z position of centre of PMT cover in PTF coordinates - updated 24Apr2017
    tilt_angle = tilt_angle*(-1);
    
    //sets up return array
    
    if(gantry==0)
	{
	    
	    point[0] = x;
	    point[1] = y;
	    point[2] = z;
	    point[3] = output_rot_angle*180/PI;
	    point[4] = tilt_angle*180/PI;
	    
	    point[5] = 0.647;
	    point[6] = 0;
	    point[7] = 0;
	    point[8] = -100;
	    point[9] = 0;
	    
	}
    else
	{
	    
	    point[0] = 0;
	    point[1] = 0;
	    point[2] = 0;
	    point[3] = -100;
	    point[4] = 0;
	    
	    point[5] = x;
	    point[6] = y;
	    point[7] = z;
	    point[8] = output_rot_angle*180/PI;
	    point[9] = tilt_angle*180/PI;
	    
	}
    
    return;
    
}
