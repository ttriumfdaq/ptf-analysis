import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import os
import sys
import csv

clear = lambda: os.system('cls')
clear()

np.set_printoptions(suppress = True)
np.set_printoptions(precision = 3)

pointOneZList = []
pointOneNList = []

pointTwoZList = []
pointTwoNList = []

pointThreeZList = []
pointThreeNList = []

pointFourZList = []
pointFourNList = []

fig = plt.figure()
ax = Axes3D(fig)
ax.set_xlabel("Incidence Angle at 0 Degrees Azimuth")
ax.set_ylabel("Incidence Angle at 90 Degrees Azimuth")
ax.set_zlabel("Relative Intensity of SPE Peak")
fig.suptitle("Relative Intensity of SPE Peak as a Function of Incidence Angle")



with open('incidence_data_csv_file.csv', 'rb') as csvfile:

    datareader = csv.reader(csvfile, delimiter = ',')

    for row in datareader:

        row = [float(i) for i in row]
        
        if int(row[1]) == 1 and int(row[2]) == 0:
            pointOneZList.append([row[3],row[4],row[5],row[6], row[7]])
        elif int(row[1]) == 1 and int(row[2]) == 90:
            pointOneNList.append([row[3],row[4],row[5],row[6], row[7]])
        elif int(row[1]) == 2 and int(row[2]) == 0:
            pointTwoZList.append([row[3],row[4],row[5],row[6], row[7]])
        elif int(row[1]) == 2 and int(row[2]) == 90:
            pointTwoNList.append([row[3],row[4],row[5],row[6], row[7]])
        elif int(row[1]) == 3 and int(row[2]) == 0:
            pointThreeZList.append([row[3],row[4],row[5],row[6], row[7]])
        elif int(row[1]) == 3 and int(row[2]) == 90:
            pointThreeNList.append([row[3],row[4],row[5],row[6], row[7]])
        elif int(row[1]) == 4 and int(row[2]) == 0:
            pointFourZList.append([row[3],row[4],row[5],row[6], row[7]])
        elif int(row[1]) == 4 and int(row[2]) == 90:
            pointFourNList.append([row[3],row[4],row[5],row[6], row[7]])

csvfile.close()

#Note on numpy arrays below for each azimuth angle and point:  
#Column 0: Incidence Angle
#Column 1: Gain
#Column 2: Integral
#Column 3: Intensity
#Column 4: Detection Efficiency

pointOneZList = np.asarray(pointOneZList)
pointOneNList = np.asarray(pointOneNList)

pointTwoZList = np.asarray(pointTwoZList)
pointTwoNList = np.asarray(pointTwoNList)

pointThreeZList = np.asarray(pointThreeZList)
pointThreeNList = np.asarray(pointThreeNList)

pointFourZList = np.asarray(pointFourZList)
pointFourNList = np.asarray(pointFourNList)

pointAnalyzed = raw_input("\n\nEnter which point you would like to see data for:  ")
pointAnalyzed = int(pointAnalyzed)

if pointAnalyzed == 1:
    
    arrayOfOs = np.zeros(len(pointOneZList[:,0]))
    arrayOfNs = np.zeros(len(pointOneNList[:,0]))
    
    ax.scatter(pointOneZList[:,0], arrayOfOs, pointOneZList[:,2],s=4000,c='r')
    ax.scatter(arrayOfNs, pointOneNList[:,0], pointOneNList[:,2],s=4000,c='b')
    
elif pointAnalyzed == 2:

    arrayOfOs = np.zeros(len(pointTwoZList[:,0]))
    arrayOfNs = np.zeros(len(pointTwoNList[:,0]))
    
    ax.scatter(pointTwoZList[:,0], arrayOfOs, pointTwoZList[:,2],'b')
    ax.scatter(arrayOfNs, pointTwoNList[:,0], pointTwoNList[:,2],'r')

elif pointAnalyzed == 3:
    
    arrayOfOs = np.zeros(len(pointThreeZList[:,0]))
    arrayOfNs = np.zeros(len(pointThreeNList[:,0]))

    ax.scatter(pointThreeZList[:,0], arrayOfOs, pointThreeZList[:,2],'b')
    ax.scatter(arrayOfNs, pointThreeNList[:,0], pointThreeNList[:,2],'r')

elif pointAnalyzed == 4:

    arrayOfOs = np.zeros(len(pointFourZList[:,0]))
    arrayOfNs = np.zeros(len(pointFourNList[:,0]))
    
    ax.scatter(pointFourZList[:,0], arrayOfOs, pointFourZList[:,2],'b')
    ax.scatter(arrayOfNs, pointFourZList[:,0], pointFourNList[:,2],'r')
    
else:

    print("\n\nSorry, this is not a valid point in the dataset.  Please choose between 1 and 4.\n\n")

    
plt.show()




   

