#include "TTree.h"
#include "TBranch.h"
#include "time.h"
#include "stdio.h"
#include "unistd.h"
#include <iostream>
#include <sstream>
#include "TGraph.h"
#include "TH1F.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TH2D.h"
#include "TColor.h"
#include "TStyle.h"

void plotIncidenceDataRough(){

    //Space for file I/O for writing to csv file.  
    //Important to give append access to the file in this section.  
    //Permanent name of file: incidence_data_txt_file.txt
    FILE *outTxtFile;
    outTxtFile = fopen("/home1/midptf/online/src/analysis_scripts/incidence_data_txt_file.txt", "a+");


    Int_t stopBatch = 1;
    Int_t inputFlag = 0;

    //Local variable initialization
    Int_t runNumber; //root file number to look at, corresponds with run number in ODB (MIDAS)
    Int_t point; //point number, ID's where the data was taken on the PMT
    Int_t azimuth; //azimuth angle for identification purposes
    Int_t incidence; //incidence angle for identification purposes
    Int_t saveToFile;
    char fileName[100]; //stores root file name
    char outputString[] = "%d,%d,%d,%d,%5.3f,%5.3f,%5.3f,%5.3f\n"; //stores data to be written to line in file

    
    //local variable creation
    Int_t arr[2000000];
    Int_t val_points;
    Int_t numEntries;
    Int_t p_lo_bound;
    Int_t p_hi_bound;
    Int_t e_lo_bound;
    Int_t e_hi_bound;
    Int_t binmax;
    Int_t x_pos;
    Double_t xposition;
    Double_t yposition;
    Float_t sigmaEvent;
    Float_t sigmaPed;
    Float_t meanEvent;
    Float_t meanPed;
    Float_t constantEvent;
    Float_t constantPed;

    //Creates histograms that will be used to store data
    TH1F *smallHist = new TH1F("LittleHist","Little",120,130,250); //used for local data analysis

    TCanvas *c1 = new TCanvas("c1","Results of Gaussian Fit to Pedestal");
    c1->Divide(1,1);
  
    TBranch *branch = new TBranch();
    TBranch *points = new TBranch();


    while(stopBatch !=0){
    
	inputFlag = 0;

	//Reads in run number for accurate file opening, efficiency
	printf("Enter a run number:\n");
	scanf("%d", &runNumber);

	printf("Enter point data taken at:\n");
	scanf("%d", &point);

	printf("Enter an azimuth angle:\n");
	scanf("%d", &azimuth);

	printf("Enter an incidence angle:\n");
	scanf("%d", &incidence);

	//creates filename based on the grammar (empirical, this can be changed as our system evolves)
	sprintf(fileName, "~/online/rootfiles/out_run0%d.root", runNumber);

	//Set up File I/O
	TFile *f = new TFile(fileName);

	Double_t xVal = 0;
	Double_t yVal = 0;
    
	//10 is empirical and observed average spread, will be refined later
	Int_t spread = 2.5;

	//empirical, tbd later
	e_lo_bound = 177;
	e_hi_bound = 207;

	//Creates pointer to TTree contained in the input file and pointers to its relevant branches
	TTree *T = (TTree*)f->Get("scan_tree");
	branch = T->GetBranch("ADC0_voltage");
	points = T->GetBranch("num_points");

	//defines total number of entries for indexing bound
	numEntries = branch->GetEntries();

	//Sets where the value read in by GetEntry() is stored
	points->SetAddress(&val_points);
	branch->SetAddress(arr);
	branch->SetAutoDelete(kFALSE);

	//Make Graph to check if signal dependant on position
	smallHist->SetTitle("SPE Peak Intensity as a Function of Position on PMT");
	smallHist->GetXaxis()->SetTitle("X Position (m)");
	smallHist->GetXaxis()->CenterTitle();
	smallHist->GetYaxis()->SetTitle("Y Position (m)");
	smallHist->GetYaxis()->CenterTitle();

	//Code to draw the histograms to the canvas (Displays data to user)
	c1->cd(1);
	smallHist->Draw();

	T->GetEntry(0);

	for(Int_t k = 70000; k < 80000; k++){

	    smallHist->Fill(arr[k]);

	}

	gPad->SetLogy();

	//Insert Code to analyze smallHistogram here
	binmax = smallHist->GetMaximumBin();
	x_pos = smallHist->GetXaxis()->GetBinCenter(binmax);

	p_lo_bound = x_pos - spread;
	p_hi_bound = x_pos + spread;

	//Creates fit objects for gaussians on the histogram smallHist
	TF1 *fPed = new TF1("pedFit", "gaus", p_lo_bound, p_hi_bound);
	TF1 *fEvent = new TF1("eventFit","gaus",e_lo_bound, e_hi_bound);
		
	smallHist->Fit(fPed,"RQ0");
	smallHist->Fit(fEvent,"RQ0");

	TF1 *fInter = new TF1("interFit","abs(eventFit - pedFit)", 168, 200);

	fPed->SetRange(140,240);
	fEvent->SetRange(140,240);
		
	fPed->Draw("same");
	fEvent->Draw("+ same");
	fInter->Draw("+ same");

	meanEvent = fEvent->GetParameter(1);
	meanPed = fPed->GetParameter(1);

	sigmaEvent = fEvent->GetParameter(2);
	sigmaPed = fPed->GetParameter(2);

	constantEvent = fEvent->GetParameter(0);
	constantPed = fPed->GetParameter(0);

	printf("Would you like to save this data set to file?\n\n(1 for yes, 0 for no): ");
    
	while(inputFlag == 0){
	
	    //initial getchar to eat whitespace character
	    scanf("%d", &saveToFile);

	    if(!(saveToFile== 1 || saveToFile== 0)){

		printf("\nInvalid input, please enter either 0 or 1\n");

	    }
	    else if(saveToFile == 0){

		inputFlag = 1;

	    }
	    else{

		inputFlag = 1; 

		fprintf(outTxtFile, outputString, runNumber, point, azimuth, incidence, meanEvent - meanPed, smallHist->Integral(54,110), constantEvent, smallHist->Integral(54,110)/10000);


	    }

	}

	smallHist->Reset();

	f->Close();

	printf("Would you like to run another data set?\n 1 yes 0 no: ");
	scanf("%d", &stopBatch);

    }

    fclose(outTxtFile);

    //Code to fill masterHist with data from smallHist
    //masterHist->Fill(integratedADC);

    //sigmaPedHist->Fill(sigmaPed);

    //Code to Verify Position vs. PMT Measurement
    // if((fEvent->GetNDF()!=0) && (fPed->GetNDF()!=0)){
		
    // 	xVal = fInter->GetMinimumX();

    // 	yVal = fEvent->Eval(xVal, 0, 0, 0);
    // 	yPeak = fEvent->GetMaximum(xVal, xVal + 100, 1.E-10, 100, false);

    // }

}
