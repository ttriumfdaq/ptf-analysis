#include "iostream.h"
#include "fstream.h"
#include "TTree.h"
#include "TBranch.h"
#include "time.h"
#include "stdio.h"
#include "unistd.h"
#include "TSpectrum.h"

Double_t roundToThousandths(Double_t x){

  x*=1000;
  return floor(x+0.5)/1000;

}

void Parameter5mm(){

	//Set up File I/O
	TFile* f = new TFile("~/online/rootfiles/out_run01466.root");

	//Set up Display Canvas etc.
	Double_t w = 1600;
	Double_t h = 1000;
	//TCanvas *c1 = new TCanvas("c1","Results of Gaussian Fit to Pedestal", w, h);
	TCanvas *c2 = new TCanvas("c2","Plot of Positional Data from PMT Run",800,800);
	c2->Divide(1,1);

	//Segev BenZvi's Colour palette (http://ultrahigh.org/2007/08/20/making-pretty-root-color-palettes/)                                                                               
	const int NRGBs = 5;
	const int NCont = 255;
  
	double stops[NRGBs] = { 0.00, 0.34, 0.61, 0.84, 1.00 };
	double red[NRGBs]   = { 0.00, 0.00, 0.87, 1.00, 0.51 };
	double green[NRGBs] = { 0.00, 0.81, 1.00, 0.20, 0.00 };
	double blue[NRGBs]  = { 0.51, 1.00, 0.12, 0.00, 0.00 };
	TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
	gStyle->SetNumberContours(NCont);
  
	// Pad & Canvas Options
	gStyle->SetPadColor(10);
	gStyle->SetPadTickX(1);
	gStyle->SetPadTickY(1);
	gStyle->SetCanvasColor(10);
	//gStyle->SetPadGridX(kTRUE);
	//gStyle->SetPadGridY(kTRUE);

	// Frame Options
	gStyle->SetFrameLineWidth(1);
	gStyle->SetFrameFillColor(10);

	// Marker Options
	gStyle->SetMarkerStyle(20);
	gStyle->SetMarkerSize(0.8);

	gStyle->SetTitleSize(0.05,"x");//X-axis title size
	gStyle->SetTitleSize(0.05,"y");
	gStyle->SetTitleSize(0.05,"z");
	gStyle->SetLabelSize(0.05,"x");//X-axis title size
	gStyle->SetLabelSize(0.05,"y");
	gStyle->SetLabelSize(0.05,"z");

	//local variable creation
	Double_t parameterValue[100];
	Double_t averageBz = 0;
	Int_t val_points;
	Int_t numEntries;

	Int_t x_pos;
	Double_t xposition;
	Double_t yposition;

	//Creates pointer to TTree contained in the input file and pointers to its relevant branches
	TTree* T = (TTree*)f->Get("scan_tree");
	TBranch *branch = T->GetBranch("phidg1_Bz");
	TBranch *points = T->GetBranch("num_phidg1_points");
	TBranch *gantry1_x = T->GetBranch("gantry1_x");
	TBranch *gantry1_y = T->GetBranch("gantry1_y");

	//defines total number of entries for indexing bound
	numEntries = branch->GetEntries();

	//Sets where the value read in by GetEntry() is stored
	gantry1_x->SetAddress(&xposition);
	gantry1_y->SetAddress(&yposition);
	points->SetAddress(&val_points);
	branch->SetAddress(parameterValue);
	branch->SetAutoDelete(kFALSE);

	//Creates histograms that will be used to store data
	TH2D *gainPositionHist = new TH2D("gainHist","Gain From Pedestal as a Function of Position on PMT",125,-0.0025,0.6225,123,0.0475,0.6625);
	TH2D *binCheckHist = new TH2D("binningCheck","PositionBinningHisto",125,-0.0025,0.6225,123,0.0475,0.6625);

	//Make Graph to check if signal dependant on position
	gainPositionHist->SetTitle("Intensity over Spread of SPE as a Function of Position on PMT");
	gainPositionHist->GetXaxis()->SetTitle("X Position (m)");
	gainPositionHist->GetXaxis()->CenterTitle();
	gainPositionHist->GetYaxis()->SetTitle("Y Position (m)");
	gainPositionHist->GetYaxis()->CenterTitle();

	//Code to draw the histograms to the canvas (Displays data to user)
	c2->cd(1);
	gainPositionHist->Draw("colz");

	//looping code to iterate through the TTree Structure
	for(Long64_t i = 0; i < numEntries; i++){

		T->GetEntry(i);
		averageBz = 0;

		for(int j = 0; j < val_points; j++){

			averageBz+=parameterValue[j]/val_points;

		}

		printf("%f\n\n", averageBz);

		//Code to Verify Position vs. PMT Measurement
		gainPositionHist->Fill(roundToThousandths(xposition), roundToThousandths(yposition), averageBz);
		binCheckHist->Fill(roundToThousandths(xposition),roundToThousandths(yposition));

	      	//positionChiGraph->SetPointError(i,0.001,0.001,sigmaEvent)
		//Prepares small histogram (local) to be written to again for the next entry of data

	}

       	gainPositionHist->Divide(binCheckHist);
	//constantEventHist->Scale(1/constantEventHist->Integral("width"));	
	//constantPedHist->Scale(1/constantPedHist->Integral("width"));

	c2->Update();

}
