#include "TTree.h"
#include "TBranch.h"
#include "time.h"
#include "stdio.h"
#include "unistd.h"
#include <iostream>
#include <sstream>
#include "TGraph.h"
#include "TH1F.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TF1.h"

void rescaleaxis(TGraph *g,double scale)
{
 int N=g->GetN();
 double *y=g->GetY();
 int i=0;
 while(i<N)
 {
 y[i]=y[i]*scale;
 i=i+1;
 }
 g->GetHistogram()->Delete();
 g->SetHistogram(0);
}

void PrintIndividualSpectra(){

	//Set up File I/O
	TFile* f = new TFile("~/online/rootfiles/out_run02612.root");

	//Set up Display Canvas etc.
	Double_t w = 1600;
	Double_t h = 1000;
	TCanvas *c1 = new TCanvas("c1","Results of Gaussian Fit to ADC Spectrum", w, h);
	c1->Divide(1,1);

	//local variable creation
	Int_t arr[20000];
	Int_t val_points;
	Int_t numEntries;
	Int_t p_lo_bound;
	Int_t p_hi_bound;
	Int_t e_lo_bound;
	Int_t e_hi_bound;
	Int_t binmax;
	Int_t x_pos;
	Double_t xposition;
	Double_t yposition;
	Int_t numBins = 150;
	Double_t positionArray[150];
	Double_t gainArray[150];
	Double_t average = 0;

	Float_t meanEvent;
	Float_t meanPed;
	Float_t constantEvent;
	Float_t constantPed;
	Float_t sigmaEvent;
	Float_t sigmaPed;

	char buffer[100];
	char title[100];

	//10 is empirical and observed average spread, will be refined later
	Int_t spread = 2.5;
	//empirical, tbd later
	e_lo_bound = 177;
	e_hi_bound = 205;

	//Creates pointer to TTree contained in the input file and pointers to its relevant branches
	TTree* T = (TTree*)f->Get("scan_tree");
	TBranch *branch = T->GetBranch("ADC0_voltage");
	TBranch *points = T->GetBranch("num_points");
	TBranch *gantry1_x = T->GetBranch("gantry1_x");
	TBranch *gantry1_y = T->GetBranch("gantry1_y");



	//defines total number of entries for indexing bound
	numEntries = branch->GetEntries();

	//Sets where the value read in by GetEntry() is stored
	gantry1_x->SetAddress(&xposition);
	gantry1_y->SetAddress(&yposition);
	points->SetAddress(&val_points);
	branch->SetAddress(arr);
	branch->SetAutoDelete(kFALSE);

	//Creates histograms that will be used to store data
	TH1F *smallHist = new TH1F("ADCSpec","ADC Spectrum",numBins,150,300); //used for local data analysis
	//TH1F *pedHist = new TH1F("nothing","Pedestal Storage",numBins,150,300); //used for local data analysis
	TGraph *gainGraph = new TGraph(150);

	//Code to draw the histograms to the canvas (Displays data to user)
	

	c1->cd(1);
	smallHist->Draw();

	Int_t alpha = 0;

	TF1 *fPed = new TF1("pedFit", "gaus");

	TF1 *fEvent = new TF1("eventFit","gaus");

	//looping code to iterate through the TTree Structure
	for(Long64_t i = 0; i < numEntries-2; i++){

			T->GetEntry(i);

		if((xposition > 0.279)&&(xposition < 0.285)){

				for(Int_t k = 0; k < val_points; k++){

					smallHist->Fill(arr[k]);

				}

				//sprintf(buffer,"ADCSpec_x_%f__y_%f__.pdf", xposition, yposition);
				sprintf(title,"ADC Spectrum at x: %f, y:%f", xposition, yposition);

				//Insert Code to analyze smallHistogram here
				binmax = smallHist->GetMaximumBin();
				x_pos = smallHist->GetXaxis()->GetBinCenter(binmax);

				p_lo_bound = x_pos - spread;
				p_hi_bound = x_pos + spread;

				//Creates fit objects for gaussians on the histogram smallHist
				fPed->SetRange(p_lo_bound, p_hi_bound);

				smallHist->Fit(fPed,"RQ0 +");

				//fPed->SetRange(100,260);
				//fPed->Draw("same");

				//smallHist->Add(fPed,-1);

				fEvent->SetRange(e_lo_bound, e_hi_bound);

				smallHist->Fit(fEvent,"RQ0");
			
				
				//fEvent->SetRange(100,260);

				
				//fEvent->Draw("same");

				//smallHist->Add(fEvent,-1);

				//f2PE->

				//smallHist->SetTitle(title);
				
				constantEvent = fEvent->GetParameter(0);
				constantPed = fPed->GetParameter(0);

				meanEvent = fEvent->GetParameter(1);
				meanPed = fPed->GetParameter(1);

				sigmaEvent = fEvent->GetParameter(2);
				sigmaPed = fPed->GetParameter(2);

				//Double_t chiValue = (fEvent->GetChisquare())/(fEvent->GetNDF());
				
				//printf("DoF Event:  %f\nDoF Ped:  %f\n\n",fEvent->GetNDF(),fPed->GetNDF());
				//c1->SaveAs(buffer);

				//Code to display the master histogram to the user.  Simple at the moment
				//usleep(500000);

				//printf("%f\n", meanEvent);

				if((meanEvent - meanPed >= 0) && (meanEvent - meanPed < 40)){
					if(xposition > 0.11){
						gainGraph->SetPoint(alpha, yposition,meanEvent-meanPed);
						average+=meanEvent-meanPed;
						alpha++;
					}
				}

				//Prepares small histogram (local) to be written to again for the next entry of data
				smallHist->Reset();
				c1->Modified();
				c1->Update();
				
		}


	}

	average = average/alpha;

	/*for (int i = 0; i < alpha; i++){

		gainArray[i] = gainArray[i]/average;
		printf("%f\n", gainArray[i]);

		gainGraph->SetPoint(i,positionArray[i], gainArray[i]);
	
	}*/

	gainGraph->SetTitle("Relative Gain vs. Y");
	rescaleaxis(gainGraph, 1/average);
	gainGraph->GetXaxis()->SetTitle("Y Position (m)");
	gainGraph->GetYaxis()->SetTitle("Relative Gain");
	c1->Update();


	//constantEventHist->Scale(1/constantEventHist->Integral("width"));	
	//constantPedHist->Scale(1/constantPedHist->Integral("width"));

	//f->Close();
}
