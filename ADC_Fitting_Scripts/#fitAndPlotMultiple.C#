/**
 * All purpose FitAnalysis script for various resolution scans.
 * Compatible with scans using both gantries.
 * Generates different types of gain and relative detection efficiency plots
 * based on user input (plots with respect to gantry position as well as with
 * absolute coordinates of the point of laser injection on the PMT cover).
 *
 * Edited by: Alexander Jaffray
 * Date: June 21, 2017
 *
 * NOTE: some values for the analysis of ADC spectra are still hardcoded
 *       and must be edited manually.
 **/

#include "TTree.h"
#include "TBranch.h"
#include "time.h"
#include "stdio.h"
#include "unistd.h"
#include <iostream>
#include <sstream>
#include "TGraph.h"
#include "TH1F.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TH2D.h"
#include "TColor.h"
#include "TStyle.h"
#include <vector>


Double_t roundToThousandths(Double_t x){

	x*=1000;
	return floor(x+0.5)/1000;

}

void fitAndPlotMultiple(){
    
    //Constants and plotting parameters for nice colour palette
    const double PI = 3.141592653589793238463;
    const int GANTRY_POS = 0; //Plot of Gantry position
    const int PMT_ABS_COORD = 1; //Plot of PMT position in absolute coordinates
    
    //Segev BenZvi's Colour palette (http://ultrahigh.org/2007/08/20/making-pretty-root-color-palettes/)                                                                               
    const int NRGBs = 5;
    const int NCont = 255;
    double stops[NRGBs] = { 0.00, 0.34, 0.61, 0.84, 1.00 };
    double red[NRGBs]   = { 0.00, 0.00, 0.87, 1.00, 0.51 };
    double green[NRGBs] = { 0.00, 0.81, 1.00, 0.20, 0.00 };
    double blue[NRGBs]  = { 0.51, 1.00, 0.12, 0.00, 0.00 };
    TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
    gStyle->SetNumberContours(NCont);
    
    // Pad & Canvas Options
    gStyle->SetPadColor(10);
    gStyle->SetPadTickX(1);
    gStyle->SetPadTickY(1);
    gStyle->SetCanvasColor(10);
    gStyle->SetOptStat(0);
    gStyle->SetPadGridX(kTRUE);
    gStyle->SetPadGridY(kTRUE);
    
    // Frame Options
    gStyle->SetFrameLineWidth(1);
    gStyle->SetFrameFillColor(10);
    
    // Marker Options
    gStyle->SetMarkerStyle(20);
    gStyle->SetMarkerSize(0.8);
    
    gStyle->SetTitleSize(0.05,"x");//X-axis title size
    gStyle->SetTitleSize(0.05,"y");
    gStyle->SetTitleSize(0.05,"z");
    gStyle->SetLabelSize(0.05,"x");//X-axis title size
    gStyle->SetLabelSize(0.05,"y");
    gStyle->SetLabelSize(0.05,"z");
    
    

    //Run Data Variables
    
    TString runNumberList[] = {"2747", "2746", "2745", "2743", "2742", "2741", "2740", "2748", "2749", "2750", "2751", "2754", "2756", "2759"};
    Double_t incidenceAngleList[] = {-21, -18, -15, -9, -6, -3, 0, 3, 6, 9, 12, 15, 18, 21};
    Double_t reflectanceList[] = {0.9521, 0.9543, 0.9562, 0.9587,0.9594, 0.9599, 0.96, 0.9599, 0.9594, 0.9587, 0.9576, 0.9562, 0.9543, 0.9521};
    Double_t detEffArr[14];
    Double_t gainArr[14];
    Double_t pvRatioArr[14];
    Double_t csPedArr[14];
    Double_t csSPEArr[14];
    Int_t numberOfScans = 14;
        
    TCanvas *c1 = new TCanvas("c1","Detection Efficiency as a Function of Incidence Angle", 1200, 800);
    TH1F *smallHist = new TH1F("LittleHist","Little",121,129.5,250.5); //used for local data analysis
    TH1F *smoothHist = new TH1F("smoothHist","Smoothed Spectrum",121,129.5,250.5); //used for local data analysis
    
    c1->Divide(2,2);
    
    c1->cd(1);
    smoothHist->Draw();

    TString gainPlotTitle = "Gain from Pedestal as a Function of Position on PMT in Absolute Coordinates";
    TString detPlotTitle = "Relative Detection Efficiency as a Function of Position on PMT in Absolute Coordinates";
    TString pvPlotTitle = "Peak to Valley Ratio as a Function of Position on PMT in Absolute Coordinates";

    //LOCAL VARIABLE CREATION FOR FIT PARAMETERS AND DATA ANALYSIS
    
    Int_t arr[200000];
    Int_t val_points;
    Int_t numEntries;
    Int_t p_lo_bound;
    Int_t p_hi_bound;
    Int_t binmax;
    Int_t x_pos;
    Int_t spread = 2.5;
    
    Double_t xVal = 0;
    Double_t yVal = 0;
    Double_t sigmaEvent;
    Double_t sigmaPed;
    Double_t meanEvent;
    Double_t meanPed;
    Double_t constantEvent;
    Double_t constantPed;
    
    Double_t totalDetEffTrack;
    Double_t totalpvRatioTrack;
    Double_t totalGainTrack;
    Double_t totalChiSquareSPETrack;
    Double_t totalChiSquarePedTrack;
    Int_t totalValidCount;

    //GANTRY POSITIONAL CORRECTION PARAMETERS FOR PLOTTING AND DATA TRACKING
    
    //offset in laser aperture with respect to gantry position
    Double_t g0_offset1 = 0.115;
    Double_t g0_offset2 = 0.042;
    Double_t g0_offset3 = 0.0365;
    
    Double_t g1_offset1 = 0.115;
    Double_t g1_offset2 = 0.052;
    Double_t g1_offset3 = 0.0395;
    
    Double_t xposition, yposition; //gantry0 position
    Double_t gantrytilt;
    Double_t gantryrot;
    Double_t x_inc, y_inc; //point of incidence of laser beam
    Double_t x, y; //point plotted on gain and detection efficiency plots
    Double_t final_offset_x, final_offset_y; //Corrections for optical box
    Double_t beamLength = 0.05; // (26Apr2017): changed from 0.1m to 0.05m
    
    Double_t gantryXlimit = 0.749;
    Double_t gantryYlimit = 0.696;
    
    //Set up File I/O
    for(Int_t runIndex = 0; runIndex < numberOfScans; runIndex++){ //loops for each data point
	
	//reads in new TFile from run number in array
	TFile* f = new TFile("~/online/rootfiles/out_run0" + runNumberList[runIndex]  + ".root");
	
	//Creates pointer to TTree contained in the input file and pointers to its relevant branches
	TTree* T = (TTree*)f->Get("scan_tree");
	TBranch *branch = T->GetBranch("ADC0_voltage");
	TBranch *points = T->GetBranch("num_points");
	TBranch *gantry_x = T->GetBranch("gantry1_x");
	TBranch *gantry_y = T->GetBranch("gantry1_y");
	TBranch *gantry_tilt = T->GetBranch("gantry1_tilt");
	TBranch *gantry_rot = T->GetBranch("gantry1_rot");
	
	//defines total number of entries for indexing bound
	numEntries = branch->GetEntries();
	
	//Sets where the value read in by GetEntry() is stored
	gantry_x->SetAddress(&xposition);
	gantry_y->SetAddress(&yposition);
	gantry_tilt->SetAddress(&gantrytilt);
	gantry_rot->SetAddress(&gantryrot);
	points->SetAddress(&val_points);
	branch->SetAddress(arr);
	branch->SetAutoDelete(kFALSE);    
	
	totalDetEffTrack = 0;
	totalGainTrack = 0;
	totalpvRatioTrack = 0;
	totalChiSquareSPETrack = 0;
	totalChiSquarePedTrack = 0;
	totalValidCount = 0;

	Int_t sumSmooth;

	//looping code to iterate through the TTree Structure
	for(Long64_t i = 0; i < numEntries; i++){
	    
	    T->GetEntry(i);
	    
	    for(Int_t k = 0; k < val_points; k++){
		smallHist->Fill(arr[k]);
	    }
	    
	    Int_t ubound = smallHist->GetNbinsX()-2;
			
	    //Code to smooth histogram using moving average
	    for(Int_t indexSmoothing = 2; indexSmoothing < ubound; indexSmoothing++){
		sumSmooth = 0;
		for(Int_t smoothRange = 0; smoothRange < 5; smoothRange++){
		    sumSmooth+= smallHist->GetBinContent(indexSmoothing-2 + smoothRange);
		    
		}
		
		smoothHist->Fill(smallHist->GetBinCenter(indexSmoothing), sumSmooth/5.0);
		
	    }

	    //Insert Code to analyze smallHistogram here
	    binmax = smoothHist->GetMaximumBin();
	    x_pos = smoothHist->GetXaxis()->GetBinCenter(binmax);
	    
	    p_lo_bound = x_pos - spread;
	    p_hi_bound = x_pos + spread;
	    
	    //Creates fit objects for gaussians on the histogram smallHist
	    TF1 *fPed = new TF1("pedFit", "gaus", p_lo_bound, p_hi_bound);
	    TF1 *fEvent = new TF1("eventFit","gaus",x_pos + 11, x_pos + 32);
	    
	    smoothHist->Fit(fPed,"RQ0");
	    smoothHist->Fit(fEvent,"RQ0");
	    
	    TF1 *fInter = new TF1("interFit","abs(eventFit - pedFit)", x_pos, x_pos + 24);
	    
	    meanEvent = fEvent->GetParameter(1);
	    meanPed = fPed->GetParameter(1);
	    
	    sigmaEvent = fEvent->GetParameter(2);
	    sigmaPed = fPed->GetParameter(2);
	    
	    constantEvent = fEvent->GetParameter(0);
	    constantPed = fPed->GetParameter(0);
	    
	    Double_t totalIntegralEvent = fEvent->Integral(180,290);

	    //Code to calculate point of incidence of laser beam on PMT:
	    gantryrot *= PI/180; //convert to radians
	    gantrytilt *= PI/180;
	    
	    //Corrections for gantry 1 optical box
	    gantryrot+=PI;
	    final_offset_x = g1_offset1*cos(gantryrot)*cos(gantrytilt) - g1_offset2*sin(gantryrot) + g1_offset3*cos(gantryrot)*sin(gantrytilt);
	    final_offset_y = g1_offset1*sin(gantryrot)*cos(gantrytilt) + g1_offset2*cos(gantryrot) + g1_offset3*sin(gantryrot)*sin(gantrytilt);
	    
	    //Calculating the point of incidence of laser beam
	    if(beamLength*cos(gantrytilt)<0){
		beamLength *= -1; //making sure that the value of beamLength*cos(gantrytilt) is always positive
		//to ensure that the sign of the x and y components of the beam trajectory
		//are only dictated by the rotation angle of gantry0
	    }
	    x_inc = xposition + final_offset_x + beamLength*cos(gantryrot)*cos(gantrytilt);
	    y_inc = yposition + final_offset_y + beamLength*sin(gantryrot)*cos(gantrytilt);
	    
	    x = x_inc;
	    y = y_inc;
	    
	    //Code to Verify Position vs. PMT Measurement
	    if((fEvent->GetNDF()!=0) && (fPed->GetNDF()!=0)){
		
		xVal = fInter->GetMinimumX();
		yVal = fEvent->Eval(xVal, 0, 0, 0);
		yPeak = fEvent->GetMaximum(xVal, xVal + 100, 1.E-10, 100, false);
		
		if(meanEvent - meanPed > 8 && meanEvent - meanPed < 60 && totalIntegralEvent > 400 && totalIntegralEvent < 900 && fEvent->GetChisquare()/fEvent->GetNDF() < 1.2){
		    
		    totalGainTrack+= meanEvent - meanPed;
		    totalDetEffTrack+= totalIntegralEvent/val_points;
		    totalpvRatioTrack+= yPeak/yVal;
		    totalChiSquareSPETrack+= fEvent->GetChisquare()/fEvent->GetNDF();
		    totalChiSquarePedTrack+= fPed->GetChisquare()/fPed->GetNDF();
		    totalValidCount++;

		    std::cout<<totalIntegralEvent<<endl;
		    
		    //usleep(75000);
		    
		    //c1->Update();
		    
		}
		
	

		//Prepares small histogram (local) to be written to again for the next entry of data	    
		smallHist->Reset();
		smoothHist->Reset();
	    }
	}
	
	detEffArr[runIndex] = totalDetEffTrack/totalValidCount;
	gainArr[runIndex] = totalGainTrack/totalValidCount;
	pvRatioArr[runIndex] = totalpvRatioTrack/totalValidCount;
	csSPEArr[runIndex] = totalChiSquareSPETrack/totalValidCount;
	csPedArr[runIndex] = totalChiSquarePedTrack/totalValidCount;
	
    }

    
    TGraph* detAngleGraph = new TGraph(numberOfScans,incidenceAngleList,detEffArr);
    TGraph* gainAngleGraph = new TGraph(numberOfScans, incidenceAngleList, gainArr);
    TGraph* pvRatioAngleGraph = new TGraph(numberOfScans, incidenceAngleList, pvRatioArr);
    TGraph* csAngleGraph = new TGraph(numberOfScans, incidenceAngleList, csSPEArr);

    //Make Graph to check if signal dependant on position
    detAngleGraph->SetTitle("Detection Efficiency vs. Incidence Angle");
    
    detAngleGraph->GetXaxis()->SetTitle("Incidence Angle (Degrees)");
    detAngleGraph->GetXaxis()->CenterTitle();
    detAngleGraph->GetXaxis()->SetTitleSize(0.04);
    detAngleGraph->GetXaxis()->SetLabelSize(0.04);
    
    detAngleGraph->GetYaxis()->SetTitle("Detection Efficiency");
    detAngleGraph->GetYaxis()->SetTitleSize(0.04);
    detAngleGraph->GetYaxis()->SetTitleOffset(1.2);
    detAngleGraph->GetYaxis()->SetLabelSize(0.04);
    detAngleGraph->GetYaxis()->CenterTitle();
    
    //Set axis limits
    detAngleGraph->GetXaxis()->SetRangeUser(-24,24);
    detAngleGraph->GetYaxis()->SetRangeUser(0, 0.14);

    //Make Graph to check if signal dependant on position
    gainAngleGraph->SetTitle("Gain vs. Incidence Angle");
    
    gainAngleGraph->GetXaxis()->SetTitle("Incidence Angle (Degrees)");
    gainAngleGraph->GetXaxis()->CenterTitle();
    gainAngleGraph->GetXaxis()->SetTitleSize(0.04);
    gainAngleGraph->GetXaxis()->SetLabelSize(0.04);
    
    gainAngleGraph->GetYaxis()->SetTitle("Gain from Pedestal");
    gainAngleGraph->GetYaxis()->SetTitleSize(0.04);
    gainAngleGraph->GetYaxis()->SetTitleOffset(1.2);
    gainAngleGraph->GetYaxis()->SetLabelSize(0.04);
    gainAngleGraph->GetYaxis()->CenterTitle();
    
    //Set axis limits
    gainAngleGraph->GetXaxis()->SetRangeUser(-24,24);
    gainAngleGraph->GetYaxis()->SetRangeUser(0, 35);

    //Make Graph to check if signal dependant on position
    pvRatioAngleGraph->SetTitle("Peak to Valley Ratio vs. Incidence Angle");
    
    pvRatioAngleGraph->GetXaxis()->SetTitle("Incidence Angle (Degrees)");
    pvRatioAngleGraph->GetXaxis()->CenterTitle();
    pvRatioAngleGraph->GetXaxis()->SetTitleSize(0.04);
    pvRatioAngleGraph->GetXaxis()->SetLabelSize(0.04);
    
    pvRatioAngleGraph->GetYaxis()->SetTitle("Peak to Valley Ratio");
    pvRatioAngleGraph->GetYaxis()->SetTitleSize(0.04);
    pvRatioAngleGraph->GetYaxis()->SetTitleOffset(1.2);
    pvRatioAngleGraph->GetYaxis()->SetLabelSize(0.04);
    pvRatioAngleGraph->GetYaxis()->CenterTitle();
    
    //Set axis limits
    pvRatioAngleGraph->GetXaxis()->SetRangeUser(-24,24);
    pvRatioAngleGraph->GetYaxis()->SetRangeUser(0, 6);
    
    //Make Graph to check if signal dependant on position
    csAngleGraph->SetTitle("Chi-Squared/DoF vs. Incidence Angle");
    
    csAngleGraph->GetXaxis()->SetTitle("Incidence Angle (Degrees)");
    csAngleGraph->GetXaxis()->CenterTitle();
    csAngleGraph->GetXaxis()->SetTitleSize(0.04);
    csAngleGraph->GetXaxis()->SetLabelSize(0.04);
    
    csAngleGraph->GetYaxis()->SetTitle("Peak to Valley Ratio");
    csAngleGraph->GetYaxis()->SetTitleSize(0.04);
    csAngleGraph->GetYaxis()->SetTitleOffset(1.2);
    csAngleGraph->GetYaxis()->SetLabelSize(0.04);
    csAngleGraph->GetYaxis()->CenterTitle();
    
    //Set axis limits
    csAngleGraph->GetXaxis()->SetRangeUser(-24,24);
    csAngleGraph->GetYaxis()->SetRangeUser(0, 0.3);

    c1->cd(1);
    detAngleGraph->Draw("AP");
    
    c1->cd(2);
    gainAngleGraph->Draw("AP");

    c1->cd(3);
    pvRatioAngleGraph->Draw("AP");
    
    c1->cd(4);
    csAngleGraph->Draw("AP");

    c1->Update();
    
}    
