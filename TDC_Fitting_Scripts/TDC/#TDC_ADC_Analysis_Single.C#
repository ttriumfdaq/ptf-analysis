/**
 * All purpose FitAnalysis script for 1cm resolution scans.
 * Compatible with scans using both gantries.
 * Generates different types of gain and relative detection efficiency plots
 * based on user input (plots with respect to gantry position as well as with
 * absolute coordinates of the point of laser injection on the PMT cover).
 *
 * Edited by: Rika Sugimoto
 * Date: February 14, 2017
 *
 * NOTE: some values for the analysis of ADC spectra are still hardcoded
 *       and must be edited manually.
 **/

#include "TTree.h"
#include "TBranch.h"
#include "time.h"
#include "stdio.h"
#include "unistd.h"
#include <iostream>
#include <sstream>
#include "TGraph.h"
#include "TH1F.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TH2D.h"
#include "TColor.h"
#include "TStyle.h"

Double_t roundToThousandths(Double_t x){

	x*=1000;
	return floor(x+0.5)/1000;

}

void TDC_ADC_Analysis_Single(){

    //Set up File I/O
    TFile* f = new TFile("~/online/rootfiles/out_run02650.root");
    TFile* fTDC = new TFile("~/online/rootfiles/out_run02650.root");

    //Set up Display Canvas etc.
    Double_t w = 1600;
    Double_t h = 1000;
    TCanvas *c1 = new TCanvas("c1","Results of Gaussian Fit to ADC Spectrum", w, h);
    TCanvas *c2 = new TCanvas("c2","TDC Data", w, h);
    c1->Divide(1,1);
    c2->Divide(1,1);
    
    //local variable creation
    Int_t arr[2000000];
    Int_t val_points;
    Int_t numEntries;
    Int_t p_lo_bound;
    Int_t p_hi_bound;
    Int_t e_lo_bound;
    Int_t e_hi_bound;
    Int_t binmax;
    Int_t valleyBin;
    Int_t valleyPos;
    Int_t x_pos;
    Int_t spePeak;
    Int_t speBin;
    Double_t xposition;
    Double_t yposition;
    Int_t numBins = 150;
    Double_t positionArray[150];
    Double_t gainArray[150];
    Double_t average = 0;
    
    Float_t meanEvent;
    Float_t meanPed;
    Float_t constantEvent;
    Float_t constantPed;
    Float_t sigmaEvent;
    Float_t sigmaPed;
    
    char buffer[100];
    char title[100];
    
    //10 is empirical and observed average spread, will be refined later
    Int_t spread = 2.5;
    //empirical, tbd later
    e_lo_bound = 203;
    e_hi_bound = 227;
    
    //Creates pointer to TTree contained in the input file and pointers to its relevant branches
    TTree* T = (TTree*)f->Get("scan_tree");
    TBranch *branch = T->GetBranch("ADC0_voltage");
    TBranch *points = T->GetBranch("num_points");
    TBranch *gantry1_x = T->GetBranch("gantry1_x");
    TBranch *gantry1_y = T->GetBranch("gantry1_y");
    
    //defines total number of entries for indexing bound
    numEntries = branch->GetEntries();
    
    //Sets where the value read in by GetEntry() is stored
    gantry1_x->SetAddress(&xposition);
    gantry1_y->SetAddress(&yposition);
    points->SetAddress(&val_points);
    branch->SetAddress(arr);
    branch->SetAutoDelete(kFALSE);
    
    //Creates histograms that will be used to store data
    TH1F *smallHist = new TH1F("ADCSpec","ADC Spectrum",9000,0,900); //used for local data analysis
    TH1D *TDCHist = new TH1D("TDCSpec", "TDC Spectrum", 9000, 0, 900);
    TH1D *smoothHist = new TH1D("TDCSpecSmooth", "TDC Spectrum Smoothed", 9000, 0, 900);

    //TH1F *pedHist = new TH1F("nothing","Pedestal Storage",numBins,150,300); //used for local data analysis
    //TGraph *gainGraph = new TGraph(150);
    
    //Code to draw the histograms to the canvas (Displays data to user)
    
    c2->cd(1);
    
    TDCHist = (TH1D*)fTDC->Get("V1190Diff_0_4");
    TF1 *fTTS = new TF1("TDCFit", "gaus");
    
    //Code to smooth histogram using moving average
	for(Int_t indexSmoothing = 3; indexSmoothing < TDCHist->GetNBinsX()-2; indexSmoothing++){
	    sumSmooth = 0;
	    for(Int_t smoothRange = 0; smoothRange < 5; smoothRange++){
		sumSmooth+= TDCHist->GetBinContent(indexSmoothing-2 + smoothRange);
		
	    }
	    
	    smoothHist->Fill(sumSmooth);
	    
	}

    //TDCHist->GetXaxis()->SetRangeUser(0,900);
    
    //Insert Code to analyze TDCHistogram here
    Int_t binmax1 = smoothHist->GetMaximumBin();
    Float_t x_pos1 = smoothHist->GetXaxis()->GetBinCenter(binmax1);
    printf("%d\n", x_pos1);
    
    TTS_lo_bound = x_pos1 - spread+1.2;
    TTS_hi_bound = x_pos1 + spread-1.2;
    
    //Creates fit objects for gaussians on the histogram TDCHist
    
    fTTS->SetRange(TTS_lo_bound, TTS_hi_bound);
    smoothHist->Fit(fTTS, "RQ0");
    fTTS->SetRange(0, 1000);
    smoothHist->Draw();
    fTTS->Draw("same");
    
    int bin1 =smoothHist->FindFirstBinAbove(TDCHist->GetMaximum()/2);
    int bin2 = smoothHist->FindLastBinAbove(TDCHist->GetMaximum()/2);
    double fwhm = smoothHist->GetBinCenter(bin2) - TDCHist->GetBinCenter(bin1);
    
    printf("%f\n", fwhm);
    
    c1->cd(1);
    smallHist->Draw();
    
    Int_t alpha = 0;
    
    TF1 *fPed = new TF1("pedFit", "gaus");
    
    TF1 *fEvent = new TF1("eventFit","gaus");
    
    //looping code to iterate through the TTree Structure
    for(Long64_t i = 0; i < numEntries; i++){
	
	T->GetEntry(i);
	
	for(Int_t k = 0; k <200000; k++){
	    
	    smallHist->Fill(arr[k]);
	    
	}
	
	
	
	sprintf(buffer,"ADCSpec_x_%f__y_%f__.pdf", xposition, yposition);
	sprintf(title,"ADC Spectrum at x: %f, y:%f", xposition, yposition);
	
	//Insert Code to analyze smallHistogram here
	binmax = smallHist->GetMaximumBin();
	x_pos = smallHist->GetXaxis()->GetBinCenter(binmax);
	printf("%d\n", x_pos);

	p_lo_bound = x_pos - spread;
	p_hi_bound = x_pos + spread;
	
	//Creates fit objects for gaussians on the histogram smallHist
	fPed->SetRange(p_lo_bound, p_hi_bound);
	
	smallHist->Fit(fPed,"RQ0 +");
	
	fPed->SetRange(100,260);
	fPed->Draw("same");

	//smallHist->Add(fPed,-1);
	
	fEvent->SetRange(p_hi_bound + 12, p_hi_bound + 30);
	
	smallHist->Fit(fEvent,"RQ0");
	
	
	fEvent->SetRange(100,260);
	
	
	fEvent->Draw("same");
	
	//smallHist->Add(fEvent,-1);
		
	smallHist->SetTitle(title);
	
	constantEvent = fEvent->GetParameter(0);
	constantPed = fPed->GetParameter(0);
	
	meanEvent = fEvent->GetParameter(1);
	meanPed = fPed->GetParameter(1);
	
	sigmaEvent = fEvent->GetParameter(2);
	sigmaPed = fPed->GetParameter(2);
	
	//Double_t chiValue = (fEvent->GetChisquare())/(fEvent->GetNDF());
	
	printf("DoF Event:  %f\nDoF Ped:  %f\n\n",fEvent->GetNDF(),fPed->GetNDF());
	//c1->SaveAs(buffer);
	
	//Code to display the master histogram to the user.  Simple at the moment
	usleep(500000);
	
	printf("%f\n", meanEvent);
	
	//Prepares small histogram (local) to be written to again for the next entry of data
	//smallHist->Reset();
	c1->Modified();
	c1->Update();
	
    }
}
