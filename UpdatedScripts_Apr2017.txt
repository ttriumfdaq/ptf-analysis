----------------------------------
Scripts Updated Since January 2017
----------------------------------
By: Rika Sugimoto
Date: Apr 30, 2017


Script Name                       Last Used   Description
-----------                       ---------   -----------

DegausCalcPy.sh                   Apr2017     Input: the desired end currents in each coil in a pair, and the number of currents to iterate through for degaussing;

                                              Output: a list of currents to iterate through for each coil in the pair;
                                                      once the voltage required to reach a given current is known,
                                                      one can write a degaussing script that can automate the procedure (see ~/online/src/DegausCycloON.sh)

FitAnalysis1cmShiftedPed.C        29Apr2017   Input: run number of the PMT scan taken by each gantry (in the case where a scan has been taken using both gantries;
                                                         if only one gantry has been used, input the same scan number for each gantry);
                                                     type of plot desired (either using the gantry position directly,
                                                     or using the position of the laser on the PMT for plotting).

                                                     NOTE-the analysis of the ADC spectrum uses hardcoded values in the script that may need to be adjusted
                                                     for different laser sources and other environmental variation such as temperature and noise.
                                                     (we are currently using the FitAnalysis*ShiftedPed.C scripts as we observed a shift in the pedestal position
                                                     compared to December 2016, which required us to adjust some of the hardcoded parameters).

                                              Output: 2D histograms of the relative gain and detection efficiency of the PMT as a function of
                                                      the position of the gantry / PMT surface.

                                                     NOTE-the binning of the histogram must be adjusted based on the start position of the gantry.

FitAnalysis2mmShiftedPed.C        28Apr2017   Same functionality as FitAnalysis1cmShiftedPed.C, but used to produce 2D histograms with
                                              stepsize 2mm in both x & y positions of the gantry.

FitAnalysis5mmShiftedPed.C        28Apr2017   Same functionality as FitAnalysis1cmShiftedPed.C, but used to produce 2D histograms with
                                              stepsize 5mm in both x & y positions of the gantry.


Magneticfield_output_textfile.C   Apr2017     Input: run number of the magnetic field scan;

                                              Output: textfiles in the directory ~/online/src/analysis_scripts/MagneticTextfiles/
                                                      containing 6 columns of data:
                                                      the first three columns correspond to the x, y, z coordinate position of the phidget (magnetometer)
                                                      at a particular point in time during a scan, and
                                                      the last three columns correspond to the x, y, z components of the magnetic field read by the phidget.

                                                      NOTE-the calculation of the phidget position involves reading the gantry position and adding constant offsets
                                                      in each coordinate (the offsets are hardcoded, and currently set for when Phidget 4 is placed
                                                      0.400m below the gantry tilt motor gear).

Magneticfield_output_textfile_ph0.C           Same functionality as Magneticfield_output_textfile.C, producing textfiles of magnetic field data from Phidget 0,
                                                     the positional offset of Phidget 0 has been estimated to be (0.1, 0.1, 0.05).

Magneticfield_output_textfile_ph0_rawdata.C   Same functionality as Magneticfield_output_textfile.C, producing textfiles of magnetic field data from Phidget 0,
                                                     and directly using Gantry 0 positions rather than correcting for the position of the Phidget
                                                     relative to the gantry.

                                                     NOTE-textfiles produced using this script can be used to plot magnetic field plots used for
                                                     magnetic field compensation/calibration, as the gantry position can be directly related to the phidget reading.

magFieldPlotClean.py              Apr2017     Input: run number of the magnetic field scan;

                                                     NOTE-upper and lower x, y, z limits of the scan area, stepsize in the three directions during the scan,
                                                     colorbar boundary settings, and correction for phidget offsets are hardcoded values in the script that
                                                     have to be edited for each scan.

                                              Output: plots of the magnetic field vector and magnitude of its x, y, z components at different z planes.

maFieldPlotClean_ph0.py          Apr2017    
magFieldPlotClean_ph0_rawdata.py
