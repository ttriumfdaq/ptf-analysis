import os
import numpy as np
import csv
import math
from mpl_toolkits.mplot3d import Axes3D
from Tkinter import Tk
import mayavi.mlab as ml

runNumber = input('Enter a run number')

wholeArray = np.array([0.1, 0.1, 0.5, 0, 0, 0])

with open('Magnetic_' + str(runNumber) + '.txt', 'rb') as csvfile:
    readObject = csv.reader(csvfile, delimiter = ',')
    for row in readObject:
        row = np.array(map(float, row))
        print(row)
        wholeArray = np.vstack((wholeArray,row))

csvfile.close()

x = wholeArray[:,0]
y = wholeArray[:,1]
z = wholeArray[:,2]
u = wholeArray[:,3]
v = wholeArray[:,5]
w = wholeArray[:,4]

Bnorm = np.sqrt(u**2 + v**2 + w**2)

ml.quiver3d(x,y,z,u,v,w)
