import os
import numpy as np
import csv
import math
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.patches as mptch
import matplotlib.tri as tri
import plotly.plotly as pltly
from plotly.graph_objs import *
from matplotlib.pyplot import cm
from matplotlib.pyplot import colorbar
from mpl_toolkits.mplot3d import Axes3D
from Tkinter import Tk
from tkFileDialog import askdirectory


def split(arr, cond):
    return arr[cond]

def frange(start, stop, step):
    i = start
    while i < stop:
        yield i
        i += step
        print i

alphaVal = 0.2
runNumber = raw_input('Enter a run number: ')
runString = raw_input('\nEnter a note on the run: ')
trackPlot=1

radiusOfPMT = 0.27
pmtXcentre = 0.374
pmtYcentre = 0.348
pmtZcentre = 0.6

wholeArray = np.array([0.1, 0.1, 0.5, 0, 0, 0])

with open('Magnetic_' + str(runNumber) + '_ph0.txt', 'rb') as csvfile:
    readObject = csv.reader(csvfile, delimiter = ',')
    for row in readObject:
        row = np.array(map(float, row))
        print(row)
        wholeArray = np.vstack((wholeArray,row))

csvfile.close()

Tk().withdraw() # we don't want a full GUI, so keep the root window from appearing
foldername = askdirectory() # show an "Open" dialog box and return the path to the selected file

uLimX=620+100
lLimX=120+100
spaceX = 50

uLimY=600+100
lLimY=90+100
spaceY = 50

uLimZ=550 +50
lLimZ=150 +50
spaceZ=100

edges = np.linspace(-0.050, 0.950, num=21)
xedges = np.linspace((lLimX-25)/1000.0, (uLimX+25)/1000.0, num=12)
yedges = np.linspace((lLimY-25)/1000.0, (uLimY+25)/1000.0, num=12)
    
for cutVal in range(lLimZ, uLimZ, spaceZ):

    fig = plt.figure(figsize=(8.0,6.0))
    mng = plt.get_current_fig_manager()
    mng.full_screen_toggle()
    
    print cutVal
    
    wholeArrayTemp = np.copy(wholeArray)
    wholeArrayTemp = wholeArrayTemp*1000
    wholeArrayTemp.astype(int)
    
    planeArr = split(wholeArrayTemp, wholeArrayTemp[:,2] == cutVal)
    
    x = planeArr[:,0]/1000.0
    y = planeArr[:,1]/1000.0
    z = planeArr[:,2]/1000.0
    u = planeArr[:,3] -8 #the offset in phidget 0 is (+15mG,-65mG,-15mG); see pg215 of PTF Researcher diary ;)
    v = planeArr[:,4] +65
    w = (-1)*(planeArr[:,5] +15)
    
    Bnorm = np.sqrt(u**2 + v**2 + w**2)
    
    BnormLoc = np.sqrt(v**2 + w**2)

    #Plot of overall magnetic field as quiver plot
    ax1 = fig.add_subplot(2,2,1)
    plt.title(runString + '\nMagnetic Field Vector Projection in X-Y Plane at Z = ' + str(cutVal/1000.0)+ '\nContour Overlay is Overall Field Magnitude (mG)')
    plt.xlabel('Position in X (m)')
    plt.ylabel('Position in Y (m)')  
    quiv = plt.quiver(x,y,u,v,Bnorm,cmap=cm.jet, width=0.003, pivot='mid')
    plt.colorbar()
    plt.clim(-50, 50)
    triang = tri.Triangulation(x,y)
    plt.tricontourf(x,y,Bnorm, 15, alpha=alphaVal)
    plt.colorbar()
    plt.clim(-50, 50)
    plt.axis('equal')
    plt.axis([lLimX/1000.0, (uLimX+spaceX)/1000.0, lLimY/1000.0, (uLimY+spaceY)/1000.0])
    
    patch1 = mptch.Circle((pmtXcentre, pmtYcentre),np.sqrt(radiusOfPMT**2 - (cutVal/1000.0 - 0.4  - radiusOfPMT)**2),color='k', fill = False) 
    ax1.add_patch(patch1)
    
    #Plot of field in X Direction
    ax2 = fig.add_subplot(2,2,2)
    plt.title('Magnetic Field Magnitude (mG) in X Direction at Z = ' + str(cutVal/1000.0))
    plt.xlabel('Position in X (m)')
    plt.ylabel('Position in Y (m)')
    plt.hist2d(x,y,weights=u, bins=edges)
    
    for alpha in range(0, len(u)):
        plt.text(x[alpha],y[alpha],int(u[alpha]), fontsize=6)
    
    plt.colorbar()
    plt.clim(-20, 20)
    plt.axis('equal')
    plt.axis([lLimX/1000.0, (uLimX+spaceX)/1000.0, lLimY/1000.0, (uLimY+spaceY)/1000.0])
    patch2 = mptch.Circle((pmtXcentre, pmtYcentre),np.sqrt(radiusOfPMT**2 - (cutVal/1000.0 - 0.4  - radiusOfPMT)**2),color='k', fill = False) 
    ax2.add_patch(patch2)

    #Plot of field in Y Direction
    ax3 = fig.add_subplot(2,2,3)
    plt.title('Magnetic Field Magnitude (mG) in Y Direction at Z = ' + str(cutVal/1000.0))
    plt.xlabel('Position in X (m)')
    plt.ylabel('Position in Y (m)')
    plt.hist2d(x,y,weights=v, bins=edges)
    
    for alpha in range(0, len(v)):
        plt.text(x[alpha],y[alpha],int(v[alpha]), fontsize=6)
        
    plt.colorbar()
    plt.clim(-20, 20)
    plt.axis('equal')
    plt.axis([lLimX/1000.0, (uLimX+spaceX)/1000.0, lLimY/1000.0, (uLimY+spaceY)/1000.0])
    patch3 = mptch.Circle((pmtXcentre, pmtYcentre),np.sqrt(radiusOfPMT**2 - (cutVal/1000.0 - 0.4  - radiusOfPMT)**2),color='k', fill = False) 
    ax3.add_patch(patch3)

    #Plot of field in Z Direction
    ax4 = fig.add_subplot(2,2,4)
    plt.title('Magnetic Field Magnitude (mG) in Z Direction at Z = ' + str(cutVal/1000.0))
    plt.xlabel('Position in X (m)')
    plt.ylabel('Position in Y (m)')
    plt.hist2d(x,y,weights=w, bins=edges)
    
    for alpha in range(0, len(w)):
        plt.text(x[alpha],y[alpha],int(w[alpha]), fontsize=6)
    
    plt.colorbar()
    plt.clim(-20, 20)
    plt.axis('equal')
    plt.axis([lLimX/1000.0, (uLimX+spaceX)/1000.0, lLimY/1000.0, (uLimY+spaceY)/1000.0])
    patch4 = mptch.Circle((pmtXcentre, pmtYcentre),np.sqrt(radiusOfPMT**2 - (cutVal/1000.0 - 0.4  - radiusOfPMT)**2),color='k', fill = False) 
    ax4.add_patch(patch4)

    print ax1.patches
    
    #Saves figure
    save_name = 'xy_z_'+str(cutVal)
    
    plt.draw()
    plt.pause(0.5)

    del ax1
    del ax2
    del ax3
    del ax4
    
    fig.savefig(foldername+os.sep + str(runNumber) + '_ph0_' + save_name+'.png')
    plt.close('all')
    
