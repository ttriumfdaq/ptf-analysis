import os
import numpy as np
import csv
import math
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.patches as mptch
import matplotlib.tri as tri
import plotly.plotly as pltly
from plotly.graph_objs import *
from matplotlib.pyplot import cm
from matplotlib.pyplot import colorbar
from mpl_toolkits.mplot3d import Axes3D
from Tkinter import Tk
from tkFileDialog import askdirectory


def split(arr, cond):
    return arr[cond]

def frange(start, stop, step):
    i = start
    while i < stop:
        yield i
        i += step
        print i

alphaVal = 0.2
runNumber = input('Enter a run number')
trackPlot=1

wholeArray = np.array([0.1, 0.1, 0.5, 0, 0, 0])

with open('Magnetic_' + str(runNumber) + '.txt', 'rb') as csvfile:
    readObject = csv.reader(csvfile, delimiter = ',')
    for row in readObject:
        row = np.array(map(float, row))
        print(row)
        wholeArray = np.vstack((wholeArray,row))

csvfile.close()

Tk().withdraw() # we don't want a full GUI, so keep the root window from appearing
foldername = askdirectory() # show an "Open" dialog box and return the path to the selected file


fig = plt.figure(figsize=(8.0,6.0))

for i in range(0,3):

    print i

    if i==2:
        uLim=960
        lLim=520
        space = 40

    elif i==0:
        uLim=602
        lLim=102
        space = 50

    else:
        uLim=652
        lLim=152
        space=50

    for cutVal in range(lLim, uLim, space):

        print cutVal
        
        ax = fig.gca()
        
        wholeArrayTemp = np.copy(wholeArray)
        wholeArrayTemp = wholeArrayTemp*1000
        wholeArrayTemp.astype(int)
        
        planeArr = split(wholeArrayTemp, wholeArrayTemp[:,i] == cutVal)
        
        x = planeArr[:,0]/1000.0
        y = planeArr[:,1]/1000.0
        z = planeArr[:,2]/1000.0
        u = planeArr[:,3]/1000.0 + 0.021
        v = planeArr[:,5]/1000.0 + 0.004
        w = planeArr[:,4]/1000.0 + 0.021

        Bnorm = np.sqrt(u**2 + v**2 + w**2)
        
        if i == 0:

            BnormLoc = np.sqrt(v**2 + w**2)
            
            plt.subplot(2,2,1)
            plt.title('Magnetic Field Vector Projection in Y-Z Plane at x=' + str(cutVal/1000.0)+ '\nContour Overlay is Overall Field Magnitude')
            plt.xlabel('Position in Y (m)')
            plt.ylabel('Position in Z (m)')  
            quiv = plt.quiver(y,z,v,w,BnormLoc,cmap=cm.jet, width=0.003, pivot='mid')
            plt.colorbar()
            plt.clim(0,1.5)
            triang = tri.Triangulation(y,z)
            plt.tricontourf(y,z,Bnorm, 15, alpha=alphaVal)
            plt.colorbar()
            #plt.clim(0,1.5)
            plt.scatter(0.396, 0.77, s=60000, facecolors='none')
            plt.axis('equal')
            plt.axis([0, 0.7, 0.4, 1.1])
            plt.subplot(2,2,2)
            plt.title('Magnetic Field Magnitude in X at x =' + str(cutVal/1000.0))
            plt.xlabel('Position in Y (m)')
            plt.ylabel('Position in Z (m)')          
            plt.quiver(y,z,v,w,np.fabs(u),cmap=cm.jet, width=0.003, pivot='mid')
            plt.colorbar()
            plt.clim(0, 1.5)
            triang = tri.Triangulation(y,z)
            plt.tricontourf(y,z,np.fabs(u), 15, alpha=alphaVal)
            plt.colorbar()
            plt.scatter(0.396, 0.77, s=60000, facecolors='none')
            plt.axis('equal')
            plt.axis([0, 0.7, 0.4, 1.1])
            plt.subplot(2,2,3)
            plt.xlabel('Magnetic Field in X')
            plt.title('Magnetic Field in X: Hist')
            plt.hist(u, 12)

            plt.subplot(2,2,4)
            plt.xlabel('Magnetic Field Magnitude')
            plt.title('Total Magnetic Field Magnitude: Hist')
            plt.hist(Bnorm, 12)
            
            save_name = 'yz_x_'+str(cutVal)
            
            
            #plt.show()

        if i == 1:

            BnormLoc = np.sqrt(u**2 + w**2)
            
            plt.subplot(2,2,1)
            plt.title('Magnetic Field Vector Projection in X-Z Plane at y =' + str(cutVal/1000.0)+ '\nContour Overlay is Overall Field Magnitude')
            plt.xlabel('Position in X (m)')
            plt.ylabel('Position in Z (m)')
            quiv = plt.quiver(x,z,u,w,BnormLoc, cmap=cm.jet, width=0.003, pivot='mid')
            plt.colorbar()
            plt.clim(0,1.5)
            triang = tri.Triangulation(x,z)
            plt.tricontourf(x,z,Bnorm, 15, alpha=alphaVal)
            plt.colorbar()
            #plt.clim(0, 1.5)
            plt.scatter(0.372, 0.77, s=60000, facecolors='none')
            plt.axis('equal')
            plt.axis([0, 0.7, 0.4, 1.1])
            plt.subplot(2,2,2)
            plt.title('Magnetic Field Magnitude in Y at y =' + str(cutVal/1000.0))
            plt.xlabel('Position in X (m)')
            plt.ylabel('Position in Z (m)')          
            plt.quiver(x,z,u,w,np.fabs(v),cmap=cm.jet, width=0.003, pivot='mid')
            plt.colorbar()
            plt.clim(0, 1.5)
            triang = tri.Triangulation(x,z)
            plt.tricontourf(x,z,np.fabs(v), 15, alpha=alphaVal)
            plt.colorbar()
            plt.scatter(0.372, 0.77, s=60000, facecolors='none')
            plt.axis('equal')
            plt.axis([0, 0.7,0.4, 1.1])
            plt.subplot(2,2,3)
            plt.xlabel('Magnetic Field in Y')
            plt.title('Magnetic Field in Y: Hist')
            plt.hist(v, 12)
            
            plt.subplot(2,2,4)
            plt.xlabel('Magnetic Field Magnitude')
            plt.title('Total Magnetic Field Magnitude: Hist')
            plt.hist(Bnorm, 12)
            
            save_name = 'xz_y_'+str(cutVal)
            #plt.show()
            
        if i == 2:

            BnormLoc = np.sqrt(v**2 + u**2)
            
            plt.subplot(2,2,1)
            plt.title('Magnetic Field Vector Projection in X-Y Plane at z =' + str(cutVal/1000.0)+ '\nContour Overlay is Overall Field Magnitude')
            plt.xlabel('Position in X (m)')
            plt.ylabel('Position in Y (m)')
            quiv = plt.quiver(x,y,u,v,BnormLoc, cmap=cm.jet, width=0.003, pivot='mid')
            plt.colorbar()
            plt.clim(0,1.5)
            triang = tri.Triangulation(x,y)
            plt.tricontourf(x,y,Bnorm, 15, alpha=alphaVal)
            plt.colorbar()
            #plt.clim(0, 1.5)
            plt.scatter(0.372, 0.396, s=60000, facecolors='none')
            plt.axis('equal')
            plt.axis([0, 0.7, 0, 0.7])
            plt.subplot(2,2,2)
            plt.title('Magnetic Field Magnitude in Z at z =' + str(cutVal/1000.0))
            plt.xlabel('Position in X (m)')
            plt.ylabel('Position in Y (m)')          
            plt.quiver(x,y,u,v,np.fabs(w),cmap=cm.jet, width=0.003, pivot='mid')
            plt.colorbar()
            plt.clim(0, 1.5)
            triang = tri.Triangulation(x,y)
            plt.tricontourf(x,y,np.fabs(w), 15, alpha=alphaVal)
            plt.colorbar()
            plt.scatter(0.372, 0.396, s=60000, facecolors='none')
            plt.axis('equal')
            plt.axis([0, 0.7, 0, 0.7])
            plt.subplot(2,2,3)
            plt.xlabel('Magnetic Field in Z')
            plt.title('Magnetic Field in Z: Hist')
            plt.hist(w, 12)

            plt.subplot(2,2,4)
            plt.xlabel('Magnetic Field Magnitude')
            plt.title('Total Magnetic Field Magnitude: Hist')
            plt.hist(Bnorm, 12)
            
            save_name = 'xy_z_'+str(cutVal)
            #plt.show()         
            
        plt.draw()
        plt.pause(0.5)

        
        fig.savefig(foldername+os.sep + str(runNumber) + '_' + save_name+'.png')

        plt.clf()
