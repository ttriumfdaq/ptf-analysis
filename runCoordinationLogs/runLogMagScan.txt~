Log File for Tracking Magnetic Field Scan Data Collection:

Measurement Performed: 3-D Scan of Magnetic Field for Set Field Offsets

Scan Parameters:
     
     Time (ms):		2000
     X-Length (m):	0.5
     Y-Length (m):	0.5
     Z-Length (m):	0.4
     X-Step (m):	0.05
     Y-Step (m):	0.05
     Z-Step (m):	0.05
     X-Start (m):	0.102
     Y-Start (m):	0.102
     Z-Start (m):	0.02
     Tilt (deg):	-90
     Rotation (deg):	90			
     Gantry (0,1):	0

Z-Axis Remarks:

       The difference in coil currents between Coil 1 and Coil 2 in order to
       compensate for the inherent gradient in the Z-Direction in the ambient
       B-Field has been measured to be 0.74 Ampere. 

       The relationship between Coil 1 current (I1) and Coil 2 current (I2) 
       is as follows:

       	   I2 = I1 + 0.74A 

Degaussing:

	The Degaussing procedure must be completed before a run is begun. 
	Once completed, increase or decrease coil currents to achieve desired
	B-Field offset.	   

X-Axis:

Offset(mG) |Current (mA)			     |Run Number     |Time Start     |Time End	     |User
-----------|Coil1-|Coil2-|Coil3-|Coil4-|Coil5-|Coil6-|---------------|---------------|---------------|---------------|
    +50mG  | 1967 | 2715 | 266  | 269  | 158  | 159  | 1952	     | 15:00	     | 17:00	     | Alexander
    +100mG | 1969 | 2719 | 206  | 202  | 152  | 153  | 1953          | 17:12         | 19:13	     | Alexander
    +150mG | 1870 | 2644 | 45	| 0    | 139  |	141  | 1954	     | 20:08         | 22:08         | Alexander
    -50mG  | 1989 | 2751 | 740	| 737  | 157  |	158  | 1955	     | 22:34	     | 23:56         | Alexander
    -100mG | 1991 | 2724 | 986	| 970  | 158  |	159  | 1956          | 00:33	     | 02:33         | Alexander
    -150mG | 1987 | 2721 | 1169	| 1191 | 158  |	159  | 1957	     | 02:51	     | 04:51 	     | Alexander       

Y-Axis:                                                                                                               

Offset(mG) |Current (mA)			     |Run Number     |Time Start     |Time End	     |User
-----------|Coil1-|Coil2-|Coil3-|Coil4-|Coil5-|Coil6-|---------------|---------------|---------------|---------------|
    +50mG  | 2004 | 2743 | 575  | 553  | 197  | 194  | 1958 	     | 08:40	     | 10:35	     | Alexander
    +100mG | 2007 | 2745 | 541  | 523  | 208  | 209  | 1960          | 11:15         | 13:13	     | Alexander
    +150mG | 2009 | 2747 | 570	| 548  | 272  |	274  | 1961          | 15:37         | 17:38	     | Alexander
    -50mG  | 2007 | 2743 | 561	| 538  | 119  |	119  | 1962	     | 17:52	     | 19:52         | Alexander
    -100mG | 2009 | 2745 | 556	| 538  | 076  |	079  | 1963	     | 20:21	     | 22:22         | Alexander
    -150mG | 2011 | 2747 | 562	| 543  | 018  |	022  | 1964	     | 23:00	     | 01:01	     | Alexander

Z-Axis:

Offset(mG) |Current (mA)			     |Run Number     |Time Start     |Time End	     |User
-----------|Coil1-|Coil2-|Coil3-|Coil4-|Coil5-|Coil6-|---------------|---------------|---------------|---------------|
    +50mG  | 1906 | 2650 | 556  | 538  | 154  | 158  | 1965	     | 01:26	     | 03:26	     | Alexander
    +100mG | 1788 | 2538 | 556  | 538  | 148  | 156  | 1966          | 09:24         | 11:25	     | Alexander
    +150mG | 1677 | 2448 | 555	| 534  | 150  |	154  | 1967          | 11:40         | 13:41	     | Alexander
    -50mG  | 2113 | 2850 | 561	| 539  | 150  |	154  | 1968	     | 13:59	     | 16:00         | Alexander
    -100mG | 2228 | 2951 | 566	| 543  | 150  |	156  | 1970	     | 16:22	     | 18:21         | Alexander
    -150mG | 2331 | 3056 | 580	| 562  | 150  |	155  | 1971	     | 18:54	     | 20:55	     | Alexander

Add scan:
X-axis:

Offset(mG) |Current (mA)			     |Run Number     |Time Start     |Time End	     |User
-----------|Coil1-|Coil2-|Coil3-|Coil4-|Coil5-|Coil6-|---------------|---------------|---------------|---------------|
    +50mG  | 2012 | 2751 | 361  | 358  | 150  | 148  | 1978	     | 15:40(11/16)  | 18:10	     | Fukuda
    +100mG | 2014 | 2752 | 161  | 163  | 149  | 148  | 1979          | 19:20         | 22:00	     | Fukuda

Normal:
Offset(mG) |Current (mA)			     |Run Number     |Time Start     |Time End	     |User
-----------|Coil1-|Coil2-|Coil3-|Coil4-|Coil5-|Coil6-|---------------|---------------|---------------|---------------|
    0mG    | 2004 | 2741 | 550  | 537  | 150  | 148  | 1977	     | 13:00(11/16)  | 15:30 	     | Fukuda


Add scan:
X-axis:

Offset(mG) |Current (mA)			     |Run Number     |Time Start     |Time End	     |User
-----------|Coil1-|Coil2-|Coil3-|Coil4-|Coil5-|Coil6-|---------------|---------------|---------------|---------------|
    +50mG  | 2014 | 2751 | 356  | 353  | 150  | 149  | 1986	     | 8:30(11/17)   | 10:30	     | Fukuda
//    +100mG | 2014 | 2752 | 161  | 163  | 149  | 148  | 1979          | 19:20         | 22:00	     | Fukuda

Normal:
Offset(mG) |Current (mA)			     |Run Number     |Time Start     |Time End	     |User
-----------|Coil1-|Coil2-|Coil3-|Coil4-|Coil5-|Coil6-|---------------|---------------|---------------|---------------|
//    0mG    | 2004 | 2741 | 550  | 537  | 150  | 148  | 1977	     | 13:00(11/16)  | 15:30 	     | Fukuda



-Alexander Jaffray
