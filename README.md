# README #

A series of standalone scripts for analysis of PTF data using ROOT analysis package

### What is this repository for? ###

* Scripts for analysis of PTF and PMT data and gantry position calculations
* First Iteration (Summer 2016)

### How do I get set up? ###

* Make sure ROOT analysis platform is available to use
* Navigate to directory where you would like to clone this repository or the original on midptf (can use ssh) 
* Begin ROOT
* Ensure you have local copies of the data files (ROOT files) you would like to analyze in the same folder
* Run the scripts by typing ".x myFile.cxx" in the root interpreter or if it is a python script type "python myFile.py"
* Save output and results as pdf or make another root tree holding data for subsequent analysis
* IMPORTANT NOTE:  If you are ssh-ing into midptf, please ensure you have XMING installed and correctly configure PuTTY (https://wiki.utdallas.edu/wiki/display/FAQ/X11+Forwarding+using+Xming+and+PuTTY)

### Contribution guidelines ###

* Ensure that all analysis is correct (measure twice, cut once approach)
* For scripts involving positional plotting, use a histogram with appropriate bins to map the x-y-z coordinate grid
* Proper commenting is encouraged, and making use of the colour scheme used by the T2K group is appreciated (Ask Tom about this)

### Who do I talk to? ###

* Alex Jaffray (or current Co-op student)
* Thomas Lindner
* Tom Feusels