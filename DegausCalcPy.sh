#!/bin/bash

function getDegausCurrents {
I1="$1" I2="$2" NUM_ITER="$3" python - <<END
import os
import math

current1 = int(os.environ['I1'])
current2 = int(os.environ['I2'])
numIterations = int(os.environ['NUM_ITER'])


if current1 < current2:
    smallerCurrent = current1
else:
    smallerCurrent = current2

k = (numIterations-2.0) / math.log(10.0/smallerCurrent)

for i in range(0, numIterations-1):
    if i%2 == 0:
        i1 = current1 - smallerCurrent*math.exp(i/k)
        i2 = current2 - smallerCurrent*math.exp(i/k)
    else:
        i1 = current1 + smallerCurrent*math.exp(i/k)
        i2 = current2 + smallerCurrent*math.exp(i/k)

    print "Current in first coil = " + str(i1) + "mA"
    print "Current in second coil = " + str(i2) + "mA"

print "Current in first coil = " + str(current1) + "mA"
print "Current in second coil = " + str(current2) + "mA"

END
}

printf "Degaussing Calculations\n"

read -p "Input final current for first coil: " curr1
read -p "Input final current for second coil: " curr2
read -p "Input number of degausing currents wanted: " numIterations

getDegausCurrents $curr1 $curr2 $numIterations


echo "Done"